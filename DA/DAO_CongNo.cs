﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
	public class DAO_CongNo : DBConnect
	{
		#region Get danh sách công nợ

		/// <summary>
		/// Get danh sách công nợ
		/// </summary>
		/// <returns></returns>
		public DataTable GetCongNo()
		{
			SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM CongNo", _conn);
			DataTable dtCongNo = new DataTable();
			adapter.Fill(dtCongNo);
			return dtCongNo;
		}

		#endregion

		#region Get danh sách công nợ theo mã công nợ

		/// <summary>
		/// Get danh sách công nợ theo mã công nợ
		/// </summary>
		/// <returns></returns>
		public DataTable GetCongNoByID(string Id)
		{
			SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM CongNo WHERE ID_CongNo = {Id} AND DelFlg = 0", _conn);
			DataTable dtCongNo = new DataTable();
			adapter.Fill(dtCongNo);
			return dtCongNo;
		}

		#endregion

		#region Get danh sách công nợ theo mã công nợ

		/// <summary>
		/// Get danh sách công nợ theo mã công nợ
		/// </summary>
		/// <returns></returns>
		public DataTable GetCongNoById(string Id_LsTp, string Id_KhachHang, string Id_Phong)
		{
			SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM CongNo WHERE ID_LichSuThuePhong = '" + Id_LsTp + "' AND ID_KhachHang = '" + Id_KhachHang + "' AND ID_Phong = '" + Id_Phong + "' AND DelFlg = 0", _conn);
			DataTable dtCongNo = new DataTable();
			adapter.Fill(dtCongNo);
			return dtCongNo;
		}

		#endregion

		#region Update trạng thái nợ

		/// <summary>
		/// Update trạng thái nợ
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		public bool UpdateTrangThaiNo(string Id, string tt)
		{
			_conn.Open();
			string strQuery = "UPDATE CongNo SET TrangThaiNo = '" + tt + "' WHERE ID_CongNo = '" + Id + "';";
			SqlDataAdapter dt = new SqlDataAdapter();
			dt.InsertCommand = new SqlCommand(strQuery, _conn);
			dt.InsertCommand.ExecuteNonQuery();
			_conn.Close();

			return true;
		}

		#endregion

		#region 

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetMaxIdCongNo()
		{
			string idCongNo= string.Empty;
			DataTable dtCongNo = new DataTable();

			_conn.Open();
			SqlCommand command = new SqlCommand("SELECT TOP 1 ID_CongNo FROM CongNo order by ID_CongNo DESC", _conn);
			using (SqlDataReader reader = command.ExecuteReader())
			{
				if (reader.Read())
				{
					idCongNo = reader["ID_CongNo"].ToString();
				}
			}
			_conn.Close();

			return idCongNo;
		}

		#endregion

		#region 

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public bool Update(DTO_CongNo congNo)
		{
			try
			{
				_conn.Open();
				string strQuery = string.Empty;
				if (congNo.FlagInsert == true)
				{
					strQuery = "INSERT INTO CongNo (ID_CongNo, ID_KhachHang, ID_Phong , ThoiGianBatDau ,ThoiGianKetThuc, TrangThaiNo, DelFlg , ID_LichSuThuePhong, SoTienNo, ID_SoDien, ID_SoNuoc ) VALUES(N'" + congNo.ID_CongNo + "' , N'" + congNo.ID_KhachHang + "' ,N'" + congNo.ID_Phong + "',N'" + congNo.ThoiGianBatDau + "',N'" + congNo.ThoiGianKetThuc + "',N'" + congNo.TrangThaiNo + "'," + 0 + ",N'" + congNo.ID_LichSuThuePhong + "',N'" + congNo.SoTienNo + "',N'" + congNo.ID_SoDien + "',N'" + congNo.ID_SoNuoc + "'); ";
				}
				else
				{
					strQuery = "UPDATE CongNo SET SoTienNo = '" + congNo.SoTienNo + "' WHERE ID_Phong = '" + congNo.ID_Phong + "' AND ID_KhachHang = '" + congNo.ID_KhachHang + "' AND ID_SoDien <> ''";
				}

				SqlDataAdapter dt = new SqlDataAdapter();
				dt.InsertCommand = new SqlCommand(strQuery, _conn);
				dt.InsertCommand.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				return false;
			}

			return true;
		}

		#endregion

		public bool UpdateSoTienNo(string Id, string tien)
		{
			_conn.Open();
			string strQuery = "UPDATE CongNo SET SoTienNo = '" + tien + "' WHERE ID_CongNo = '" + Id + "';";
			SqlDataAdapter dt = new SqlDataAdapter();
			dt.InsertCommand = new SqlCommand(strQuery, _conn);
			dt.InsertCommand.ExecuteNonQuery();
			_conn.Close();

			return true;
		}

		public DataTable CheckCongNoByIdDienNuoc(string Id_LsTp, string Id_Dien, string Id_Nuoc)
		{
			SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM CongNo WHERE ID_LichSuThuePhong = '" + Id_LsTp + "' AND ID_SoDien = '" + Id_Dien + "' AND ID_SoNuoc = '" + Id_Nuoc + "' AND DelFlg = 0", _conn);
			DataTable dtCongNo = new DataTable();
			adapter.Fill(dtCongNo);
			return dtCongNo;
		}
	}
}
