﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
	public class DAO_LichSuThanhToan : DBConnect
	{
		#region Insert/Update thông tin lịch sử thanh toán

		/// <summary>
		/// Insert/Update thông tin lịch sử  thanh toán
		/// </summary>
		/// <returns></returns>
		public bool Update(DTO_LichSuThanhToan lichSuThanhToan)
		{
			try
			{
				_conn.Open();
				string strQuery = string.Empty;
				if (lichSuThanhToan.FlagInsert == true)
				{
					strQuery = "INSERT INTO LichSuThanhToan (ID_ThanhToan, ID_LichSuThuePhong, ID_SoDien , ID_SoNuoc , KyThanhToan , SoTienThanhToan ) VALUES('" + lichSuThanhToan.ID_ThanhToan + "' , '" + lichSuThanhToan.ID_LichSuThuePhong + "' ,N'" + lichSuThanhToan.ID_SoDien + "',N'" + lichSuThanhToan.ID_SoNuoc + "',N'" + lichSuThanhToan.KyThanhToan + "', '" + lichSuThanhToan.SoTienThanhToan + "'); ";
				}
				SqlDataAdapter dt = new SqlDataAdapter();
				dt.InsertCommand = new SqlCommand(strQuery, _conn);
				dt.InsertCommand.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				return false;
			}

			return true;
		}

		#endregion

		#region Get id lịch sử thanh toán lớn nhất

		/// <summary>
		/// Get id lịch sử thanh toán lớn nhất
		/// </summary>
		/// <returns></returns>
		public string GetMaxIdLsThanhToan()
		{
			string idLsThanhtoan = string.Empty;

			_conn.Open();
			SqlCommand command = new SqlCommand("SELECT TOP 1 ID_ThanhToan FROM LichSuThanhToan order by ID_ThanhToan DESC", _conn);
			using (SqlDataReader reader = command.ExecuteReader())
			{
				if (reader.Read())
				{
					idLsThanhtoan = reader["ID_ThanhToan"].ToString();
				}
			}
			_conn.Close();

			return idLsThanhtoan;
		}

		#endregion
	}
}
