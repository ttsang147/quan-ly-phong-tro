﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
	public class DAO_TaiKhoan : DBConnect
	{
		#region Get danh sách tài khoản

		/// <summary>
		/// Get danh sách tài khoản
		/// </summary>
		/// <returns></returns>
		public DataTable GetTaiKhoan()
		{
			SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM TaiKhoan where DelFlg = 0", _conn);
			DataTable dtTk = new DataTable();
			adapter.Fill(dtTk);
			return dtTk;
		}

		#endregion

		#region Get danh sách tài khoản by Name

		/// <summary>
		/// Get danh sách tài khoản by Name
		/// </summary>
		/// <returns></returns>
		public DataTable GetTaiKhoanbyName(string key)
		{
			SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM TaiKhoan where TenTaiKhoan like '%" + key + "%' and DelFlg = 0", _conn);
			DataTable dtTk = new DataTable();
			adapter.Fill(dtTk);
			return dtTk;
		}

		#endregion

		#region Get tài khoản theo ID

		/// <summary>
		/// Get tài khoản theo ID
		/// </summary>
		/// <returns></returns>
		public DataTable GetTaiKhoanByID(string Id)
		{
			SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM TaiKhoan WHERE ID_TaiKhoan = '" + Id + "' AND DelFlg = 0", _conn);
			DataTable dtTk = new DataTable();
			adapter.Fill(dtTk);
			return dtTk;
		}

		#endregion

		#region Get tài khoản login

		/// <summary>
		/// Get tài khoản login
		/// </summary>
		/// <returns></returns>
		public DataTable GetLogin(string Ten, string Pass)
		{
			SqlDataAdapter adapter = new SqlDataAdapter(@"SELECT * FROM TaiKhoan WHERE TenTaiKhoan = '" +Ten+ "' AND Password = '" + Pass + "' AND DelFlg = 0", _conn);
			DataTable dtTk = new DataTable();
			adapter.Fill(dtTk);
			return dtTk;
		}

		#endregion

		#region 

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetMaxId()
		{
			string id = string.Empty;

			_conn.Open();
			SqlCommand command = new SqlCommand("SELECT TOP 1 ID_TaiKhoan FROM TaiKhoan order by ID_TaiKhoan DESC", _conn);
			using (SqlDataReader reader = command.ExecuteReader())
			{
				if (reader.Read())
				{
					id = reader["ID_TaiKhoan"].ToString();
				}
			}
			_conn.Close();

			return id;
		}

		#endregion

		public bool Update(DTO_TaiKhoan tk)
		{
			try
			{
				_conn.Open();
				string strQuery = string.Empty;
				if (tk.FlagInsert == true)
				{
					strQuery = "INSERT INTO TaiKhoan (ID_TaiKhoan, Password, TenTaiKhoan , Quyen , DelFlg) VALUES('" + tk.ID_TaiKhoan + "' , N'" + tk.Password + "' ,N'" + tk.TenTaiKhoan + "','" + tk.Quyen + "',N'" + tk.DelFlg + "'); ";
				}
				else
				{
					strQuery = "UPDATE TaiKhoan SET Password = '" + tk.Password + "',TenTaiKhoan = N'" + tk.TenTaiKhoan + "', Quyen = '" + tk.Quyen + "' WHERE ID_TaiKhoan = '" + tk.ID_TaiKhoan + "'";
				}

				SqlDataAdapter dt = new SqlDataAdapter();
				dt.InsertCommand = new SqlCommand(strQuery, _conn);
				dt.InsertCommand.ExecuteNonQuery();
			}
			catch (Exception)
			{
				return false;
			}

			return true;
		}

		public bool Delete(string id)
		{
			try
			{
				_conn.Open();
				string strQuery = "UPDATE TaiKhoan SET DelFlg = 1 WHERE ID_TaiKhoan = '" + id + "';";
				SqlDataAdapter dt = new SqlDataAdapter();
				dt.InsertCommand = new SqlCommand(strQuery, _conn);
				dt.InsertCommand.ExecuteNonQuery();
				_conn.Close();
			}
			catch (Exception)
			{

				return false;
			}

			return true;
		}
	}
}
