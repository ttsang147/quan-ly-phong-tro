﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
	public class DAO_DataGet : DBConnect
	{
		#region Get danh sách đối tượng dùng cho trang chủ

		/// <summary>
		/// Get danh sách đối tượng dùng cho trang chủ
		/// </summary>
		/// <returns></returns>
		public DataTable GetDataTagetUseTrangChu(string searchStr, string trangThaiPhong, string trangThaiPhi)
		{
			SqlCommand cmd = new SqlCommand();
			SqlDataAdapter da = new SqlDataAdapter();
			DataTable dataTagetUseTrangChu = new DataTable();

			cmd = new SqlCommand("STP_ListDataTagetUseTrangChu", _conn);
			cmd.Parameters.Add(new SqlParameter("@Search", searchStr));
			cmd.Parameters.Add(new SqlParameter("@TrangThaiPhong", trangThaiPhong));
			cmd.Parameters.Add(new SqlParameter("@TrangThaiPhi", trangThaiPhi));
			cmd.CommandType = CommandType.StoredProcedure;
			da.SelectCommand = cmd;
			da.Fill(dataTagetUseTrangChu);
			return dataTagetUseTrangChu;
		}

		#endregion

		#region Get thông tin khách hàng

		/// <summary>
		/// Get thông tin khách hàng
		/// </summary>
		/// <returns></returns>
		public DataTable GetDataKhachHangInf(string searchStr)
		{
			SqlCommand cmd = new SqlCommand();
			SqlDataAdapter da = new SqlDataAdapter();
			DataTable dataKhachHang = new DataTable();

			cmd = new SqlCommand("STP_GetDataKhachHang", _conn);
			cmd.Parameters.Add(new SqlParameter("@Search", searchStr));
			cmd.CommandType = CommandType.StoredProcedure;
			da.SelectCommand = cmd;

			da.Fill(dataKhachHang);
			return dataKhachHang;
		}

		#endregion

		#region Get thông tin điện nước

		/// <summary>
		/// Get thông tin điện nước
		/// </summary>
		/// <returns></returns>
		public DataTable GetDataDienNuocInf( string phong, string tt)
		{
			SqlCommand cmd = new SqlCommand();
			SqlDataAdapter da = new SqlDataAdapter();
			DataTable dataDienNuoc = new DataTable();

			cmd = new SqlCommand("STP_GetDataDienNuoc", _conn);
			cmd.Parameters.Add(new SqlParameter("@Search", phong));
			cmd.Parameters.Add(new SqlParameter("@TrangThaiPhong", tt));
			cmd.CommandType = CommandType.StoredProcedure;
			da.SelectCommand = cmd;

			da.Fill(dataDienNuoc);
			return dataDienNuoc;
		}

		#endregion

		public DataTable GetDataTinhTienInf(string search, string tt)
		{
			SqlCommand cmd = new SqlCommand();
			SqlDataAdapter da = new SqlDataAdapter();
			DataTable dataTinhTien = new DataTable();

			cmd = new SqlCommand("STP_GetDataTinhTien", _conn);
			cmd.Parameters.Add(new SqlParameter("@Search", search));
			cmd.Parameters.Add(new SqlParameter("@TrangThaiPhong", tt));
			cmd.CommandType = CommandType.StoredProcedure;
			da.SelectCommand = cmd;

			da.Fill(dataTinhTien);
			return dataTinhTien;
		}

		public DataTable GetDataBaoCao(string search, string tt, string ngay)
		{
			SqlCommand cmd = new SqlCommand();
			SqlDataAdapter da = new SqlDataAdapter();
			DataTable dataBaoCao = new DataTable();

			cmd = new SqlCommand("STP_GetDataBaoCao", _conn);
			cmd.Parameters.Add(new SqlParameter("@Search", search));
			cmd.Parameters.Add(new SqlParameter("@TrangThaiPhong", tt));
			cmd.Parameters.Add(new SqlParameter("@Ngay", ngay));
			cmd.CommandType = CommandType.StoredProcedure;
			da.SelectCommand = cmd;

			da.Fill(dataBaoCao);
			return dataBaoCao;
		}

		public DataTable GetDataSoDien(string idPhong)
		{
			SqlCommand cmd = new SqlCommand();
			SqlDataAdapter da = new SqlDataAdapter();
			DataTable dataChiTiet= new DataTable();

			cmd = new SqlCommand("STP_GetDataSoDien", _conn);
			cmd.Parameters.Add(new SqlParameter("@IdPhong", idPhong));
			cmd.CommandType = CommandType.StoredProcedure;
			da.SelectCommand = cmd;

			da.Fill(dataChiTiet);
			return dataChiTiet;
		}

		public DataTable GetDataSoNuoc(string idPhong)
		{
			SqlCommand cmd = new SqlCommand();
			SqlDataAdapter da = new SqlDataAdapter();
			DataTable dataChiTiet = new DataTable();

			cmd = new SqlCommand("STP_GetDataSoNuoc", _conn);
			cmd.Parameters.Add(new SqlParameter("@IdPhong", idPhong));
			cmd.CommandType = CommandType.StoredProcedure;
			da.SelectCommand = cmd;

			da.Fill(dataChiTiet);
			return dataChiTiet;
		}

	}
}
