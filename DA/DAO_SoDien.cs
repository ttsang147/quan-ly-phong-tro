﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
	public class DAO_SoDien : DBConnect
	{
		#region Update sổ điện

		/// <summary>
		/// Update sổ điện
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		public bool Update(string Idp, string IdSd, string SoCu, string SoMoi)
		{
			_conn.Open();
			string strQuery = "UPDATE SoDien SET SoCu = '" + SoCu + "', SoMoi = '" + SoMoi + "' WHERE ID_SoDien = '" + IdSd + "' AND ID_Phong = '" + Idp + "';";
			SqlDataAdapter dt = new SqlDataAdapter();
			dt.InsertCommand = new SqlCommand(strQuery, _conn);
			dt.InsertCommand.ExecuteNonQuery();
			_conn.Close();

			return true;
		}

		#endregion

		public string GetMaxIdSoDien()
		{
			string idSodien = string.Empty;

			_conn.Open();
			SqlCommand command = new SqlCommand("SELECT TOP 1 ID_SoDien FROM SoDien order by ID_SoDien DESC", _conn);
			using (SqlDataReader reader = command.ExecuteReader())
			{
				if (reader.Read())
				{
					idSodien = reader["ID_SoDien"].ToString();
				}
			}
			_conn.Close();

			return idSodien;
		}

		public bool Insert(DTO_SoDien soDien)
		{
			try
			{
				_conn.Open();
				string strQuery = string.Empty;
				if (soDien.FlagInsert == true)
				{
					strQuery = "INSERT INTO SoDien (ID_SoDien, ID_Phong, SoCu , SoMoi , NgayBatDauSuDungDien , NgayKetThucSuDungDien, DelFlg, ID_LoaiDonGia ) VALUES('" + soDien.ID_SoDien + "' , '" + soDien.ID_Phong + "' ,N'" + soDien.SoCu + "',N'" + soDien.SoMoi + "',N'" + soDien.NgayBatDauSuDungDien + "', '" + soDien.NgayKetThucSuDungDien + "','" + soDien.DelFlg + "','" + soDien.ID_LoaiDonGia + "'); ";
				}

				SqlDataAdapter dt = new SqlDataAdapter();
				dt.InsertCommand = new SqlCommand(strQuery, _conn);
				dt.InsertCommand.ExecuteNonQuery();
			}
			catch (Exception)
			{
				return false;
			}

			return true;
		}

		public void Delete( string id )
		{
			_conn.Open();
			string strQuery = "UPDATE SoDien SET DelFlg = 1 WHERE ID_SoDien = '" + id + "';";
			SqlDataAdapter dt = new SqlDataAdapter();
			dt.InsertCommand = new SqlCommand(strQuery, _conn);
			dt.InsertCommand.ExecuteNonQuery();
			_conn.Close();
		}

		public void DeleteByIdPhong(string id)
		{
			_conn.Open();
			string strQuery = "UPDATE SoDien SET DelFlg = 1 WHERE ID_Phong = '" + id + "';";
			SqlDataAdapter dt = new SqlDataAdapter();
			dt.InsertCommand = new SqlCommand(strQuery, _conn);
			dt.InsertCommand.ExecuteNonQuery();
			_conn.Close();
		}

		public DataTable GetMSoDienByIdPhong(string id)
		{
			SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM SoDien WHERE ID_Phong = '"+ id +"'", _conn);
			DataTable dtLPhong = new DataTable();
			adapter.Fill(dtLPhong);
			return dtLPhong;
		}
	}
}
