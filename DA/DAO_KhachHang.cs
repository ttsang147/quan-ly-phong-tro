﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
	public class DAO_KhachHang : DBConnect
	{
		#region Get danh sách khách hàng

		/// <summary>
		/// Get danh sách khách hàng
		/// </summary>
		/// <returns></returns>
		public DataTable GetKhachHang()
		{
			SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM KhachHang", _conn);
			DataTable dtKhachHang = new DataTable();
			adapter.Fill(dtKhachHang);
			return dtKhachHang;
		}

		#endregion

		#region Get danh sách khách hàng theo ID khách hang

		/// <summary>
		/// Get danh sách khách hàng theo ID khách hang
		/// </summary>
		/// <returns></returns>
		public DataTable GetKhachHangByID(string Id)
		{
			SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM KhachHang WHERE ID_KhachHang = '" + Id + "' AND DelFlg = 0", _conn);
			DataTable dtKhachHang = new DataTable();
			adapter.Fill(dtKhachHang);
			return dtKhachHang;
		}

		#endregion

		#region Get danh sách khách hàng theo ID khách hang

		/// <summary>
		/// Get danh sách khách hàng theo ID khách hang
		/// </summary>
		/// <returns></returns>
		public string GetMaxIdKhachHang()
		{
			string idKhachHang = string.Empty;
			DataTable dtKhachHang = new DataTable();

			_conn.Open();
			SqlCommand command = new SqlCommand("SELECT TOP 1 ID_KhachHang FROM KhachHang order by ID_KhachHang DESC", _conn);
			using (SqlDataReader reader = command.ExecuteReader())
			{
				if (reader.Read())
				{
					idKhachHang = reader["ID_KhachHang"].ToString();
				}
			}
			_conn.Close();

			return idKhachHang;
		}

		#endregion

		#region insert thông tin khách hàng

		/// <summary>
		/// insert thông tin khách hàng
		/// </summary>
		/// <returns></returns>
		public bool Update(DTO_KhachHang khachHang)
		{
			try
			{
				_conn.Open();
				string strQuery = string.Empty;
				if (khachHang.FlagInsert == true)
				{
					strQuery = "INSERT INTO KhachHang (ID_KhachHang, TenKhachHang, CCCD , SoDienThoai , DelFlg , DiaChi ) VALUES(N'" + khachHang.ID_KhachHang + "' , N'" + khachHang.TenKhachHang + "' ,N'" + khachHang.CMND + "',N'" + khachHang.SoDienThoai + "'," + 0 + ",N'" + khachHang .DiaChi + "'); ";
				}
				else
				{
					strQuery = "UPDATE KhachHang SET TenKhachHang = N'" + khachHang.TenKhachHang + "',CCCD = N'" + khachHang.CMND + "', SoDienThoai = '" + khachHang.SoDienThoai + "', DiaChi = N'" + khachHang.DiaChi + "' WHERE ID_KhachHang = '" + khachHang.ID_KhachHang + "'";
				}

				SqlDataAdapter dt = new SqlDataAdapter();
				dt.InsertCommand = new SqlCommand(strQuery, _conn);
				dt.InsertCommand.ExecuteNonQuery();
			}
			catch (Exception ẽ)
			{
				return false;
			}

			return true;
		}

		#endregion

		public bool Delete(string id)
		{
			_conn.Open();
			string strQuery = "UPDATE KhachHang SET DelFlg = 1 where ID_KhachHang = '" + id + "';";
			SqlDataAdapter dt = new SqlDataAdapter();
			dt.InsertCommand = new SqlCommand(strQuery, _conn);
			dt.InsertCommand.ExecuteNonQuery();
			return true;
		}

		public DataTable GetKhachHangByIdPhong(string Id)
		{
			SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM KhachHang WHERE ID_Phong = '" + Id + "' AND DelFlg = 0", _conn);
			DataTable dtKhachHang = new DataTable();
			adapter.Fill(dtKhachHang);
			return dtKhachHang;
		}
	}
}
