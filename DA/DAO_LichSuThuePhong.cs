﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
	public class DAO_LichSuThuePhong : DBConnect
	{
		#region Get danh sách lịch sử thuê phòng

		/// <summary>
		/// Get danh sách lịch sử thuê phòng
		/// </summary>
		/// <returns></returns>
		public DataTable GetLichSuThuePhong()
		{
			SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM LichSuThuePhong", _conn);
			DataTable dtLSTPhong = new DataTable();
			adapter.Fill(dtLSTPhong);
			return dtLSTPhong;
		}

		#endregion

		#region Get danh sách lịch sử thuê phòng theo mã lịch sử thuê phòng

		/// <summary>
		/// Get danh sách lịch sử thuê phòng theo mã lịch sử thuê phòng
		/// </summary>
		/// <returns></returns>
		public DataTable GetLichSuThuePhongByID(string Id)
		{
			SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM LichSuThuePhong WHERE ID_LichSuThuePhong = '" +Id + "' AND DelFlg = 0", _conn);
			DataTable dtLSTPhong = new DataTable();
			adapter.Fill(dtLSTPhong);
			return dtLSTPhong;
		}

		#endregion

		#region Get id lịch sử thuê phòng lớn nhất

		/// <summary>
		/// Get id lịch sử thuê phòng lớn nhất
		/// </summary>
		/// <returns></returns>
		public string GetMaxIdLsThuePhong()
		{
			string idLsThuePhong = string.Empty;

			_conn.Open();
			SqlCommand command = new SqlCommand("SELECT TOP 1 ID_LichSuThuePhong FROM LichSuThuePhong order by ID_LichSuThuePhong DESC", _conn);
			using (SqlDataReader reader = command.ExecuteReader())
			{
				if (reader.Read())
				{
					idLsThuePhong = reader["ID_LichSuThuePhong"].ToString();
				}
			}
			_conn.Close();

			return idLsThuePhong;
		}

		#endregion

		#region Insert/Update thông tin lịch sử thuê phòng

		/// <summary>
		/// Insert/Update thông tin lịch sử thuê phòng
		/// </summary>
		/// <returns></returns>
		public bool Update(DTO_LichSuThuePhong lichSuThuePhong)
		{
			try
			{
				_conn.Open();
				string strQuery = string.Empty;
				if (lichSuThuePhong.FlagInsert == true)
				{
					strQuery = "INSERT INTO LichSuThuePhong (ID_LichSuThuePhong, ID_KhachHang, ID_Phong , SoTienThanhToan , NgayThanhToan , ThoiGianBatDauThue, ThoiGianKetThucThue, DelFlg ) VALUES('" + lichSuThuePhong.ID_LichSuThuePhong + "' , '" + lichSuThuePhong.ID_KhachHang + "' ,N'" + lichSuThuePhong.ID_Phong + "',N'" + lichSuThuePhong.SoTienThanhToan + "',N'" + lichSuThuePhong.NgayThanhToan + "', '" + lichSuThuePhong.ThoiGianBatDauThue + "','" + lichSuThuePhong.ThoiGianKetThucThue +"','" + 0 + "'); ";
				}
				else
				{
					strQuery = "UPDATE LichSuThuePhong SET ID_Phong = '" + lichSuThuePhong.ID_Phong + "',SoTienThanhToan = N'" + lichSuThuePhong.SoTienThanhToan + "', NgayThanhToan = '" + lichSuThuePhong.NgayThanhToan + "', ThoiGianBatDauThue = '" + lichSuThuePhong.ThoiGianBatDauThue + "', ThoiGianKetThucThue = '" + lichSuThuePhong.ThoiGianKetThucThue + "' WHERE ID_LichSuThuePhong = '" + lichSuThuePhong.ID_LichSuThuePhong + "'";
				}

				SqlDataAdapter dt = new SqlDataAdapter();
				dt.InsertCommand = new SqlCommand(strQuery, _conn);
				dt.InsertCommand.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				return false;
			}

			return true;
		}

		#endregion

		public bool Delete(string id)
		{
			_conn.Open();
			string strQuery = "UPDATE LichSuThuePhong SET DelFlg = 1 where ID_LichSuThuePhong = '" + id + "';";
			SqlDataAdapter dt = new SqlDataAdapter();
			dt.InsertCommand = new SqlCommand(strQuery, _conn);
			dt.InsertCommand.ExecuteNonQuery();
			return true;
		}

		public bool UpdateSoTienThanhToan(string id, string Sotien)
		{
			_conn.Open();
			string strQuery = "UPDATE LichSuThuePhong SET SoTienThanhToan = '" + Sotien + "' where ID_LichSuThuePhong = '" + id + "';";
			SqlDataAdapter dt = new SqlDataAdapter();
			dt.InsertCommand = new SqlCommand(strQuery, _conn);
			dt.InsertCommand.ExecuteNonQuery();
			return true;
		}

		public DataTable GetLichSuThuePhongByIdPhong(string Id)
		{
			SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM LichSuThuePhong WHERE ID_Phong = '" + Id + "' AND DelFlg = 0", _conn);
			DataTable dtLSTPhong = new DataTable();
			adapter.Fill(dtLSTPhong);
			return dtLSTPhong;
		}
	}
}
