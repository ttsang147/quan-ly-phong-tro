﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
	public class DAO_LoaiPhong : DBConnect
	{
		#region Get danh sách loại phòng

		/// <summary>
		/// Get danh sách loại phòng
		/// </summary>
		/// <returns></returns>
		public DataTable GetLoaiPhong()
		{
			SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM LoaiPhong", _conn);
			DataTable dtLPhong = new DataTable();
			adapter.Fill(dtLPhong);
			return dtLPhong;
		}

		#endregion

		#region Get danh sách phong theo ma phong

		/// <summary>
		/// Get danh sách phong theo ma phong
		/// </summary>
		/// <returns></returns>
		public DataTable GetLoaiPhongByID(string Id)
		{
			SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM LoaiPhong WHERE ID_LoaiPhong = {Id} AND DelFlg = 0", _conn);
			DataTable dtLPhong = new DataTable();
			adapter.Fill(dtLPhong);
			return dtLPhong;
		}

		#endregion
	}
}
