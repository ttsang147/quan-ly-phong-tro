﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
	public class DAO_SoNuoc: DBConnect
	{
		#region Update sổ nước

		/// <summary>
		/// Update sổ nước
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		public bool Update(string Idp, string Idsn, string SoCu, string SoMoi)
		{
			_conn.Open();
			string strQuery = "UPDATE SoNuoc SET SoCu = '" + SoCu + "', SoMoi = '" + SoMoi + "' WHERE ID_SoNuoc = '" + Idsn + "' AND ID_Phong ='" + Idp + "';";
			SqlDataAdapter dt = new SqlDataAdapter();
			dt.InsertCommand = new SqlCommand(strQuery, _conn);
			dt.InsertCommand.ExecuteNonQuery();
			_conn.Close();

			return true;
		}

		#endregion

		public string GetMaxIdSoNuoc()
		{
			string idSoNuoc = string.Empty;

			_conn.Open();
			SqlCommand command = new SqlCommand("SELECT TOP 1 ID_SoNuoc FROM SoNuoc order by ID_SoNuoc DESC", _conn);
			using (SqlDataReader reader = command.ExecuteReader())
			{
				if (reader.Read())
				{
					idSoNuoc = reader["ID_SoNuoc"].ToString();
				}
			}
			_conn.Close();

			return idSoNuoc;
		}

		public bool Insert(DTO_SoNuoc soNuoc)
		{
			try
			{
				_conn.Open();
				string strQuery = string.Empty;
				if (soNuoc.FlagInsert == true)
				{
					strQuery = "INSERT INTO SoNuoc (ID_SoNuoc, ID_Phong, SoCu , SoMoi , NgayBatDauSuDungNuoc , NgayKetThucSuDungNuoc, DelFlg, ID_LoaiDonGia ) VALUES('" + soNuoc.ID_SoNuoc + "' , '" + soNuoc.ID_Phong + "' ,N'" + soNuoc.SoCu + "',N'" + soNuoc.SoMoi + "',N'" + soNuoc.NgayBatDauSuDungNuoc + "', '" + soNuoc.NgayKetThucSuDungNuoc + "','" + soNuoc.DelFlg + "','" + soNuoc.ID_LoaiDonGia + "'); ";
				}

				SqlDataAdapter dt = new SqlDataAdapter();
				dt.InsertCommand = new SqlCommand(strQuery, _conn);
				dt.InsertCommand.ExecuteNonQuery();
			}
			catch (Exception)
			{
				return false;
			}

			return true;
		}

		public void Delete(string id)
		{
			_conn.Open();
			string strQuery = "UPDATE SoNuoc SET DelFlg = 1 WHERE ID_SoNuoc = '" + id + "';";
			SqlDataAdapter dt = new SqlDataAdapter();
			dt.InsertCommand = new SqlCommand(strQuery, _conn);
			dt.InsertCommand.ExecuteNonQuery();
			_conn.Close();
		}

		public void DeleteByIdPhong(string id)
		{
			_conn.Open();
			string strQuery = "UPDATE SoNuoc SET DelFlg = 1 WHERE ID_Phong = '" + id + "';";
			SqlDataAdapter dt = new SqlDataAdapter();
			dt.InsertCommand = new SqlCommand(strQuery, _conn);
			dt.InsertCommand.ExecuteNonQuery();
			_conn.Close();
		}

		public DataTable GetMSoNuocByIdPhong(string id)
		{
			SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM SoNuoc WHERE ID_Phong = '" + id + "'", _conn);
			DataTable dtLPhong = new DataTable();
			adapter.Fill(dtLPhong);
			return dtLPhong;
		}
	}
}
