﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
	public class DAO_Phong : DBConnect
	{
		#region Get danh sách phòng

		/// <summary>
		/// Get danh sách phòng
		/// </summary>
		/// <returns></returns>
		public DataTable GetPhong()
		{
			SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM Phong WHERE DelFlag = 0", _conn);
			DataTable dtPhong = new DataTable();
			adapter.Fill(dtPhong);
			return dtPhong;
		}

		#endregion

		#region Get danh sách phong theo ma phong

		/// <summary>
		/// Get danh sách phong theo ma phong
		/// </summary>
		/// <returns></returns>
		public DataTable GetPhongByID(string Id)
		{
			SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM Phong WHERE ID_Phong = " + Id + " AND DelFlag = 0", _conn);
			DataTable dtPhong = new DataTable();
			adapter.Fill(dtPhong);
			return dtPhong;
		}

		#endregion

		#region Update trạng thái

		/// <summary>
		/// Update trạng thái
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		public bool UpdateTrangThai(string Id, string tt)
		{
			_conn.Open();
			string strQuery = "UPDATE Phong SET TrangThai = '" + tt + "' WHERE ID_Phong = '" + Id + "';";
			SqlDataAdapter dt = new SqlDataAdapter();
			dt.InsertCommand = new SqlCommand(strQuery, _conn);
			dt.InsertCommand.ExecuteNonQuery();
			_conn.Close();

			return true;
		}

		#endregion

		#region

		/// <summary>
		/// 
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		public bool InsertDefault(string id)
		{
			_conn.Open();
			string strQuery = "INSERT INTO Phong (ID_Phong,ID_LoaiPhong,Gia,TrangThai,DelFlag) VALUES( '" + id + "','00','1000000','0',0);";
			SqlDataAdapter dt = new SqlDataAdapter();
			dt.InsertCommand = new SqlCommand(strQuery, _conn);
			dt.InsertCommand.ExecuteNonQuery();
			_conn.Close();

			return true;
		}

		#endregion

		public string GetMaxIDPhong()
		{
			string idPhong = string.Empty;

			_conn.Open();
			SqlCommand command = new SqlCommand("SELECT TOP 1 ID_Phong FROM Phong order by ID_Phong DESC", _conn);
			using (SqlDataReader reader = command.ExecuteReader())
			{
				if (reader.Read())
				{
					idPhong = reader["ID_Phong"].ToString();
				}
			}
			_conn.Close();

			return idPhong;
		}

		public void Update(string Id, string gia, string LoaiPhong)
		{
			_conn.Open();
			string strQuery = "UPDATE Phong SET ID_LoaiPhong = '" + LoaiPhong + "', Gia ='" + gia + "' WHERE ID_Phong = '" + Id + "';";
			SqlDataAdapter dt = new SqlDataAdapter();
			dt.InsertCommand = new SqlCommand(strQuery, _conn);
			dt.InsertCommand.ExecuteNonQuery();
			_conn.Close();

		}

		public void Delete(string Id)
		{
			_conn.Open();
			string strQuery = "UPDATE Phong SET DelFlag = 1 WHERE ID_Phong = '" + Id + "';";
			SqlDataAdapter dt = new SqlDataAdapter();
			dt.InsertCommand = new SqlCommand(strQuery, _conn);
			dt.InsertCommand.ExecuteNonQuery();
			_conn.Close();

		}
	}
}
