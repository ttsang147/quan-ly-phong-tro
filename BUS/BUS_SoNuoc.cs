﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
	public class BUS_SoNuoc
	{
		DAO_SoNuoc SoNuocDao = new DAO_SoNuoc();

		public bool Update(string Idp, string IdSn, string SoCu, string SoMoi)
		{
			return SoNuocDao.Update(Idp, IdSn, SoCu, SoMoi);
		}

		public string GetMaxIdSoNuoc()
		{
			return SoNuocDao.GetMaxIdSoNuoc();
		}

		public DataTable GetMSoNuocByIdPhong(string id)
		{
			return SoNuocDao.GetMSoNuocByIdPhong(id);
		}

		public bool Insert(DTO_SoNuoc soNuoc)
		{
			return SoNuocDao.Insert(soNuoc);
		}

		public void Delete( string id )
		{
			SoNuocDao.Delete( id );
		}

		public void DeleteByIdPhong(string id)
		{
			SoNuocDao.DeleteByIdPhong(id);
		}
	}
}
