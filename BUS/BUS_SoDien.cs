﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using DTO;
using System.Data;

namespace BUS
{
	public class BUS_SoDien
	{
		DAO_SoDien SoDienDao = new DAO_SoDien();

		public bool Update(string Idp, string IdSd, string SoCu, string SoMoi)
		{
			return SoDienDao.Update( Idp, IdSd, SoCu, SoMoi );
		}

		public DataTable GetMSoDienByIdPhong(string id)
		{
			return SoDienDao.GetMSoDienByIdPhong(id);
		}

		public string GetMaxIdSoDien()
		{
			return SoDienDao.GetMaxIdSoDien();
		}

		public bool Insert(DTO_SoDien soDien)
		{
			return SoDienDao.Insert(soDien);
		}

		public void Delete( string id )
		{
			SoDienDao.Delete(id);
		}

		public void DeleteByIdPhong(string id)
		{
			SoDienDao.DeleteByIdPhong(id);
		}
	}
}
