﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
	public class BUS_CongNo
	{
		DAO_CongNo CongNoDao = new DAO_CongNo();

		#region Get danh sách công nợ

		/// <summary>
		/// Get danh sách công nợ
		/// </summary>
		/// <returns></returns>
		public DataTable GetCongNo()
		{
			return CongNoDao.GetCongNo();
		}

		#endregion

		#region Get danh sách loại phong theo ma phong

		/// <summary>
		/// Get danh sách loại phong theo ma phong
		/// </summary>
		/// <returns></returns>
		public DataTable GetCongNoByID(string Id)
		{
			return CongNoDao.GetCongNoByID(Id);
		}

		#endregion

		#region Get danh sách loại phong theo ma phong

		/// <summary>
		/// Get danh sách loại phong theo ma phong
		/// </summary>
		/// <returns></returns>
		public DataTable GetCongNoByID(string Id_LsTp, string Id_Kh, string Id_Phong)
		{
			return CongNoDao.GetCongNoById(Id_LsTp, Id_Kh, Id_Phong);
		}

		#endregion

		#region Update trạng thái công nợ

		/// <summary>
		/// Update trạng thái công nợ
		/// </summary>
		/// <returns></returns>
		public bool UpdateTrangThaiNo(string Id, string tt)
		{
			return CongNoDao.UpdateTrangThaiNo(Id,tt);
		}

		#endregion

		#region 

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetMaxIdCongNo()
		{
			return CongNoDao.GetMaxIdCongNo();
		}

		#endregion

		#region

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public bool Update(DTO_CongNo dTO_CongNo)
		{
			return CongNoDao.Update(dTO_CongNo);
		}

		#endregion

		public bool UpdateSoTienNo(string Id, string tien)
		{
			return CongNoDao.UpdateSoTienNo(Id, tien);
		}
		public DataTable CheckCongNoByIdDienNuoc(string IdLsTp, string Id_Dien, string Id_nuoc)
		{
			return CongNoDao.CheckCongNoByIdDienNuoc(IdLsTp, Id_Dien, Id_nuoc);
		}
	}
}
