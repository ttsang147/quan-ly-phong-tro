﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
	public class BUS_LichSuThuePhong
	{
		DAO_LichSuThuePhong LichSuThuePhongDao = new DAO_LichSuThuePhong();

		#region Get danh sách lịch sử thue phòng

		/// <summary>
		/// Get danh sách lịch sử thue phòng
		/// </summary>
		/// <returns></returns>
		public DataTable GetLichSuThuePhong()
		{
			return LichSuThuePhongDao.GetLichSuThuePhong();
		}

		#endregion

		#region Get danh sách lịch sử thuê phòng theo mã lịch sử thuê phòng

		/// <summary>
		/// Get danh sách lịch sử thuê phòng theo mã lịch sử thuê phòng
		/// </summary>
		/// <returns></returns>
		public DataTable GetLichSuThuePhongByID(string Id)
		{
			return LichSuThuePhongDao.GetLichSuThuePhongByID(Id);
		}

		#endregion

		#region Get id lịch sử thuê phòng lớn nhất

		/// <summary>
		/// Get id lịch sử thuê phòng lớn nhất
		/// </summary>
		/// <returns></returns>
		public string GetMaxIdLsThuePhong()
		{
			return LichSuThuePhongDao.GetMaxIdLsThuePhong();
		}

		#endregion

		#region Insert/Update thông tin lịch sử thuê phòng

		/// <summary>
		/// Insert/Update thông tin lịch sử thuê phòng
		/// </summary>
		/// <returns></returns>
		public bool Update(DTO_LichSuThuePhong lichSuThuePhong)
		{
			return LichSuThuePhongDao.Update(lichSuThuePhong);
		}

		#endregion

		public bool Delete(string id)
		{
			return LichSuThuePhongDao.Delete(id);
		}

		public bool UpdateSoTienThanhToan(string id, string Sotien)
		{
			return LichSuThuePhongDao.UpdateSoTienThanhToan(id, Sotien);
		}

		public DataTable GetLichSuThuePhongByIdPhong(string Id)
		{
			return LichSuThuePhongDao.GetLichSuThuePhongByIdPhong(Id);
		}
	}
}
