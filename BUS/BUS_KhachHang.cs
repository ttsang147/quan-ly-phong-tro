﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using System.Data;
using DTO;

namespace BUS
{
	public class BUS_KhachHang
	{
		DAO_KhachHang khDao = new DAO_KhachHang();

		#region Get danh sách khách hàng

		/// <summary>
		/// Get danh sách khách hàng
		/// </summary>
		/// <returns></returns>
		public DataTable GetKhachHang()
		{
			return khDao.GetKhachHang();
		}

		#endregion

		#region Get danh sách khách hàng theo ID khách háng

		/// <summary>
		/// Get danh sách khách hàng theo ID khách háng
		/// </summary>
		/// <returns></returns>
		public DataTable GetKhachHangByID(string Id)
		{
			return khDao.GetKhachHangByID(Id);
		}

		#endregion

		#region Get danh sách khách hàng theo ID khách hang

		/// <summary>
		/// Get danh sách khách hàng theo ID khách hang
		/// </summary>
		/// <returns></returns>
		public string GetMaxIdKhachHang()
		{
			return khDao.GetMaxIdKhachHang();
		}

		#endregion

		#region Insert thông tin khách hàng

		/// <summary>
		/// Insert thông tin khách hàng
		/// </summary>
		/// <returns></returns>
		public bool Update(DTO_KhachHang dTO_KhachHang)
		{
			return khDao.Update(dTO_KhachHang);
		}

		#endregion

		public bool Delete(string id)
		{
			return khDao.Delete(id);
		}

		public DataTable GetKhachHangByIdPhong(string Id)
		{
			return khDao.GetKhachHangByIdPhong(Id);
		}
	}
}
