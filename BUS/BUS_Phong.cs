﻿using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
	public class BUS_Phong
	{
		DAO_Phong PhongDao = new DAO_Phong();

		#region Get danh sách phòng

		/// <summary>
		/// Get danh sách phòng
		/// </summary>
		/// <returns></returns>
		public DataTable GetPhong()
		{
			return PhongDao.GetPhong();
		}

		#endregion

		#region Get danh sách phong theo ma phong

		/// <summary>
		/// Get danh sách phong theo ma phong
		/// </summary>
		/// <returns></returns>
		public DataTable GetPhongByID(string Id)
		{
			return PhongDao.GetPhongByID(Id);
		}

		#endregion

		#region Update trạng thái

		/// <summary>
		/// Update trạng thái
		/// </summary>
		/// <returns></returns>
		public bool UpdateTrangThai(string Id, string tt)
		{
			return PhongDao.UpdateTrangThai(Id, tt);
		}

		#endregion

		public string GetMaxIDPhong()
		{
			return PhongDao.GetMaxIDPhong();
		}

		public bool Insert(string Id)
		{
			return PhongDao.InsertDefault(Id);
		}

		public void Update(string Id, string gia, string LoaiPhong)
		{
			PhongDao.Update(Id, gia, LoaiPhong);
		}

		public void Delete(string Id)
		{
			PhongDao.Delete(Id);
		}
	}
}
