﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
	public class BUS_LichSuThanhToan
	{
		DAO_LichSuThanhToan lichSuThanhToan = new DAO_LichSuThanhToan();

		#region Insert/Update thông tin lịch sử thuê phòng

		/// <summary>
		/// Insert/Update thông tin lịch sử thuê phòng
		/// </summary>
		/// <returns></returns>
		public bool Update(DTO_LichSuThanhToan lichSuThuePhong)
		{
			return lichSuThanhToan.Update(lichSuThuePhong);
		}

		#endregion

		#region Get id lịch sử thanht toán lớn nhất

		/// <summary>
		/// Get id lịch sử thanh toán lớn nhất
		/// </summary>
		/// <returns></returns>
		public string GetMaxIdLsThanhToan()
		{
			return lichSuThanhToan.GetMaxIdLsThanhToan();
		}

		#endregion
	}
}
