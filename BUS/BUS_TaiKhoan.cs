﻿using DAO;
using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
	public class BUS_TaiKhoan
	{
		DAO_TaiKhoan TaiKhoangDao = new DAO_TaiKhoan();

		#region Get danh sách tài khoản by name

		/// <summary>
		/// Get danh sách tài khoản by name
		/// </summary>
		/// <returns></returns>
		public DataTable GetTaiKhoanbyName(string key)
		{
			return TaiKhoangDao.GetTaiKhoanbyName(key);
		}

		#endregion

		#region Get danh sách tài khoản

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public DataTable GetTaiKhoan()
		{
			return TaiKhoangDao.GetTaiKhoan();
		}

		#endregion

		#region

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetMaxId()
		{
			return TaiKhoangDao.GetMaxId();
		}

		#endregion

		#region Get tài khoản theo ID

		/// <summary>
		/// Get tài khoản theo ID
		/// </summary>
		/// <returns></returns>
		public DataTable GetTaiKhoanByID(string Id)
		{
			return TaiKhoangDao.GetTaiKhoanByID(Id);
		}

		#endregion

		#region Get tài khoản login

		/// <summary>
		/// Get tài khoản login
		/// </summary>
		/// <returns></returns>
		public DataTable GetLogin(string Id, string Pass)
		{
			return TaiKhoangDao.GetLogin(Id,Pass);
		}

		#endregion

		#region Update tài khoản

		/// <summary>
		///  Update tài khoản
		/// </summary>
		/// <returns></returns>
		public bool Update(DTO_TaiKhoan tk)
		{
			return TaiKhoangDao.Update(tk);
		}

		#endregion

		public bool Delete(string id)
		{
			return TaiKhoangDao.Delete(id);
		}

	}
}
