﻿using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
	public class BUS_LoaiPhong
	{
		DAO_LoaiPhong LoaiPhongDao = new DAO_LoaiPhong();

		#region Get danh sách loại phòng

		/// <summary>
		/// Get danh sách loại phòng
		/// </summary>
		/// <returns></returns>
		public DataTable GetLoaiPhong()
		{
			return LoaiPhongDao.GetLoaiPhong();
		}

		#endregion

		#region Get danh sách loại phong theo ma phong

		/// <summary>
		/// Get danh sách loại phong theo ma phong
		/// </summary>
		/// <returns></returns>
		public DataTable GetLoaiPhongByID(string Id)
		{
			return LoaiPhongDao.GetLoaiPhongByID(Id);
		}

		#endregion
	}
}
