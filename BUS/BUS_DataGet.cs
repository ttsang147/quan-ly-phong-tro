﻿using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
	public class BUS_DataGet
	{
		DAO_DataGet dataGetDao = new DAO_DataGet();

		#region Get danh sách đối tượng dùng cho trang chủ

		/// <summary>
		/// Get danh sách đối tượng dùng cho trang chủ
		/// </summary>
		/// <returns></returns>
		public DataTable GetDataTagetUseTrangChu(string searchStr, string trangThaiPhong, string trangThaiPhi)
		{
			return dataGetDao.GetDataTagetUseTrangChu(searchStr, trangThaiPhong, trangThaiPhi);
		}

		#endregion

		#region Get thông tin khách hàng

		/// <summary>
		/// Get thông tin khách hàng
		/// </summary>
		/// <returns></returns>
		public DataTable GetDataKhachHangInf(string searchStr)
		{
			return dataGetDao.GetDataKhachHangInf(searchStr);
		}

		#endregion

		#region Get thông tin điện nước

		/// <summary>
		/// Get thông tin điện nước
		/// </summary>
		/// <returns></returns>
		public DataTable GetDataDienNuocInf(string phonng, string tt)
		{
			return dataGetDao.GetDataDienNuocInf(phonng, tt);
		}

		#endregion

		public DataTable GetDataTinhTienInf(string search, string tt)
		{
			return dataGetDao.GetDataTinhTienInf(search, tt);
		}

		public DataTable GetDataBaoCao(string search, string tt, string ngay)
		{
			return dataGetDao.GetDataBaoCao( search, tt, ngay);
		}

		public DataTable GetDataSoDien(string idPgong)
		{
			return dataGetDao.GetDataSoDien(idPgong);
		}

		public DataTable GetDataSoNuoc(string idPgong)
		{
			return dataGetDao.GetDataSoNuoc(idPgong);
		}
	}
}
