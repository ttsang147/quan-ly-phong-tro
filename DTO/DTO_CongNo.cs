﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
	public class DTO_CongNo
	{
		private string _ID_CongNo;
		private string _ID_KhachHang;
		private string _ID_Phong;
		private string _ThoiGianBatDau;
		private string _ThoiGianKetThuc;
		private string _TrangThaiNo;
		private int _DelFlg;
		private string _ID_LichSuThuePhong;
		private string _SoTienNo;
		private string _ID_SoDien;
		private string _ID_SoNuoc;
		private bool _FlagInsert;

		/* ======== GETTER/SETTER ======== */
		public string ID_CongNo
			{
				get
				{
					return _ID_CongNo;
				}

				set
				{
					_ID_CongNo = value;
				}
			}

		public string ID_KhachHang
		{
			get
			{
				return _ID_KhachHang;
			}

			set
			{
				_ID_KhachHang = value;
			}
		}

		public string ID_Phong
		{
			get
			{
				return _ID_Phong;
			}

			set
			{
				_ID_Phong = value;
			}
		}

		public string ThoiGianBatDau
		{
			get
			{
				return _ThoiGianBatDau;
			}

			set
			{
				_ThoiGianBatDau = value;
			}
		}

		public string ThoiGianKetThuc
		{
			get
			{
				return _ThoiGianKetThuc;
			}

			set
			{
				_ThoiGianKetThuc = value;
			}
		}

		public string TrangThaiNo
		{
			get
			{
				return _TrangThaiNo;
			}

			set
			{
				_TrangThaiNo = value;
			}
		}

		public int DelFlg
		{
			get
			{
				return _DelFlg;
			}

			set
			{
				_DelFlg = value;
			}
		}

		public string ID_LichSuThuePhong
		{
			get
			{
				return _ID_LichSuThuePhong;
			}

			set
			{
				_ID_LichSuThuePhong = value;
			}
		}

		public string SoTienNo
		{
			get
			{
				return _SoTienNo;
			}

			set
			{
				_SoTienNo = value;
			}
		}

		public string ID_SoDien
		{
			get
			{
				return _ID_SoDien;
			}

			set
			{
				_ID_SoDien = value;
			}
		}

		public string ID_SoNuoc
		{
			get
			{
				return _ID_SoNuoc;
			}

			set
			{
				_ID_SoNuoc = value;
			}
		}

		public bool FlagInsert
		{
			get
			{
				return _FlagInsert;
			}

			set
			{
				_FlagInsert = value;
			}
		}
		
		/* === Constructor === */
		public DTO_CongNo()
		{

		}

		public DTO_CongNo(string id, string idKh, string idp, string tgbd, string tgkt, string ttn, int flag, string idlstp, bool flagIns, string soTienNo, string idsodien, string idsonuoc)
		{
			this.ID_CongNo = id;
			this.ID_KhachHang = idKh;
			this.ID_Phong = idp;
			this.ThoiGianBatDau = tgbd;
			this.ThoiGianKetThuc = tgkt;
			this._TrangThaiNo = ttn;
			this.DelFlg = flag;
			this.ID_LichSuThuePhong = idlstp;
			this.FlagInsert = flagIns;
			this.SoTienNo = soTienNo;
			this.ID_SoDien = idsodien;
			this.ID_SoNuoc = idsonuoc;
		}
	}
}
