﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
	public class DTO_LichSuThuePhong
	{
		private string _ID_LichSuThuePhong;
		private string _ID_KhachHang;
		private string _ID_Phong;
		private string _SoTienThanhToan;
		private string _NgayThanhToan;
		private string _ThoiGianBatDauThue;
		private string _ThoiGianKetThucThue;
		private int _DelFlg;
		private bool _FlagInsert;

		/* ======== GETTER/SETTER ======== */
		public string ID_LichSuThuePhong
		{
			get
			{
				return _ID_LichSuThuePhong;
			}

			set
			{
				_ID_LichSuThuePhong = value;
			}
		}

		public string ID_KhachHang
		{
			get
			{
				return _ID_KhachHang;
			}

			set
			{
				_ID_KhachHang = value;
			}
		}

		public string ID_Phong
		{
			get
			{
				return _ID_Phong;
			}

			set
			{
				_ID_Phong = value;
			}
		}

		public string SoTienThanhToan
		{
			get
			{
				return _SoTienThanhToan;
			}

			set
			{
				_SoTienThanhToan = value;
			}
		}

		public string NgayThanhToan
		{
			get
			{
				return _NgayThanhToan;
			}

			set
			{
				_NgayThanhToan = value;
			}
		}

		public string ThoiGianBatDauThue
		{
			get
			{
				return _ThoiGianBatDauThue;
			}

			set
			{
				_ThoiGianBatDauThue = value;
			}
		}

		public string ThoiGianKetThucThue
		{
			get
			{
				return _ThoiGianKetThucThue;
			}

			set
			{
				_ThoiGianKetThucThue = value;
			}
		}

		public int DelFlg
		{
			get
			{
				return _DelFlg;
			}

			set
			{
				_DelFlg = value;
			}
		}

		public bool FlagInsert
		{
			get
			{
				return _FlagInsert;
			}

			set
			{
				_FlagInsert = value;
			}
		}

		/* === Constructor === */
		public DTO_LichSuThuePhong()
		{

		}

		public DTO_LichSuThuePhong(string id, string idkh, string idp, string sttt, string ntt, string tgbdt, string tgktt, int Flg, bool flgIns)
		{
			this.ID_LichSuThuePhong = id;
			this.ID_KhachHang = idkh;
			this.ID_Phong = idp;
			this.SoTienThanhToan = sttt;
			this.NgayThanhToan = ntt;
			this.ThoiGianBatDauThue = tgbdt;
			this.ThoiGianKetThucThue = tgktt;
			this.DelFlg = Flg;
			this.FlagInsert = flgIns;
		}
	}
}
