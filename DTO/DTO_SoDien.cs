﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
	public class DTO_SoDien
	{
		private string _ID_SoDien;
		private string _ID_Phong;
		private string _SoCu;
		private string _SoMoi;
		private string _NgayBatDauSuDungDien;
		private string _NgayKetThucSuDungDien;
		private int _DelFlg;
		private string _ID_LoaiDonGia;
		private bool _FlagInsert;

		/* ======== GETTER/SETTER ======== */
		public string ID_SoDien
			{
				get
				{
					return _ID_SoDien;
				}

				set
				{
					_ID_SoDien = value;
				}
			}

		public string ID_Phong
		{
			get
			{
				return _ID_Phong;
			}

			set
			{
				_ID_Phong = value;
			}
		}

		public string SoCu
		{
			get
			{
				return _SoCu;
			}

			set
			{
				_SoCu = value;
			}
		}

		public string SoMoi
		{
			get
			{
				return _SoMoi;
			}

			set
			{
				_SoMoi = value;
			}
		}

		public int DelFlg
		{
			get
			{
				return _DelFlg;
			}

			set
			{
				_DelFlg = value;
			}
		}

		public string NgayBatDauSuDungDien
		{
			get
			{
				return _NgayBatDauSuDungDien;
			}

			set
			{
				_NgayBatDauSuDungDien = value;
			}
		}

		public string NgayKetThucSuDungDien
		{
			get
			{
				return _NgayKetThucSuDungDien;
			}

			set
			{
				_NgayKetThucSuDungDien = value;
			}
		}

		public bool FlagInsert
		{
			get
			{
				return _FlagInsert;
			}

			set
			{
				_FlagInsert = value;
			}
		}

		public string ID_LoaiDonGia
		{
			get
			{
				return _ID_LoaiDonGia;
			}

			set
			{
				_ID_LoaiDonGia = value;
			}
		}
		
		/* === Constructor === */
		public DTO_SoDien()
		{

		}

		public DTO_SoDien(string id, string idp, string Socu, string somoi, int Flg, string ngatbatdau, string ngaykethuc, string idLdg, bool flagIns)
		{
			this.ID_SoDien = id;
			this.ID_Phong = idp;
			this.SoCu = Socu;
			this.SoMoi = somoi;
			this.DelFlg = Flg;
			this.NgayBatDauSuDungDien = ngatbatdau;
			this.NgayKetThucSuDungDien = ngaykethuc;
			this.ID_LoaiDonGia = idLdg;
			this.FlagInsert = flagIns;
		}
	}
}
