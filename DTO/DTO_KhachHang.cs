﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
	public class DTO_KhachHang
	{
		private string _ID_KhachHang;
		private string _TenKhachHang;
		private string _CMND;
		private string _SoDienThoai;
		private int _DelFlg;
		private string _DiaChi;
		private bool _FlagInsert;

		/* ======== GETTER/SETTER ======== */
		public string ID_KhachHang
			{
				get
				{
					return _ID_KhachHang;
				}

				set
				{
					_ID_KhachHang = value;
				}
			}

		public string TenKhachHang
		{
			get
			{
				return _TenKhachHang;
			}

			set
			{
				_TenKhachHang = value;
			}
		}

		public string CMND
		{
			get
			{
				return _CMND;
			}

			set
			{
				_CMND = value;
			}
		}

		public string SoDienThoai
		{
			get
			{
				return _SoDienThoai;
			}

			set
			{
				_SoDienThoai = value;
			}
		}

		public int DelFlg
		{
			get
			{
				return _DelFlg;
			}

			set
			{
				_DelFlg = value;
			}
		}

		public string DiaChi
		{
			get
			{
				return _DiaChi;
			}

			set
			{
				_DiaChi = value;
			}
		}

		public bool FlagInsert
		{
			get
			{
				return _FlagInsert;
			}

			set
			{
				_FlagInsert = value;
			}
		}
		
		/* === Constructor === */
		public DTO_KhachHang()
		{

		}

		public DTO_KhachHang(string id, string name, string cmnd, string phone, int Flg, string diaChi, bool flagIns)
		{
			this.ID_KhachHang = id;
			this.TenKhachHang = name;
			this.CMND = cmnd;
			this.SoDienThoai = phone;
			this.DelFlg = Flg;
			this.DiaChi = diaChi;
			this.FlagInsert = flagIns;
		}
	}
}
