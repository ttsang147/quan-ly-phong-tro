﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
	public class DTO_SoNuoc
	{
		private string _ID_SoNuoc;
		private string _ID_Phong;
		private string _SoCu;
		private string _SoMoi;
		private string _NgayBatDauSuDungNuoc;
		private string _NgayKetThucSuDungNuoc;
		private int _DelFlg;
		private string _ID_LoaiDonGia;
		private bool _FlagInsert;

		/* ======== GETTER/SETTER ======== */
		public string ID_SoNuoc
			{
				get
				{
					return _ID_SoNuoc;
				}

				set
				{
					_ID_SoNuoc = value;
				}
			}

		public string ID_Phong
		{
			get
			{
				return _ID_Phong;
			}

			set
			{
				_ID_Phong = value;
			}
		}

		public string SoCu
		{
			get
			{
				return _SoCu;
			}

			set
			{
				_SoCu = value;
			}
		}

		public string SoMoi
		{
			get
			{
				return _SoMoi;
			}

			set
			{
				_SoMoi = value;
			}
		}

		public int DelFlg
		{
			get
			{
				return _DelFlg;
			}

			set
			{
				_DelFlg = value;
			}
		}

		public string NgayBatDauSuDungNuoc
		{
			get
			{
				return _NgayBatDauSuDungNuoc;
			}

			set
			{
				_NgayBatDauSuDungNuoc = value;
			}
		}

		public string NgayKetThucSuDungNuoc
		{
			get
			{
				return _NgayKetThucSuDungNuoc;
			}

			set
			{
				_NgayKetThucSuDungNuoc = value;
			}
		}

		public bool FlagInsert
		{
			get
			{
				return _FlagInsert;
			}

			set
			{
				_FlagInsert = value;
			}
		}

		public string ID_LoaiDonGia
		{
			get
			{
				return _ID_LoaiDonGia;
			}

			set
			{
				_ID_LoaiDonGia = value;
			}
		}
		
		/* === Constructor === */
		public DTO_SoNuoc()
		{

		}

		public DTO_SoNuoc(string id, string idp, string Socu, string somoi, int Flg, string ngatbatdau, string ngaykethuc, string idLdg, bool flagIns)
		{
			this.ID_SoNuoc= id;
			this.ID_Phong = idp;
			this.SoCu = Socu;
			this.SoMoi = somoi;
			this.DelFlg = Flg;
			this.NgayBatDauSuDungNuoc= ngatbatdau;
			this.NgayKetThucSuDungNuoc = ngaykethuc;
			this.ID_LoaiDonGia = idLdg;
			this.FlagInsert = flagIns;
		}
	}
}
