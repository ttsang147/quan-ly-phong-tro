﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
	public class DTO_LichSuThanhToan
	{
		private string _ID_ThanhToan;
		private string _ID_LichSuThuePhong;
		private string _ID_SoDien;
		private string _ID_SoNuoc;
		private string _KyThanhToan;
		private string _SoTienThanhToan;
		private bool _FlagInsert;

		/* ======== GETTER/SETTER ======== */
		public string ID_ThanhToan
			{
				get
				{
					return _ID_ThanhToan;
				}

				set
				{
					_ID_ThanhToan = value;
				}
			}

		public string ID_LichSuThuePhong
		{
			get
			{
				return _ID_LichSuThuePhong;
			}

			set
			{
				_ID_LichSuThuePhong = value;
			}
		}

		public string ID_SoDien
		{
			get
			{
				return _ID_SoDien;
			}

			set
			{
				_ID_SoDien = value;
			}
		}

		public string ID_SoNuoc
		{
			get
			{
				return _ID_SoNuoc;
			}

			set
			{
				_ID_SoNuoc = value;
			}
		}

		public string KyThanhToan
		{
			get
			{
				return _KyThanhToan;
			}

			set
			{
				_KyThanhToan = value;
			}
		}

		public string SoTienThanhToan
		{
			get
			{
				return _SoTienThanhToan;
			}

			set
			{
				_SoTienThanhToan = value;
			}
		}

		public bool FlagInsert
		{
			get
			{
				return _FlagInsert;
			}

			set
			{
				_FlagInsert = value;
			}
		}
		
		/* === Constructor === */
		public DTO_LichSuThanhToan()
		{

		}

		public DTO_LichSuThanhToan(string id, string idlstt, string idsd, string idsn, string ktt, string sttt, bool flagIns)
		{
			this.ID_ThanhToan = id;
			this.ID_LichSuThuePhong = idlstt;
			this.ID_SoDien = idsd;
			this.ID_SoNuoc = idsn;
			this.KyThanhToan = ktt;
			this.SoTienThanhToan = sttt;
			this.FlagInsert = flagIns;
		}
	}
}
