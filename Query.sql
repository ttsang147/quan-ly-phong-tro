USE [QLPhongXomTro]
GO
/****** Object:  Table [dbo].[CongNo]    Script Date: 12/24/2023 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CongNo](
	[ID_CongNo] [nvarchar](50) NOT NULL,
	[ID_KhachHang] [nvarchar](50) NULL,
	[ID_Phong] [nvarchar](50) NULL,
	[ThoiGianBatDau] [datetime] NULL,
	[ThoiGianKetThuc] [datetime] NULL,
	[TrangThaiNo] [nvarchar](50) NULL,
	[DelFlg] [tinyint] NULL,
	[ID_LichSuThuePhong] [nvarchar](50) NULL,
	[SoTienNo] [int] NULL,
	[ID_SoDien] [nvarchar](50) NULL,
	[ID_SoNuoc] [nvarchar](50) NULL,
 CONSTRAINT [PK_CongNo] PRIMARY KEY CLUSTERED 
(
	[ID_CongNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DonGia]    Script Date: 12/24/2023 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DonGia](
	[ID_DonGia] [nvarchar](50) NOT NULL,
	[ID_LoaiDonGia] [nvarchar](50) NULL,
	[DonGia] [nvarchar](50) NULL,
 CONSTRAINT [PK_DonGia] PRIMARY KEY CLUSTERED 
(
	[ID_DonGia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[KhachHang]    Script Date: 12/24/2023 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KhachHang](
	[ID_KhachHang] [nvarchar](50) NOT NULL,
	[TenKhachHang] [nvarchar](50) NULL,
	[CCCD] [nvarchar](50) NULL,
	[SoDienThoai] [nvarchar](50) NULL,
	[DelFlg] [tinyint] NULL,
	[DiaChi] [nvarchar](500) NULL DEFAULT (''),
PRIMARY KEY CLUSTERED 
(
	[ID_KhachHang] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LichSuThanhToan]    Script Date: 12/24/2023 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LichSuThanhToan](
	[ID_ThanhToan] [nvarchar](50) NOT NULL,
	[ID_LichSuThuePhong] [nvarchar](50) NULL,
	[ID_SoDien] [nvarchar](50) NULL,
	[ID_SoNuoc] [nvarchar](50) NULL,
	[KyThanhToan] [nvarchar](50) NULL,
	[SoTienThanhToan] [nvarchar](50) NULL,
 CONSTRAINT [PK_LichSuThanhToan] PRIMARY KEY CLUSTERED 
(
	[ID_ThanhToan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LichSuThuePhong]    Script Date: 12/24/2023 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LichSuThuePhong](
	[ID_LichSuThuePhong] [nvarchar](50) NOT NULL,
	[ID_KhachHang] [nvarchar](50) NULL,
	[ID_Phong] [nvarchar](50) NULL,
	[SoTienThanhToan] [nvarchar](50) NULL,
	[NgayThanhToan] [datetime] NULL,
	[ThoiGianBatDauThue] [datetime] NULL,
	[ThoiGianKetThucThue] [datetime] NULL,
	[DelFlg] [tinyint] NULL,
 CONSTRAINT [PK_LichSuThuePhong] PRIMARY KEY CLUSTERED 
(
	[ID_LichSuThuePhong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiPhong]    Script Date: 12/24/2023 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiPhong](
	[ID_LoaiPhong] [nvarchar](50) NOT NULL,
	[TenLoaiPhong] [nvarchar](50) NULL,
	[DelFlg] [tinyint] NULL,
 CONSTRAINT [PK_LoaiPhong] PRIMARY KEY CLUSTERED 
(
	[ID_LoaiPhong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Phong]    Script Date: 12/24/2023 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Phong](
	[ID_Phong] [nvarchar](50) NOT NULL,
	[ID_LoaiPhong] [nvarchar](50) NULL,
	[Gia] [nvarchar](50) NULL,
	[TrangThai] [tinyint] NULL,
	[DelFlag] [tinyint] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_Phong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SoDien]    Script Date: 12/24/2023 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SoDien](
	[ID_SoDien] [nvarchar](50) NOT NULL,
	[ID_Phong] [nvarchar](50) NULL,
	[SoCu] [nvarchar](50) NULL,
	[SoMoi] [nvarchar](50) NULL,
	[NgayBatDauSuDungDien] [nvarchar](50) NULL,
	[NgayKetThucSuDungDien] [nvarchar](50) NULL,
	[DelFlg] [tinyint] NULL,
	[ID_LoaiDonGia] [nvarchar](50) NULL DEFAULT ('00'),
 CONSTRAINT [PK_SoDien] PRIMARY KEY CLUSTERED 
(
	[ID_SoDien] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SoNuoc]    Script Date: 12/24/2023 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SoNuoc](
	[ID_SoNuoc] [nvarchar](50) NOT NULL,
	[ID_Phong] [nvarchar](50) NULL,
	[SoCu] [nvarchar](50) NULL,
	[SoMoi] [nvarchar](50) NULL,
	[NgayBatDauSuDungNuoc] [nvarchar](50) NULL,
	[NgayKetThucSuDungNuoc] [nvarchar](50) NULL,
	[DelFlg] [tinyint] NULL,
	[ID_LoaiDonGia] [nvarchar](50) NULL CONSTRAINT [DF_SoNuoc_ID_LoaiDonGia]  DEFAULT ('00'),
 CONSTRAINT [PK_SoNuoc] PRIMARY KEY CLUSTERED 
(
	[ID_SoNuoc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TaiKhoan]    Script Date: 12/24/2023 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaiKhoan](
	[ID_TaiKhoan] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NULL,
	[TenTaiKhoan] [nvarchar](50) NULL,
	[Quyen] [int] NULL,
	[DelFlg] [tinyint] NULL,
 CONSTRAINT [PK_TaiKhoang] PRIMARY KEY CLUSTERED 
(
	[ID_TaiKhoan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[CongNo] ([ID_CongNo], [ID_KhachHang], [ID_Phong], [ThoiGianBatDau], [ThoiGianKetThuc], [TrangThaiNo], [DelFlg], [ID_LichSuThuePhong], [SoTienNo], [ID_SoDien], [ID_SoNuoc]) VALUES (N'00001', N'00001', N'003', CAST(N'2023-12-23 21:59:44.000' AS DateTime), CAST(N'2023-12-23 21:59:35.000' AS DateTime), N'0', 0, N'00001', 15057500, N'00001', N'00001')
INSERT [dbo].[CongNo] ([ID_CongNo], [ID_KhachHang], [ID_Phong], [ThoiGianBatDau], [ThoiGianKetThuc], [TrangThaiNo], [DelFlg], [ID_LichSuThuePhong], [SoTienNo], [ID_SoDien], [ID_SoNuoc]) VALUES (N'00002', N'00002', N'004', CAST(N'2023-12-23 22:00:43.000' AS DateTime), CAST(N'2023-12-23 22:00:21.000' AS DateTime), N'0', 0, N'00002', 57500, N'00002', N'00002')
INSERT [dbo].[CongNo] ([ID_CongNo], [ID_KhachHang], [ID_Phong], [ThoiGianBatDau], [ThoiGianKetThuc], [TrangThaiNo], [DelFlg], [ID_LichSuThuePhong], [SoTienNo], [ID_SoDien], [ID_SoNuoc]) VALUES (N'00003', N'00003', N'005', CAST(N'2023-12-23 22:01:30.000' AS DateTime), CAST(N'2023-12-23 22:01:18.000' AS DateTime), N'0', 0, N'00003', 1115000, N'00003', N'00003')
INSERT [dbo].[CongNo] ([ID_CongNo], [ID_KhachHang], [ID_Phong], [ThoiGianBatDau], [ThoiGianKetThuc], [TrangThaiNo], [DelFlg], [ID_LichSuThuePhong], [SoTienNo], [ID_SoDien], [ID_SoNuoc]) VALUES (N'00004', N'00004', N'005', CAST(N'2023-12-23 22:19:20.000' AS DateTime), CAST(N'2023-10-31 22:18:59.000' AS DateTime), N'1', 0, N'00004', 1000000, N'00004', N'00004')
INSERT [dbo].[DonGia] ([ID_DonGia], [ID_LoaiDonGia], [DonGia]) VALUES (N'00', N'00', N'1500')
INSERT [dbo].[DonGia] ([ID_DonGia], [ID_LoaiDonGia], [DonGia]) VALUES (N'01', N'01', N'10000')
INSERT [dbo].[KhachHang] ([ID_KhachHang], [TenKhachHang], [CCCD], [SoDienThoai], [DelFlg], [DiaChi]) VALUES (N'00001', N'1', N'1', N'1', 0, N'1222')
INSERT [dbo].[KhachHang] ([ID_KhachHang], [TenKhachHang], [CCCD], [SoDienThoai], [DelFlg], [DiaChi]) VALUES (N'00002', N'2', N'2', N'2', 1, N'2')
INSERT [dbo].[KhachHang] ([ID_KhachHang], [TenKhachHang], [CCCD], [SoDienThoai], [DelFlg], [DiaChi]) VALUES (N'00003', N'3', N'3', N'3', 1, N'3')
INSERT [dbo].[KhachHang] ([ID_KhachHang], [TenKhachHang], [CCCD], [SoDienThoai], [DelFlg], [DiaChi]) VALUES (N'00004', N'2', N'2', N'2', 0, N'3')
INSERT [dbo].[LichSuThanhToan] ([ID_ThanhToan], [ID_LichSuThuePhong], [ID_SoDien], [ID_SoNuoc], [KyThanhToan], [SoTienThanhToan]) VALUES (N'00001', N'00002', N'00002', N'00002', N'', N'1000000 ')
INSERT [dbo].[LichSuThanhToan] ([ID_ThanhToan], [ID_LichSuThuePhong], [ID_SoDien], [ID_SoNuoc], [KyThanhToan], [SoTienThanhToan]) VALUES (N'00002', N'00001', N'00001', N'00001', N'', N'15057500 ')
INSERT [dbo].[LichSuThanhToan] ([ID_ThanhToan], [ID_LichSuThuePhong], [ID_SoDien], [ID_SoNuoc], [KyThanhToan], [SoTienThanhToan]) VALUES (N'00003', N'00002', N'00002', N'00002', N'', N'57500 ')
INSERT [dbo].[LichSuThanhToan] ([ID_ThanhToan], [ID_LichSuThuePhong], [ID_SoDien], [ID_SoNuoc], [KyThanhToan], [SoTienThanhToan]) VALUES (N'00004', N'00003', N'00003', N'00003', N'', N'1115000 ')
INSERT [dbo].[LichSuThuePhong] ([ID_LichSuThuePhong], [ID_KhachHang], [ID_Phong], [SoTienThanhToan], [NgayThanhToan], [ThoiGianBatDauThue], [ThoiGianKetThucThue], [DelFlg]) VALUES (N'00001', N'00001', N'003', N'15000000 ', CAST(N'1900-01-01 00:00:00.000' AS DateTime), CAST(N'2023-12-18 11:52:00.000' AS DateTime), CAST(N'2023-12-23 21:59:35.000' AS DateTime), 0)
INSERT [dbo].[LichSuThuePhong] ([ID_LichSuThuePhong], [ID_KhachHang], [ID_Phong], [SoTienThanhToan], [NgayThanhToan], [ThoiGianBatDauThue], [ThoiGianKetThucThue], [DelFlg]) VALUES (N'00002', N'00002', N'004', N'1000000 ', CAST(N'2023-12-23 22:00:30.000' AS DateTime), CAST(N'2023-12-23 22:00:21.000' AS DateTime), CAST(N'2023-12-23 22:00:21.000' AS DateTime), 1)
INSERT [dbo].[LichSuThuePhong] ([ID_LichSuThuePhong], [ID_KhachHang], [ID_Phong], [SoTienThanhToan], [NgayThanhToan], [ThoiGianBatDauThue], [ThoiGianKetThucThue], [DelFlg]) VALUES (N'00003', N'00003', N'005', N'1000000 ', CAST(N'1900-01-01 00:00:00.000' AS DateTime), CAST(N'2023-12-23 22:01:18.000' AS DateTime), CAST(N'2023-12-23 22:01:18.000' AS DateTime), 1)
INSERT [dbo].[LichSuThuePhong] ([ID_LichSuThuePhong], [ID_KhachHang], [ID_Phong], [SoTienThanhToan], [NgayThanhToan], [ThoiGianBatDauThue], [ThoiGianKetThucThue], [DelFlg]) VALUES (N'00004', N'00004', N'005', N'1000000 ', CAST(N'1900-01-01 00:00:00.000' AS DateTime), CAST(N'2023-07-04 11:52:00.000' AS DateTime), CAST(N'2023-10-31 22:18:59.000' AS DateTime), 0)
INSERT [dbo].[LoaiPhong] ([ID_LoaiPhong], [TenLoaiPhong], [DelFlg]) VALUES (N'00', N'Thường', 0)
INSERT [dbo].[LoaiPhong] ([ID_LoaiPhong], [TenLoaiPhong], [DelFlg]) VALUES (N'01', N'Điều hòa', 0)
INSERT [dbo].[LoaiPhong] ([ID_LoaiPhong], [TenLoaiPhong], [DelFlg]) VALUES (N'02', N'Đầy đủ nội thất', 0)
INSERT [dbo].[Phong] ([ID_Phong], [ID_LoaiPhong], [Gia], [TrangThai], [DelFlag]) VALUES (N'003', N'02', N'15000000', 1, 0)
INSERT [dbo].[Phong] ([ID_Phong], [ID_LoaiPhong], [Gia], [TrangThai], [DelFlag]) VALUES (N'004', N'01', N'1000000', 0, 0)
INSERT [dbo].[Phong] ([ID_Phong], [ID_LoaiPhong], [Gia], [TrangThai], [DelFlag]) VALUES (N'005', N'00', N'1000000', 1, 0)
INSERT [dbo].[Phong] ([ID_Phong], [ID_LoaiPhong], [Gia], [TrangThai], [DelFlag]) VALUES (N'006', N'00', N'1000000', 0, 0)
INSERT [dbo].[Phong] ([ID_Phong], [ID_LoaiPhong], [Gia], [TrangThai], [DelFlag]) VALUES (N'007', N'00', N'1000000', 0, 0)
INSERT [dbo].[Phong] ([ID_Phong], [ID_LoaiPhong], [Gia], [TrangThai], [DelFlag]) VALUES (N'008', N'00', N'1000000', 0, 0)
INSERT [dbo].[Phong] ([ID_Phong], [ID_LoaiPhong], [Gia], [TrangThai], [DelFlag]) VALUES (N'009', N'00', N'1000000', 0, 0)
INSERT [dbo].[Phong] ([ID_Phong], [ID_LoaiPhong], [Gia], [TrangThai], [DelFlag]) VALUES (N'010', N'00', N'1000000', 0, 0)
INSERT [dbo].[SoDien] ([ID_SoDien], [ID_Phong], [SoCu], [SoMoi], [NgayBatDauSuDungDien], [NgayKetThucSuDungDien], [DelFlg], [ID_LoaiDonGia]) VALUES (N'00001', N'003', N'0', N'5', N'12/23/2023 21:59:44', N'12/23/2023 21:59:44', 0, N'00')
INSERT [dbo].[SoDien] ([ID_SoDien], [ID_Phong], [SoCu], [SoMoi], [NgayBatDauSuDungDien], [NgayKetThucSuDungDien], [DelFlg], [ID_LoaiDonGia]) VALUES (N'00002', N'004', N'0', N'5', N'12/23/2023 22:00:30', N'12/23/2023 22:00:30', 1, N'00')
INSERT [dbo].[SoDien] ([ID_SoDien], [ID_Phong], [SoCu], [SoMoi], [NgayBatDauSuDungDien], [NgayKetThucSuDungDien], [DelFlg], [ID_LoaiDonGia]) VALUES (N'00003', N'005', N'0', N'10', N'12/23/2023 22:01:30', N'12/23/2023 22:01:30', 1, N'00')
INSERT [dbo].[SoDien] ([ID_SoDien], [ID_Phong], [SoCu], [SoMoi], [NgayBatDauSuDungDien], [NgayKetThucSuDungDien], [DelFlg], [ID_LoaiDonGia]) VALUES (N'00004', N'005', N'0', N'0', N'12/23/2023 22:19:20', N'12/23/2023 22:19:20', 0, N'00')
INSERT [dbo].[SoNuoc] ([ID_SoNuoc], [ID_Phong], [SoCu], [SoMoi], [NgayBatDauSuDungNuoc], [NgayKetThucSuDungNuoc], [DelFlg], [ID_LoaiDonGia]) VALUES (N'00001', N'003', N'0', N'5', N'12/23/2023 21:59:44', N'12/23/2023 21:59:44', 0, N'01')
INSERT [dbo].[SoNuoc] ([ID_SoNuoc], [ID_Phong], [SoCu], [SoMoi], [NgayBatDauSuDungNuoc], [NgayKetThucSuDungNuoc], [DelFlg], [ID_LoaiDonGia]) VALUES (N'00002', N'004', N'0', N'5', N'12/23/2023 22:00:30', N'12/23/2023 22:00:30', 1, N'01')
INSERT [dbo].[SoNuoc] ([ID_SoNuoc], [ID_Phong], [SoCu], [SoMoi], [NgayBatDauSuDungNuoc], [NgayKetThucSuDungNuoc], [DelFlg], [ID_LoaiDonGia]) VALUES (N'00003', N'005', N'0', N'10', N'12/23/2023 22:01:30', N'12/23/2023 22:01:30', 1, N'01')
INSERT [dbo].[SoNuoc] ([ID_SoNuoc], [ID_Phong], [SoCu], [SoMoi], [NgayBatDauSuDungNuoc], [NgayKetThucSuDungNuoc], [DelFlg], [ID_LoaiDonGia]) VALUES (N'00004', N'005', N'0', N'0', N'12/23/2023 22:19:20', N'12/23/2023 22:19:20', 0, N'01')
INSERT [dbo].[TaiKhoan] ([ID_TaiKhoan], [Password], [TenTaiKhoan], [Quyen], [DelFlg]) VALUES (N'00001', N'123', N'Admin', 9999, 0)
INSERT [dbo].[TaiKhoan] ([ID_TaiKhoan], [Password], [TenTaiKhoan], [Quyen], [DelFlg]) VALUES (N'00002', N'1234', N'NV1', 9999, 1)
INSERT [dbo].[TaiKhoan] ([ID_TaiKhoan], [Password], [TenTaiKhoan], [Quyen], [DelFlg]) VALUES (N'00003', N'', N'', 0, 1)
INSERT [dbo].[TaiKhoan] ([ID_TaiKhoan], [Password], [TenTaiKhoan], [Quyen], [DelFlg]) VALUES (N'00004', N'123', N'nv2', 5555, 0)
INSERT [dbo].[TaiKhoan] ([ID_TaiKhoan], [Password], [TenTaiKhoan], [Quyen], [DelFlg]) VALUES (N'00005', N'1234', N'123', 5555, 1)
ALTER TABLE [dbo].[CongNo]  WITH CHECK ADD  CONSTRAINT [FK_CongNo_LichSuThuePhong] FOREIGN KEY([ID_LichSuThuePhong])
REFERENCES [dbo].[LichSuThuePhong] ([ID_LichSuThuePhong])
GO
ALTER TABLE [dbo].[CongNo] CHECK CONSTRAINT [FK_CongNo_LichSuThuePhong]
GO
ALTER TABLE [dbo].[CongNo]  WITH CHECK ADD  CONSTRAINT [FK_CongNo_SoDien] FOREIGN KEY([ID_SoDien])
REFERENCES [dbo].[SoDien] ([ID_SoDien])
GO
ALTER TABLE [dbo].[CongNo] CHECK CONSTRAINT [FK_CongNo_SoDien]
GO
ALTER TABLE [dbo].[CongNo]  WITH CHECK ADD  CONSTRAINT [FK_CongNo_SoNuoc] FOREIGN KEY([ID_SoNuoc])
REFERENCES [dbo].[SoNuoc] ([ID_SoNuoc])
GO
ALTER TABLE [dbo].[CongNo] CHECK CONSTRAINT [FK_CongNo_SoNuoc]
GO
ALTER TABLE [dbo].[LichSuThanhToan]  WITH CHECK ADD  CONSTRAINT [FK_LichSuThanhToan_LichSuThuePhong] FOREIGN KEY([ID_LichSuThuePhong])
REFERENCES [dbo].[LichSuThuePhong] ([ID_LichSuThuePhong])
GO
ALTER TABLE [dbo].[LichSuThanhToan] CHECK CONSTRAINT [FK_LichSuThanhToan_LichSuThuePhong]
GO
ALTER TABLE [dbo].[LichSuThuePhong]  WITH CHECK ADD  CONSTRAINT [FK_LichSuThuePhong_KhachHang] FOREIGN KEY([ID_KhachHang])
REFERENCES [dbo].[KhachHang] ([ID_KhachHang])
GO
ALTER TABLE [dbo].[LichSuThuePhong] CHECK CONSTRAINT [FK_LichSuThuePhong_KhachHang]
GO
ALTER TABLE [dbo].[LichSuThuePhong]  WITH CHECK ADD  CONSTRAINT [FK_LichSuThuePhong_Phong] FOREIGN KEY([ID_Phong])
REFERENCES [dbo].[Phong] ([ID_Phong])
GO
ALTER TABLE [dbo].[LichSuThuePhong] CHECK CONSTRAINT [FK_LichSuThuePhong_Phong]
GO
ALTER TABLE [dbo].[Phong]  WITH CHECK ADD  CONSTRAINT [FK_Phong_LoaiPhong] FOREIGN KEY([ID_LoaiPhong])
REFERENCES [dbo].[LoaiPhong] ([ID_LoaiPhong])
GO
ALTER TABLE [dbo].[Phong] CHECK CONSTRAINT [FK_Phong_LoaiPhong]
GO
ALTER TABLE [dbo].[SoDien]  WITH CHECK ADD  CONSTRAINT [FK_SoDien_DonGia] FOREIGN KEY([ID_LoaiDonGia])
REFERENCES [dbo].[DonGia] ([ID_DonGia])
GO
ALTER TABLE [dbo].[SoDien] CHECK CONSTRAINT [FK_SoDien_DonGia]
GO
ALTER TABLE [dbo].[SoNuoc]  WITH CHECK ADD  CONSTRAINT [FK_SoNuoc_DonGia] FOREIGN KEY([ID_LoaiDonGia])
REFERENCES [dbo].[DonGia] ([ID_DonGia])
GO
ALTER TABLE [dbo].[SoNuoc] CHECK CONSTRAINT [FK_SoNuoc_DonGia]
GO
/****** Object:  StoredProcedure [dbo].[STP_GetDataBaoCao]    Script Date: 12/24/2023 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[STP_GetDataBaoCao]
	@Search				NVARCHAR(50)
,	@TrangThaiPhong		NVARCHAR(50)
,	@Ngay				NVARCHAR(50)
AS
SET NOCOUNT ON;
SELECT
	P.ID_Phong
,	(CASE WHEN P.TrangThai = '1' THEN 'Đã thuê' ELSE 'Chưa thê' END) as TrangThai
,	ISNULL(KH.ID_KhachHang,'') as ID_KhachHang
,	ISNULL(KH.TenKhachHang,'') as TenKhachHang
,	ISNULL(SD.ID_SoDien,'') As ID_SoDien
,	ISNULL(SN.ID_SoNuoc,'') As ID_SoNuoc
,	ISNULL(LSTP.ID_LichSuThuePhong,'') As ID_LichSuThuePhong
,	ISNULL(CONVERT(nvarchar(10), LSTP.ThoiGianBatDauThue , 103),'') As ThoiGianBatDauThue
,	ISNULL(CONVERT(nvarchar(10), LSTP.ThoiGianKetThucThue , 103),'') As ThoiGianKetThucThue
,	ISNULL(DATEDIFF(month,LSTP.ThoiGianBatDauThue,LSTP.ThoiGianKetThucThue),'') As SoThangDaThue
,	ISNULL((MAX(CAST(SD.SoMoi As INT))-MAX(CAST(SD.SoCu As INT))),'') As SoDienSuDung
,	ISNULL((MAX(CAST(SN.SoMoi As INT))-MAX(CAST(SN.SoCu As INT))),'') As SoNuocDaSuDung
,	(SUM( CAST( ISNULL(CN.SoTienNo,'0') As INT) ) + SUM( CAST( ISNULL(LSTT.SoTienThanhToan,'0') As INT) )) As TongSoTien
,	SUM( CAST( ISNULL(LSTT.SoTienThanhToan,'0') As INT) ) As TongSoTienDaThanhToan
,	SUM( CAST( ISNULL(CN.SoTienNo,'0') As INT) ) As TongSoTienChuaThanhToan
,	CONVERT(nvarchar(10), GetDate() , 103) As DateFoodter
FROM
	Phong As P
LEFT JOIN
	LichSuThuePhong As LSTP
ON
	LSTP.ID_Phong = P.ID_Phong
AND LSTP.DelFlg = 0
LEFT JOIN
	KhachHang As KH
ON
	KH.ID_KhachHang = LSTP.ID_KhachHang
AND KH.DelFlg = 0
LEFT JOIN
	SoDien As SD
ON
	SD.ID_Phong = P.ID_Phong
AND SD.DelFlg = 0
LEFT JOIN
	SoNuoc As SN
ON
	SN.ID_Phong = P.ID_Phong
AND SN.DelFlg = 0
LEFT JOIN
	CongNo As CN
ON
	CN.ID_LichSuThuePhong = LSTP.ID_LichSuThuePhong
AND CN.ID_Phong = LSTP.ID_Phong
AND CN.ID_KhachHang = LSTP.ID_KhachHang
AND CN.TrangThaiNo = 1
AND CN.DelFlg = 0
LEFT JOIN
	LichSuThanhToan As LSTT
ON
	 LSTT.ID_LichSuThuePhong = LSTP.ID_LichSuThuePhong
WHERE
	P.DelFlag = 0
AND (
	(
		P.ID_Phong like '%' + @Search + '%'
	OR
		KH.TenKhachHang like '%' + @Search + '%'
	)
	AND
	(
		P.TrangThai = @TrangThaiPhong
	OR
		@TrangThaiPhong = ''
	)
	AND
	(
			( DATEDIFF(DAY, @Ngay, LSTP.ThoiGianBatDauThue) <= 90 AND DATEDIFF(DAY, @Ngay, LSTP.ThoiGianBatDauThue) > = 0)
		OR
			@Ngay = '0'
	)
	OR
	(
		@Search = '' AND @TrangThaiPhong = '' AND @Ngay = '0'
	)
)
GROUP BY
	P.ID_Phong
,	P.TrangThai
,	KH.ID_KhachHang
,	KH.TenKhachHang
,	SD.ID_SoDien
,	SN.ID_SoNuoc
,	LSTP.ID_LichSuThuePhong
,	LSTP.ThoiGianBatDauThue
,	LSTP.ThoiGianKetThucThue

GO
/****** Object:  StoredProcedure [dbo].[STP_GetDataDienNuoc]    Script Date: 12/24/2023 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[STP_GetDataDienNuoc]
	@Search				NVARCHAR(50)
,	@TrangThaiPhong		NVARCHAR(50)
AS
SET NOCOUNT ON;
SELECT DISTINCT
	P.ID_Phong
,	P.TrangThai
,	ISNULL(LSTP.ID_LichSuThuePhong,'') As ID_LichSuThuePhong
,	ISNULL(LSTP.SoTienThanhToan,'') As SoTienThanhToan
,	ISNULL(SD.ID_SoDien,'') as ID_SoDien
,	ISNULL(SN.ID_SoNuoc,'') as ID_SoNuoc
,	ISNULL(SD.SoCu,'') As SoDienCu
,	ISNULL(SD.SoMoi,'') As SoDienMoi
,	ISNULL(SN.SoCu,'') As  SoNuocCu
,	ISNULL(SN.SoMoi,'') As SoNuocMoi
,	ISNULL(( CAST(SD.SoMoi As INT) - CAST(SD.SoCu As INT) ),'') As SoDienSuDung
,	ISNULL(( CAST(SN.SoMoi As INT) - CAST(SN.SoCu As INT) ),'') As SoNuocSuDung
,	ISNULL(DGD.DonGia,'') As DonGiaDien
,	ISNULL(DGN.DonGia,'') As DonGiaNuoc
,	ISNULL(( CAST(SD.SoMoi As INT) - CAST(SD.SoCu As INT) )*DGD.DonGia + ( CAST(SN.SoMoi As INT) - CAST(SN.SoCu As INT) )*DGN.DonGia,'')As ThanhTien
FROM
	Phong As P
LEFT JOIN
	SoDien As SD
ON
	P.ID_Phong = SD.ID_Phong
AND SD.DelFlg = 0
LEFT JOIN
	LichSuThuePhong As LSTP
ON
	LSTP.ID_Phong = P.ID_Phong
AND LSTP.DelFlg = 0
LEFT JOIN
	SoNuoc As SN
ON
	SN.ID_Phong = P.ID_Phong
AND SN.DelFlg = 0
LEFT JOIN
	DonGia As DGD
ON
	DGD.ID_LoaiDonGia = SD.ID_LoaiDonGia
LEFT JOIN
	DonGia As DGN
ON
	DGN.ID_LoaiDonGia = SN.ID_LoaiDonGia
WHERE
	P.DelFlag = 0
AND
(
		P.ID_Phong like '%'+@Search+'%'
	OR
		@Search = ''
)
AND
(
	p.TrangThai = @TrangThaiPhong
OR
	@TrangThaiPhong = ''
)


GO
/****** Object:  StoredProcedure [dbo].[STP_GetDataKhachHang]    Script Date: 12/24/2023 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[STP_GetDataKhachHang]
	@Search NvarChar(50)
AS
SET NOCOUNT ON;
SELECT
	KH.ID_KhachHang,
	KH.TenKhachHang,
	KH.CCCD,
	KH.SoDienThoai,
	KH.DiaChi,
	ISNULL(P.ID_Phong,'') As ID_Phong,
	ISNULL(LP.TenLoaiPhong,'') As TenLoaiPhong,
	ISNULL(P.Gia,'') As Gia,
	ISNULL(LSTP.ID_LichSuThuePhong,'') As ID_LichSuThuePhong,
	ISNULL(LSTP.ThoiGianBatDauThue,'') As ThoiGianBatDauThue,
	ISNULL(LSTP.ThoiGianKetThucThue,'') As ThoiGianKetThucThue
FROM
	KhachHang As KH
LEFT JOIN
	LichSuThuePhong As LSTP
ON
	LSTP.ID_KhachHang = KH.ID_KhachHang
AND LSTP.DelFlg = 0
LEFT JOIN
	Phong As P
ON
	P.ID_Phong = LSTP.ID_Phong
AND P.DelFlag = 0
LEFT JOIN
	LoaiPhong As LP
ON
	LP.ID_LoaiPhong = P.ID_LoaiPhong
AND LP.DelFlg = 0
WHERE
	KH.DelFlg = 0
AND (
	( KH.DiaChi like '%'+@Search+'%' OR KH.TenKhachHang like '%'+@Search+'%')
	OR
	@Search = ''
	)
	


GO
/****** Object:  StoredProcedure [dbo].[STP_GetDataSoDien]    Script Date: 12/24/2023 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[STP_GetDataSoDien]
	@IdPhong				NVARCHAR(50)
AS
SET NOCOUNT ON;
SELECT DISTINCT
	ISNULL(SD.ID_SoDien,'') As ID_SoDien
,	ISNUll(SD.SoCu,'') As SDSoCu
,	ISNUll(SD.SoMoi,'') As SDSoMoi
,	ISNULL(( CAST(SD.SoMoi As INT) - CAST(SD.SoCu As INT) ),'') As SoDienSuDung
,	ISNULL(( CAST(SD.SoMoi As INT) - CAST(SD.SoCu As INT) )*MAX(DGD.DonGia),'')As ThanhTien
FROM
	SoDien As SD
LEFT JOIN
	SoNuoc As SN
ON
	SN.ID_Phong = SD.ID_Phong
AND SN.DelFlg = 0
LEFT JOIN
	DonGia As DGD
ON
	DGD.ID_LoaiDonGia = SD.ID_LoaiDonGia
LEFT JOIN
	DonGia As DGN
ON
	DGN.ID_LoaiDonGia = SN.ID_LoaiDonGia
WHERE
	SD.DelFlg = 0
AND SD.ID_Phong = @IdPhong
GROUP BY
	SD.ID_SoDien
,	SD.SoCu
,	SD.SoMoi
,	SN.ID_SoNuoc
,	SN.SoCu
,	SN.SoMoi


GO
/****** Object:  StoredProcedure [dbo].[STP_GetDataSoNuoc]    Script Date: 12/24/2023 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[STP_GetDataSoNuoc]
	@IdPhong				NVARCHAR(50)
AS
SET NOCOUNT ON;
SELECT DISTINCT
	ISNUll(SN.ID_SoNuoc,'') As ID_SoNuoc
,	ISNUll(SN.SoCu,'') As SNSoCu
,	ISNUll(SN.SoMoi,'') As SNSoMoi
,	ISNULL(( CAST(SN.SoMoi As INT) - CAST(SN.SoCu As INT) ),'') As SoNuocSuDung
,	ISNULL(( CAST(SN.SoMoi As INT) - CAST(SN.SoCu As INT) )*MAX(DGN.DonGia),'')As ThanhTien
FROM
	SoDien As SD
LEFT JOIN
	SoNuoc As SN
ON
	SN.ID_Phong = SD.ID_Phong
AND SN.DelFlg = 0
LEFT JOIN
	DonGia As DGD
ON
	DGD.ID_LoaiDonGia = SD.ID_LoaiDonGia
LEFT JOIN
	DonGia As DGN
ON
	DGN.ID_LoaiDonGia = SN.ID_LoaiDonGia
WHERE
	SD.DelFlg = 0
AND SD.ID_Phong = @IdPhong
GROUP BY
	SD.ID_SoDien
,	SD.SoCu
,	SD.SoMoi
,	SN.ID_SoNuoc
,	SN.SoCu
,	SN.SoMoi


GO
/****** Object:  StoredProcedure [dbo].[STP_GetDataTinhTien]    Script Date: 12/24/2023 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[STP_GetDataTinhTien]
	@Search				NVARCHAR(50)
,	@TrangThaiPhong		NVARCHAR(50)
AS
SET NOCOUNT ON;
SELECT DISTINCT
	P.ID_Phong
,	P.TrangThai
,	ISNULL(CN.ID_CongNo,'') As ID_CongNo
,	ISNULL(SD.ID_SoDien,'') As ID_SoDien
,	ISNUll(SN.ID_SoNuoc,'') As ID_SoNuoc
,	ISNULL(KH.ID_KhachHang,'') As ID_KhachHang
,	ISNULL(KH.TenKhachHang,'') As TenKhachHang
,	ISNULL(LSTP.ID_LichSuThuePhong,'') As ID_LichSuThuePhong
,	(SUM( CAST( ISNULL(CN.SoTienNo,'0') As INT) ) + SUM( CAST( ISNULL(LSTT.SoTienThanhToan,'0') As INT) )) As TongSoTien
,	SUM( CAST( ISNULL(LSTT.SoTienThanhToan,'0') As INT) ) As TongSoTienDaThanhToan
,	SUM( CAST( ISNULL(CN.SoTienNo,'0') As INT) ) As TongSoTienChuaThanhToan
FROM
	Phong As P
INNER JOIN
	LichSuThuePhong As LSTP
ON
	LSTP.ID_Phong = P.ID_Phong
AND LSTP.DelFlg = 0
LEFT JOIN
	SoDien As SD
ON
	SD.ID_Phong = P.ID_Phong
AND SD.DelFlg = 0
LEFT JOIN
	SoNuoc As SN
ON
	SN.ID_Phong = P.ID_Phong
AND SN.DelFlg = 0
LEFT JOIN
	CongNo As CN
ON
	CN.ID_LichSuThuePhong = LSTP.ID_LichSuThuePhong
AND CN.ID_Phong = LSTP.ID_Phong
AND CN.ID_KhachHang = LSTP.ID_KhachHang
AND CN.TrangThaiNo = 1
AND CN.DelFlg = 0
LEFT JOIN
	LichSuThanhToan As LSTT
ON
	 LSTT.ID_LichSuThuePhong = LSTP.ID_LichSuThuePhong
LEFT JOIN
	KhachHang As KH
ON
	KH.ID_KhachHang = LSTP.ID_KhachHang
AND KH.DelFlg = 0
WHERE
	P.DelFlag = 0
AND
(
		P.ID_Phong like '%'+@Search+'%'
	OR
		@Search = ''
)
AND
(
		p.TrangThai = @TrangThaiPhong
	OR
		@TrangThaiPhong = ''
)
GROUP BY
	P.ID_Phong
,	P.TrangThai
,	CN.ID_CongNo
,	SD.ID_SoDien
,	SN.ID_SoNuoc
,	KH.ID_KhachHang
,	KH.TenKhachHang
,	LSTP.ID_LichSuThuePhong

GO
/****** Object:  StoredProcedure [dbo].[STP_ListDataTagetUseTrangChu]    Script Date: 12/24/2023 2:38:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[STP_ListDataTagetUseTrangChu]
	@Search					NVARCHAR(50)
,	@TrangThaiPhong			NVARCHAR(50)
,	@TrangThaiPhi			NVARCHAR(50)
AS
SET NOCOUNT ON;
SELECT DISTINCT
	P.ID_Phong,
	P.Gia,
	P.TrangThai,
	LP.ID_LoaiPhong,
	LP.TenLoaiPhong,
	ISNULL(KH.ID_KhachHang,'') As ID_KhachHang,
	ISNULL(KH.TenKhachHang,'') As TenKhachHang
FROM
	Phong As P
INNER JOIN
	LoaiPhong As LP
ON
	LP.ID_LoaiPhong = P.ID_LoaiPhong
AND LP.DelFlg = 0
LEFT JOIN
	LichSuThuePhong As LSTP
ON
	LSTP.ID_Phong = P.ID_Phong
AND LSTP.DelFlg = 0
LEFT JOIN
	KhachHang As KH
ON
	KH.ID_KhachHang = LSTP.ID_KhachHang
AND KH.DelFlg = 0
LEFT JOIN
	CongNo As CN
ON
	CN.ID_Phong = LSTP.ID_Phong
AND LSTP.ThoiGianKetThucThue <= GETDATE()
WHERE
(
	(
		P.ID_Phong like '%' + @Search + '%'
	OR
		KH.TenKhachHang like '%' + @Search + '%'
	)
	AND
	(
		P.TrangThai = @TrangThaiPhong
	OR
		@TrangThaiPhong = ''
	)
	AND
	(
		(ISNULL(CN.TrangThaiNo,'0') = @TrangThaiPhi AND P.TrangThai <> '0')
	OR
		( @TrangThaiPhi = @TrangThaiPhi AND P.TrangThai = '0' AND ISNULL(LSTP.ID_Phong,'') <> '')
	OR
		@TrangThaiPhi = ''
	)
	OR
	(
		@Search = '' AND @TrangThaiPhong = '' AND @TrangThaiPhi = '' 
	)
)
AND P.DelFlag = 0
ORDER BY P.ID_Phong ASC
	


GO
