﻿namespace QLPT
{
	partial class ChiTiet
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.richTextBoxDiaChi = new System.Windows.Forms.RichTextBox();
			this.textBoxCMND = new System.Windows.Forms.TextBox();
			this.textBoxSoDienThoai = new System.Windows.Forms.TextBox();
			this.textBoxTenKhachHang = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.dateTimePickerKetThuc = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerBatDau = new System.Windows.Forms.DateTimePicker();
			this.label19 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.textBoxGia = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.label8 = new System.Windows.Forms.Label();
			this.comboBoxLoaiPhong = new System.Windows.Forms.ComboBox();
			this.textBoxIdPhong = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.label17 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.labTong = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.lbTongTienNuoc = new System.Windows.Forms.Label();
			this.lbDaSuDungNuoc = new System.Windows.Forms.Label();
			this.lbSoNuocMoi = new System.Windows.Forms.Label();
			this.lbSoNuocCu = new System.Windows.Forms.Label();
			this.label27 = new System.Windows.Forms.Label();
			this.label28 = new System.Windows.Forms.Label();
			this.lbTongTienDien = new System.Windows.Forms.Label();
			this.lbDaSuDungDien = new System.Windows.Forms.Label();
			this.lbSoDienMoi = new System.Windows.Forms.Label();
			this.lbSoDienCu = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.richTextBoxDiaChi);
			this.groupBox1.Controls.Add(this.textBoxCMND);
			this.groupBox1.Controls.Add(this.textBoxSoDienThoai);
			this.groupBox1.Controls.Add(this.textBoxTenKhachHang);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(27, 13);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(405, 234);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Thông tin khách hàng";
			// 
			// richTextBoxDiaChi
			// 
			this.richTextBoxDiaChi.Location = new System.Drawing.Point(153, 156);
			this.richTextBoxDiaChi.Name = "richTextBoxDiaChi";
			this.richTextBoxDiaChi.Size = new System.Drawing.Size(234, 46);
			this.richTextBoxDiaChi.TabIndex = 7;
			this.richTextBoxDiaChi.Text = "";
			// 
			// textBoxCMND
			// 
			this.textBoxCMND.Location = new System.Drawing.Point(153, 116);
			this.textBoxCMND.Name = "textBoxCMND";
			this.textBoxCMND.Size = new System.Drawing.Size(234, 22);
			this.textBoxCMND.TabIndex = 6;
			// 
			// textBoxSoDienThoai
			// 
			this.textBoxSoDienThoai.Location = new System.Drawing.Point(153, 73);
			this.textBoxSoDienThoai.Name = "textBoxSoDienThoai";
			this.textBoxSoDienThoai.Size = new System.Drawing.Size(234, 22);
			this.textBoxSoDienThoai.TabIndex = 5;
			// 
			// textBoxTenKhachHang
			// 
			this.textBoxTenKhachHang.Location = new System.Drawing.Point(153, 31);
			this.textBoxTenKhachHang.Name = "textBoxTenKhachHang";
			this.textBoxTenKhachHang.Size = new System.Drawing.Size(234, 22);
			this.textBoxTenKhachHang.TabIndex = 4;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(19, 156);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(55, 17);
			this.label4.TabIndex = 3;
			this.label4.Text = "Địa chỉ:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(19, 116);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(52, 17);
			this.label3.TabIndex = 2;
			this.label3.Text = "CMND:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(16, 73);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(95, 17);
			this.label2.TabIndex = 1;
			this.label2.Text = "Số điện thoại:";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(16, 31);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(115, 17);
			this.label1.TabIndex = 0;
			this.label1.Text = "Tên khách hàng:";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.dateTimePickerKetThuc);
			this.groupBox2.Controls.Add(this.dateTimePickerBatDau);
			this.groupBox2.Controls.Add(this.label19);
			this.groupBox2.Controls.Add(this.label18);
			this.groupBox2.Controls.Add(this.textBoxGia);
			this.groupBox2.Controls.Add(this.button1);
			this.groupBox2.Controls.Add(this.label8);
			this.groupBox2.Controls.Add(this.comboBoxLoaiPhong);
			this.groupBox2.Controls.Add(this.textBoxIdPhong);
			this.groupBox2.Controls.Add(this.label6);
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Location = new System.Drawing.Point(453, 13);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(458, 234);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Thông tin phòng";
			// 
			// dateTimePickerKetThuc
			// 
			this.dateTimePickerKetThuc.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerKetThuc.Location = new System.Drawing.Point(332, 29);
			this.dateTimePickerKetThuc.Name = "dateTimePickerKetThuc";
			this.dateTimePickerKetThuc.Size = new System.Drawing.Size(101, 22);
			this.dateTimePickerKetThuc.TabIndex = 15;
			// 
			// dateTimePickerBatDau
			// 
			this.dateTimePickerBatDau.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerBatDau.Location = new System.Drawing.Point(135, 29);
			this.dateTimePickerBatDau.Name = "dateTimePickerBatDau";
			this.dateTimePickerBatDau.Size = new System.Drawing.Size(101, 22);
			this.dateTimePickerBatDau.TabIndex = 14;
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(253, 31);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(64, 17);
			this.label19.TabIndex = 13;
			this.label19.Text = "Kết thúc:";
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(31, 32);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(61, 17);
			this.label18.TabIndex = 12;
			this.label18.Text = "Bắt đầu:";
			// 
			// textBoxGia
			// 
			this.textBoxGia.Location = new System.Drawing.Point(135, 150);
			this.textBoxGia.Name = "textBoxGia";
			this.textBoxGia.Size = new System.Drawing.Size(298, 22);
			this.textBoxGia.TabIndex = 11;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(215, 184);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(131, 35);
			this.button1.TabIndex = 3;
			this.button1.Text = "Sửa";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(28, 155);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(78, 17);
			this.label8.TabIndex = 10;
			this.label8.Text = "Giá phòng:";
			// 
			// comboBoxLoaiPhong
			// 
			this.comboBoxLoaiPhong.FormattingEnabled = true;
			this.comboBoxLoaiPhong.Location = new System.Drawing.Point(135, 109);
			this.comboBoxLoaiPhong.Name = "comboBoxLoaiPhong";
			this.comboBoxLoaiPhong.Size = new System.Drawing.Size(298, 24);
			this.comboBoxLoaiPhong.TabIndex = 9;
			// 
			// textBoxIdPhong
			// 
			this.textBoxIdPhong.Location = new System.Drawing.Point(135, 73);
			this.textBoxIdPhong.Name = "textBoxIdPhong";
			this.textBoxIdPhong.Size = new System.Drawing.Size(298, 22);
			this.textBoxIdPhong.TabIndex = 8;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(28, 113);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(83, 17);
			this.label6.TabIndex = 1;
			this.label6.Text = "Loại phòng:";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(28, 71);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(94, 17);
			this.label5.TabIndex = 0;
			this.label5.Text = "Mã số phòng:";
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.label17);
			this.groupBox3.Controls.Add(this.label16);
			this.groupBox3.Controls.Add(this.labTong);
			this.groupBox3.Controls.Add(this.label14);
			this.groupBox3.Controls.Add(this.label13);
			this.groupBox3.Controls.Add(this.lbTongTienNuoc);
			this.groupBox3.Controls.Add(this.lbDaSuDungNuoc);
			this.groupBox3.Controls.Add(this.lbSoNuocMoi);
			this.groupBox3.Controls.Add(this.lbSoNuocCu);
			this.groupBox3.Controls.Add(this.label27);
			this.groupBox3.Controls.Add(this.label28);
			this.groupBox3.Controls.Add(this.lbTongTienDien);
			this.groupBox3.Controls.Add(this.lbDaSuDungDien);
			this.groupBox3.Controls.Add(this.lbSoDienMoi);
			this.groupBox3.Controls.Add(this.lbSoDienCu);
			this.groupBox3.Controls.Add(this.label12);
			this.groupBox3.Controls.Add(this.label11);
			this.groupBox3.Controls.Add(this.label10);
			this.groupBox3.Controls.Add(this.label9);
			this.groupBox3.Location = new System.Drawing.Point(27, 253);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(884, 244);
			this.groupBox3.TabIndex = 2;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Thông tin điện nước";
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(496, 147);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(73, 17);
			this.label17.TabIndex = 31;
			this.label17.Text = "* 10.000 đ";
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(189, 147);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(65, 17);
			this.label16.TabIndex = 30;
			this.label16.Text = "* 1.500 đ";
			// 
			// labTong
			// 
			this.labTong.AutoSize = true;
			this.labTong.Location = new System.Drawing.Point(586, 194);
			this.labTong.Name = "labTong";
			this.labTong.Size = new System.Drawing.Size(0, 17);
			this.labTong.TabIndex = 28;
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(549, 194);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(16, 17);
			this.label14.TabIndex = 27;
			this.label14.Text = "=";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(25, 173);
			this.label13.MaximumSize = new System.Drawing.Size(660, 0);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(613, 17);
			this.label13.TabIndex = 26;
			this.label13.Text = "---------------------------------------------------------------------------------" +
    "----------------------------------------";
			// 
			// lbTongTienNuoc
			// 
			this.lbTongTienNuoc.AutoSize = true;
			this.lbTongTienNuoc.Location = new System.Drawing.Point(460, 194);
			this.lbTongTienNuoc.Name = "lbTongTienNuoc";
			this.lbTongTienNuoc.Size = new System.Drawing.Size(0, 17);
			this.lbTongTienNuoc.TabIndex = 25;
			// 
			// lbDaSuDungNuoc
			// 
			this.lbDaSuDungNuoc.AutoSize = true;
			this.lbDaSuDungNuoc.Location = new System.Drawing.Point(457, 147);
			this.lbDaSuDungNuoc.Name = "lbDaSuDungNuoc";
			this.lbDaSuDungNuoc.Size = new System.Drawing.Size(0, 17);
			this.lbDaSuDungNuoc.TabIndex = 24;
			// 
			// lbSoNuocMoi
			// 
			this.lbSoNuocMoi.AutoSize = true;
			this.lbSoNuocMoi.Location = new System.Drawing.Point(457, 98);
			this.lbSoNuocMoi.Name = "lbSoNuocMoi";
			this.lbSoNuocMoi.Size = new System.Drawing.Size(0, 17);
			this.lbSoNuocMoi.TabIndex = 23;
			// 
			// lbSoNuocCu
			// 
			this.lbSoNuocCu.AutoSize = true;
			this.lbSoNuocCu.Location = new System.Drawing.Point(457, 49);
			this.lbSoNuocCu.Name = "lbSoNuocCu";
			this.lbSoNuocCu.Size = new System.Drawing.Size(0, 17);
			this.lbSoNuocCu.TabIndex = 22;
			// 
			// label27
			// 
			this.label27.AutoSize = true;
			this.label27.Location = new System.Drawing.Point(341, 98);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(96, 17);
			this.label27.TabIndex = 19;
			this.label27.Text = "Số nước(Mới):";
			// 
			// label28
			// 
			this.label28.AutoSize = true;
			this.label28.Location = new System.Drawing.Point(341, 49);
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size(91, 17);
			this.label28.TabIndex = 18;
			this.label28.Text = "Số nước(Cũ):";
			// 
			// lbTongTienDien
			// 
			this.lbTongTienDien.AutoSize = true;
			this.lbTongTienDien.Location = new System.Drawing.Point(122, 194);
			this.lbTongTienDien.Name = "lbTongTienDien";
			this.lbTongTienDien.Size = new System.Drawing.Size(0, 17);
			this.lbTongTienDien.TabIndex = 9;
			// 
			// lbDaSuDungDien
			// 
			this.lbDaSuDungDien.AutoSize = true;
			this.lbDaSuDungDien.Location = new System.Drawing.Point(150, 147);
			this.lbDaSuDungDien.Name = "lbDaSuDungDien";
			this.lbDaSuDungDien.Size = new System.Drawing.Size(0, 17);
			this.lbDaSuDungDien.TabIndex = 8;
			// 
			// lbSoDienMoi
			// 
			this.lbSoDienMoi.AutoSize = true;
			this.lbSoDienMoi.Location = new System.Drawing.Point(150, 98);
			this.lbSoDienMoi.Name = "lbSoDienMoi";
			this.lbSoDienMoi.Size = new System.Drawing.Size(0, 17);
			this.lbSoDienMoi.TabIndex = 7;
			// 
			// lbSoDienCu
			// 
			this.lbSoDienCu.AutoSize = true;
			this.lbSoDienCu.Location = new System.Drawing.Point(150, 49);
			this.lbSoDienCu.Name = "lbSoDienCu";
			this.lbSoDienCu.Size = new System.Drawing.Size(0, 17);
			this.lbSoDienCu.TabIndex = 6;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(23, 194);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(72, 17);
			this.label12.TabIndex = 5;
			this.label12.Text = "Tổng tiền:";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(25, 147);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(85, 17);
			this.label11.TabIndex = 4;
			this.label11.Text = "Đã sử dụng:";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(25, 98);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(92, 17);
			this.label10.TabIndex = 1;
			this.label10.Text = "Số điện(Mới):";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(23, 49);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(87, 17);
			this.label9.TabIndex = 0;
			this.label9.Text = "Số điện(Cũ):";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(634, 197);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(0, 17);
			this.label7.TabIndex = 4;
			// 
			// ChiTiet
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(934, 526);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Name = "ChiTiet";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Thông tin phòng";
			this.Load += new System.EventHandler(this.ChiTiet_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.TextBox textBoxCMND;
		private System.Windows.Forms.TextBox textBoxSoDienThoai;
		private System.Windows.Forms.TextBox textBoxTenKhachHang;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.RichTextBox richTextBoxDiaChi;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox textBoxIdPhong;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox comboBoxLoaiPhong;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textBoxGia;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label labTong;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label lbTongTienNuoc;
		private System.Windows.Forms.Label lbDaSuDungNuoc;
		private System.Windows.Forms.Label lbSoNuocMoi;
		private System.Windows.Forms.Label lbSoNuocCu;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label lbTongTienDien;
		private System.Windows.Forms.Label lbDaSuDungDien;
		private System.Windows.Forms.Label lbSoDienMoi;
		private System.Windows.Forms.Label lbSoDienCu;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.DateTimePicker dateTimePickerKetThuc;
		private System.Windows.Forms.DateTimePicker dateTimePickerBatDau;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label18;
	}
}