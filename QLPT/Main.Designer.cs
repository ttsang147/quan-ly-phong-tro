﻿namespace QLPT
{
	partial class Main
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
			this.QLPT = new System.Windows.Forms.TabControl();
			this.tabkhachHang = new System.Windows.Forms.TabPage();
			this.btnResert = new System.Windows.Forms.Button();
			this.btnXoa = new System.Windows.Forms.Button();
			this.btnThem = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.dataGridViewDsKhachHang = new System.Windows.Forms.DataGridView();
			this.ID_KhachHang = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ID_LichSuThuePhong = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TenKhachHang = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.CMND = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.SoDienThoai = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.DiaChi = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ID_Phong = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TenLoaiPhong = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Gia = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ThoiGianBatDauThue = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ThoiGianKetThucThue = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.btnTimKiemKH = new System.Windows.Forms.Button();
			this.txtTimKiemKH = new System.Windows.Forms.TextBox();
			this.labelCanhBao = new System.Windows.Forms.Label();
			this.dateTimeKetThuc = new System.Windows.Forms.DateTimePicker();
			this.dateTimeBatDau = new System.Windows.Forms.DateTimePicker();
			this.txtGia = new System.Windows.Forms.TextBox();
			this.cbLoaiPhong = new System.Windows.Forms.ComboBox();
			this.cbPhong = new System.Windows.Forms.ComboBox();
			this.checkBoxThuTien = new System.Windows.Forms.CheckBox();
			this.label11 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.txtSdt = new System.Windows.Forms.TextBox();
			this.txtCMNN = new System.Windows.Forms.TextBox();
			this.txtDiaChi = new System.Windows.Forms.RichTextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.txtTenKhachHang = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.tabDienNuoc = new System.Windows.Forms.TabPage();
			this.label24 = new System.Windows.Forms.Label();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.btnLuu = new System.Windows.Forms.Button();
			this.btnClear = new System.Windows.Forms.Button();
			this.txtChiSoNuocMoi = new System.Windows.Forms.TextBox();
			this.txtChiSoNuocCu = new System.Windows.Forms.TextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.txtChiSoDienMoi = new System.Windows.Forms.TextBox();
			this.txtChiSoDienCu = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.txtPhongUpd = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.dataGridViewSoDienNuoc = new System.Windows.Forms.DataGridView();
			this.ID_Phong1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.SoTienThanhToan1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ID_SoDien = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ID_LichSuThuePhong1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ID_SoNuoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TrangThai = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.SoDienCu = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.SoDienMoi = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.SoNuocCu = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.SoNuocMoi = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.SoDienSuDung = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.SoNuocSuDung = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.DonGiaDien = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.DonGiaNuoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ThanhTien = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.btnTimKiemSoDienNuoc = new System.Windows.Forms.Button();
			this.cbTrangThai = new System.Windows.Forms.ComboBox();
			this.txtPhongTimKiem = new System.Windows.Forms.TextBox();
			this.labeltt = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.tabTinhTien = new System.Windows.Forms.TabPage();
			this.label1 = new System.Windows.Forms.Label();
			this.button2 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.groupBox7 = new System.Windows.Forms.GroupBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.dataGridViewTinhTien = new System.Windows.Forms.DataGridView();
			this.ID_Phong2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ID_CongNo2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TrangThai2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ID_SoDien2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ID_SoNuoc2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ID_KhachHang2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TenKhachHang2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ID_LichSuThuePhong2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TongSoTien = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TongSoTienDaThanhToan = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TongSoTienChuaThanhToan = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.label21 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.btnTImKiemTinhTien = new System.Windows.Forms.Button();
			this.txtTImKiemTinhTien = new System.Windows.Forms.TextBox();
			this.label19 = new System.Windows.Forms.Label();
			this.tabgeBaoCao = new System.Windows.Forms.TabPage();
			this.label12 = new System.Windows.Forms.Label();
			this.panel3 = new System.Windows.Forms.Panel();
			this.dgvBaoCao = new System.Windows.Forms.DataGridView();
			this.Bc_ID_Phong = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Bc_TrangThai = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Bc_ID_KhachHang = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Bc_TenKhachHang = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Bc_ID_SoDien = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Bc_ID_SoNuoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Bc_ID_LichSuThuePhong = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Bc_ThoiGianBatDauThue = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Bc_ThoiGianKetThucThue = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Bc_SoThangDaThue = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Bc_SoDienSuDung = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Bc_SoNuocDaSuDung = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Bc_TongSoTien = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Bc_TongSoTienDaThanhToan = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Bc_TongSoTienChuaThanhToan = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Bc_DateFoodter = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.bc = new System.Windows.Forms.GroupBox();
			this.dateBaoBao = new System.Windows.Forms.DateTimePicker();
			this.label25 = new System.Windows.Forms.Label();
			this.label23 = new System.Windows.Forms.Label();
			this.comboBoxtt = new System.Windows.Forms.ComboBox();
			this.textBoxTimKiem = new System.Windows.Forms.TextBox();
			this.labKhachHag = new System.Windows.Forms.Label();
			this.btnBoaCao = new System.Windows.Forms.Button();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.btnDangXuat = new System.Windows.Forms.Button();
			this.groupBox9 = new System.Windows.Forms.GroupBox();
			this.label29 = new System.Windows.Forms.Label();
			this.btnTimKiemTK = new System.Windows.Forms.Button();
			this.txtTimKiemTK = new System.Windows.Forms.TextBox();
			this.groupBox8 = new System.Windows.Forms.GroupBox();
			this.btnLamMoi = new System.Windows.Forms.Button();
			this.btnSuaTK = new System.Windows.Forms.Button();
			this.btnXoaTK = new System.Windows.Forms.Button();
			this.btnThemTK = new System.Windows.Forms.Button();
			this.cbQuyen = new System.Windows.Forms.ComboBox();
			this.labMatKhauMoi = new System.Windows.Forms.Label();
			this.label28 = new System.Windows.Forms.Label();
			this.label27 = new System.Windows.Forms.Label();
			this.label26 = new System.Windows.Forms.Label();
			this.label22 = new System.Windows.Forms.Label();
			this.txtNhapLaiMK = new System.Windows.Forms.TextBox();
			this.txtMatKhau = new System.Windows.Forms.TextBox();
			this.txtTenTK = new System.Windows.Forms.TextBox();
			this.txtMaTK = new System.Windows.Forms.TextBox();
			this.dgvTaiKhoan = new System.Windows.Forms.DataGridView();
			this.ID_TaiKhoan = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.DelFlg = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TenTaiKhoan = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Password = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Quyen_TK = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.groupBoxTimKiem = new System.Windows.Forms.GroupBox();
			this.cbTrangThaiPhong = new System.Windows.Forms.ComboBox();
			this.cbTrangThaiPhi = new System.Windows.Forms.ComboBox();
			this.txtSoPhong = new System.Windows.Forms.TextBox();
			this.btnTimKiem = new System.Windows.Forms.Button();
			this.groupBoxTrangChu = new System.Windows.Forms.GroupBox();
			this.panelPhong = new System.Windows.Forms.Panel();
			this.labelTrong = new System.Windows.Forms.Label();
			this.labelSoPhongTrong = new System.Windows.Forms.Label();
			this.labelDaChoThue = new System.Windows.Forms.Label();
			this.labelSoPhongChoThue = new System.Windows.Forms.Label();
			this.labelChuaThuPhi = new System.Windows.Forms.Label();
			this.labelSoPhongChuaThuPhi = new System.Windows.Forms.Label();
			this.btnThemPhongTro = new System.Windows.Forms.Button();
			this.tabPhong = new System.Windows.Forms.TabPage();
			this.QLPT.SuspendLayout();
			this.tabkhachHang.SuspendLayout();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewDsKhachHang)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.tabDienNuoc.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewSoDienNuoc)).BeginInit();
			this.groupBox3.SuspendLayout();
			this.tabTinhTien.SuspendLayout();
			this.groupBox7.SuspendLayout();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewTinhTien)).BeginInit();
			this.groupBox6.SuspendLayout();
			this.tabgeBaoCao.SuspendLayout();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvBaoCao)).BeginInit();
			this.bc.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.groupBox9.SuspendLayout();
			this.groupBox8.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvTaiKhoan)).BeginInit();
			this.groupBoxTimKiem.SuspendLayout();
			this.groupBoxTrangChu.SuspendLayout();
			this.tabPhong.SuspendLayout();
			this.SuspendLayout();
			// 
			// QLPT
			// 
			this.QLPT.Controls.Add(this.tabPhong);
			this.QLPT.Controls.Add(this.tabkhachHang);
			this.QLPT.Controls.Add(this.tabDienNuoc);
			this.QLPT.Controls.Add(this.tabTinhTien);
			this.QLPT.Controls.Add(this.tabgeBaoCao);
			this.QLPT.Controls.Add(this.tabPage1);
			this.QLPT.Location = new System.Drawing.Point(12, 12);
			this.QLPT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.QLPT.Name = "QLPT";
			this.QLPT.SelectedIndex = 0;
			this.QLPT.Size = new System.Drawing.Size(1248, 816);
			this.QLPT.TabIndex = 0;
			this.QLPT.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.QLPT_Selecting);
			// 
			// tabkhachHang
			// 
			this.tabkhachHang.BackColor = System.Drawing.Color.White;
			this.tabkhachHang.Controls.Add(this.btnResert);
			this.tabkhachHang.Controls.Add(this.btnXoa);
			this.tabkhachHang.Controls.Add(this.btnThem);
			this.tabkhachHang.Controls.Add(this.label2);
			this.tabkhachHang.Controls.Add(this.groupBox2);
			this.tabkhachHang.Controls.Add(this.groupBox1);
			this.tabkhachHang.Location = new System.Drawing.Point(4, 25);
			this.tabkhachHang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.tabkhachHang.Name = "tabkhachHang";
			this.tabkhachHang.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.tabkhachHang.Size = new System.Drawing.Size(1240, 787);
			this.tabkhachHang.TabIndex = 1;
			this.tabkhachHang.Text = "Khách hàng";
			// 
			// btnResert
			// 
			this.btnResert.Location = new System.Drawing.Point(360, 318);
			this.btnResert.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.btnResert.Name = "btnResert";
			this.btnResert.Size = new System.Drawing.Size(120, 34);
			this.btnResert.TabIndex = 6;
			this.btnResert.Text = "Làm mới";
			this.btnResert.UseVisualStyleBackColor = true;
			this.btnResert.Click += new System.EventHandler(this.btnResert_Click);
			// 
			// btnXoa
			// 
			this.btnXoa.Location = new System.Drawing.Point(739, 318);
			this.btnXoa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.btnXoa.Name = "btnXoa";
			this.btnXoa.Size = new System.Drawing.Size(120, 34);
			this.btnXoa.TabIndex = 5;
			this.btnXoa.Text = "Xóa";
			this.btnXoa.UseVisualStyleBackColor = true;
			this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
			// 
			// btnThem
			// 
			this.btnThem.Location = new System.Drawing.Point(553, 318);
			this.btnThem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.btnThem.Name = "btnThem";
			this.btnThem.Size = new System.Drawing.Size(120, 34);
			this.btnThem.TabIndex = 3;
			this.btnThem.Text = "Thêm/Update";
			this.btnThem.UseVisualStyleBackColor = true;
			this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(481, 17);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(319, 39);
			this.label2.TabIndex = 2;
			this.label2.Text = "Quản lý khách hàng";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.dataGridViewDsKhachHang);
			this.groupBox2.Location = new System.Drawing.Point(5, 367);
			this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.groupBox2.Size = new System.Drawing.Size(1325, 414);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Danh sách khác hàng";
			// 
			// dataGridViewDsKhachHang
			// 
			this.dataGridViewDsKhachHang.AllowUserToAddRows = false;
			dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle37.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle37.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle37.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewDsKhachHang.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle37;
			this.dataGridViewDsKhachHang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewDsKhachHang.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_KhachHang,
            this.ID_LichSuThuePhong,
            this.TenKhachHang,
            this.CMND,
            this.SoDienThoai,
            this.DiaChi,
            this.ID_Phong,
            this.TenLoaiPhong,
            this.Gia,
            this.ThoiGianBatDauThue,
            this.ThoiGianKetThucThue});
			dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle38.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridViewDsKhachHang.DefaultCellStyle = dataGridViewCellStyle38;
			this.dataGridViewDsKhachHang.Location = new System.Drawing.Point(5, 21);
			this.dataGridViewDsKhachHang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.dataGridViewDsKhachHang.Name = "dataGridViewDsKhachHang";
			dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle39.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle39.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle39.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle39.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewDsKhachHang.RowHeadersDefaultCellStyle = dataGridViewCellStyle39;
			this.dataGridViewDsKhachHang.RowHeadersVisible = false;
			this.dataGridViewDsKhachHang.RowTemplate.Height = 24;
			this.dataGridViewDsKhachHang.Size = new System.Drawing.Size(1223, 386);
			this.dataGridViewDsKhachHang.TabIndex = 1;
			this.dataGridViewDsKhachHang.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewDsKhachHang_CellClick);
			// 
			// ID_KhachHang
			// 
			this.ID_KhachHang.DataPropertyName = "ID_KhachHang";
			this.ID_KhachHang.HeaderText = "Mã khác hàng";
			this.ID_KhachHang.Name = "ID_KhachHang";
			this.ID_KhachHang.Width = 50;
			// 
			// ID_LichSuThuePhong
			// 
			this.ID_LichSuThuePhong.DataPropertyName = "ID_LichSuThuePhong";
			this.ID_LichSuThuePhong.HeaderText = "ID_LichSuThuePhong";
			this.ID_LichSuThuePhong.Name = "ID_LichSuThuePhong";
			this.ID_LichSuThuePhong.Visible = false;
			// 
			// TenKhachHang
			// 
			this.TenKhachHang.DataPropertyName = "TenKhachHang";
			this.TenKhachHang.HeaderText = "Tên khách hang";
			this.TenKhachHang.Name = "TenKhachHang";
			// 
			// CMND
			// 
			this.CMND.DataPropertyName = "CMND";
			this.CMND.HeaderText = "Chứng minh Thư";
			this.CMND.Name = "CMND";
			// 
			// SoDienThoai
			// 
			this.SoDienThoai.DataPropertyName = "SoDienThoai";
			this.SoDienThoai.HeaderText = "Số điện thoại";
			this.SoDienThoai.Name = "SoDienThoai";
			// 
			// DiaChi
			// 
			this.DiaChi.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.DiaChi.DataPropertyName = "DiaChi";
			this.DiaChi.HeaderText = "Địa chỉ";
			this.DiaChi.Name = "DiaChi";
			// 
			// ID_Phong
			// 
			this.ID_Phong.DataPropertyName = "ID_Phong";
			this.ID_Phong.HeaderText = "Phòng";
			this.ID_Phong.Name = "ID_Phong";
			this.ID_Phong.Width = 50;
			// 
			// TenLoaiPhong
			// 
			this.TenLoaiPhong.DataPropertyName = "TenLoaiPhong";
			this.TenLoaiPhong.HeaderText = "Loại phòng";
			this.TenLoaiPhong.Name = "TenLoaiPhong";
			this.TenLoaiPhong.Width = 50;
			// 
			// Gia
			// 
			this.Gia.DataPropertyName = "Gia";
			this.Gia.HeaderText = "Giá phòng";
			this.Gia.Name = "Gia";
			// 
			// ThoiGianBatDauThue
			// 
			this.ThoiGianBatDauThue.DataPropertyName = "ThoiGianBatDauThue";
			this.ThoiGianBatDauThue.HeaderText = "Thời gian bắt đầu thuê";
			this.ThoiGianBatDauThue.Name = "ThoiGianBatDauThue";
			// 
			// ThoiGianKetThucThue
			// 
			this.ThoiGianKetThucThue.DataPropertyName = "ThoiGianKetThucThue";
			this.ThoiGianKetThucThue.HeaderText = "Thời gian kết thúc thuê";
			this.ThoiGianKetThucThue.Name = "ThoiGianKetThucThue";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.btnTimKiemKH);
			this.groupBox1.Controls.Add(this.txtTimKiemKH);
			this.groupBox1.Controls.Add(this.labelCanhBao);
			this.groupBox1.Controls.Add(this.dateTimeKetThuc);
			this.groupBox1.Controls.Add(this.dateTimeBatDau);
			this.groupBox1.Controls.Add(this.txtGia);
			this.groupBox1.Controls.Add(this.cbLoaiPhong);
			this.groupBox1.Controls.Add(this.cbPhong);
			this.groupBox1.Controls.Add(this.checkBoxThuTien);
			this.groupBox1.Controls.Add(this.label11);
			this.groupBox1.Controls.Add(this.label10);
			this.groupBox1.Controls.Add(this.label9);
			this.groupBox1.Controls.Add(this.label8);
			this.groupBox1.Controls.Add(this.label7);
			this.groupBox1.Controls.Add(this.txtSdt);
			this.groupBox1.Controls.Add(this.txtCMNN);
			this.groupBox1.Controls.Add(this.txtDiaChi);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.txtTenKhachHang);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Location = new System.Drawing.Point(5, 59);
			this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.groupBox1.Size = new System.Drawing.Size(1235, 240);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Thông Tin Khách Hàng";
			// 
			// btnTimKiemKH
			// 
			this.btnTimKiemKH.Location = new System.Drawing.Point(1128, 21);
			this.btnTimKiemKH.Margin = new System.Windows.Forms.Padding(4);
			this.btnTimKiemKH.Name = "btnTimKiemKH";
			this.btnTimKiemKH.Size = new System.Drawing.Size(100, 28);
			this.btnTimKiemKH.TabIndex = 22;
			this.btnTimKiemKH.Text = "Tìm kiếm";
			this.btnTimKiemKH.UseVisualStyleBackColor = true;
			this.btnTimKiemKH.Click += new System.EventHandler(this.btnTimKiemKH_Click);
			// 
			// txtTimKiemKH
			// 
			this.txtTimKiemKH.Location = new System.Drawing.Point(955, 23);
			this.txtTimKiemKH.Margin = new System.Windows.Forms.Padding(4);
			this.txtTimKiemKH.Name = "txtTimKiemKH";
			this.txtTimKiemKH.Size = new System.Drawing.Size(150, 22);
			this.txtTimKiemKH.TabIndex = 21;
			// 
			// labelCanhBao
			// 
			this.labelCanhBao.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelCanhBao.ForeColor = System.Drawing.Color.Red;
			this.labelCanhBao.Location = new System.Drawing.Point(841, 32);
			this.labelCanhBao.Name = "labelCanhBao";
			this.labelCanhBao.Size = new System.Drawing.Size(133, 46);
			this.labelCanhBao.TabIndex = 20;
			this.labelCanhBao.Text = "Đã có khách hàng thuê";
			this.labelCanhBao.Visible = false;
			// 
			// dateTimeKetThuc
			// 
			this.dateTimeKetThuc.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimeKetThuc.Location = new System.Drawing.Point(599, 206);
			this.dateTimeKetThuc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.dateTimeKetThuc.Name = "dateTimeKetThuc";
			this.dateTimeKetThuc.Size = new System.Drawing.Size(217, 22);
			this.dateTimeKetThuc.TabIndex = 18;
			// 
			// dateTimeBatDau
			// 
			this.dateTimeBatDau.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateTimeBatDau.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.dateTimeBatDau.Location = new System.Drawing.Point(599, 161);
			this.dateTimeBatDau.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.dateTimeBatDau.Name = "dateTimeBatDau";
			this.dateTimeBatDau.Size = new System.Drawing.Size(217, 22);
			this.dateTimeBatDau.TabIndex = 17;
			this.dateTimeBatDau.Value = new System.DateTime(2023, 12, 18, 11, 52, 0, 0);
			// 
			// txtGia
			// 
			this.txtGia.Location = new System.Drawing.Point(599, 121);
			this.txtGia.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.txtGia.Name = "txtGia";
			this.txtGia.Size = new System.Drawing.Size(217, 22);
			this.txtGia.TabIndex = 16;
			// 
			// cbLoaiPhong
			// 
			this.cbLoaiPhong.FormattingEnabled = true;
			this.cbLoaiPhong.Location = new System.Drawing.Point(599, 76);
			this.cbLoaiPhong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.cbLoaiPhong.Name = "cbLoaiPhong";
			this.cbLoaiPhong.Size = new System.Drawing.Size(217, 24);
			this.cbLoaiPhong.TabIndex = 15;
			// 
			// cbPhong
			// 
			this.cbPhong.FormattingEnabled = true;
			this.cbPhong.Location = new System.Drawing.Point(599, 28);
			this.cbPhong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.cbPhong.Name = "cbPhong";
			this.cbPhong.Size = new System.Drawing.Size(217, 24);
			this.cbPhong.TabIndex = 14;
			this.cbPhong.SelectedIndexChanged += new System.EventHandler(this.cbPhong_SelectedIndexChanged);
			// 
			// checkBoxThuTien
			// 
			this.checkBoxThuTien.AutoSize = true;
			this.checkBoxThuTien.Location = new System.Drawing.Point(844, 206);
			this.checkBoxThuTien.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.checkBoxThuTien.Name = "checkBoxThuTien";
			this.checkBoxThuTien.Size = new System.Drawing.Size(99, 21);
			this.checkBoxThuTien.TabIndex = 13;
			this.checkBoxThuTien.Text = "Đã thu tiền";
			this.checkBoxThuTien.UseVisualStyleBackColor = true;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(445, 206);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(131, 17);
			this.label11.TabIndex = 12;
			this.label11.Text = "Ngày kết thúc thuê:";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(445, 161);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(129, 17);
			this.label10.TabIndex = 11;
			this.label10.Text = "Ngày bắt đầu thuê:";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(445, 76);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(83, 17);
			this.label9.TabIndex = 10;
			this.label9.Text = "Loại phòng:";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(445, 121);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(34, 17);
			this.label8.TabIndex = 9;
			this.label8.Text = "Giá:";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(445, 32);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(53, 17);
			this.label7.TabIndex = 8;
			this.label7.Text = "Phòng:";
			// 
			// txtSdt
			// 
			this.txtSdt.Location = new System.Drawing.Point(157, 121);
			this.txtSdt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.txtSdt.Name = "txtSdt";
			this.txtSdt.Size = new System.Drawing.Size(207, 22);
			this.txtSdt.TabIndex = 7;
			// 
			// txtCMNN
			// 
			this.txtCMNN.Location = new System.Drawing.Point(157, 79);
			this.txtCMNN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.txtCMNN.Name = "txtCMNN";
			this.txtCMNN.Size = new System.Drawing.Size(207, 22);
			this.txtCMNN.TabIndex = 6;
			// 
			// txtDiaChi
			// 
			this.txtDiaChi.Location = new System.Drawing.Point(157, 164);
			this.txtDiaChi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.txtDiaChi.Name = "txtDiaChi";
			this.txtDiaChi.Size = new System.Drawing.Size(207, 66);
			this.txtDiaChi.TabIndex = 5;
			this.txtDiaChi.Text = "";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(37, 164);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(55, 17);
			this.label6.TabIndex = 4;
			this.label6.Text = "Địa chỉ:";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(37, 121);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(95, 17);
			this.label5.TabIndex = 3;
			this.label5.Text = "Số điện thoại:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(37, 79);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(52, 17);
			this.label4.TabIndex = 2;
			this.label4.Text = "CMNN:";
			// 
			// txtTenKhachHang
			// 
			this.txtTenKhachHang.Location = new System.Drawing.Point(157, 34);
			this.txtTenKhachHang.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.txtTenKhachHang.Name = "txtTenKhachHang";
			this.txtTenKhachHang.Size = new System.Drawing.Size(207, 22);
			this.txtTenKhachHang.TabIndex = 1;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(37, 34);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(115, 17);
			this.label3.TabIndex = 0;
			this.label3.Text = "Tên khách hàng:";
			// 
			// tabDienNuoc
			// 
			this.tabDienNuoc.BackColor = System.Drawing.Color.White;
			this.tabDienNuoc.Controls.Add(this.label24);
			this.tabDienNuoc.Controls.Add(this.groupBox5);
			this.tabDienNuoc.Controls.Add(this.groupBox4);
			this.tabDienNuoc.Controls.Add(this.groupBox3);
			this.tabDienNuoc.Location = new System.Drawing.Point(4, 25);
			this.tabDienNuoc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.tabDienNuoc.Name = "tabDienNuoc";
			this.tabDienNuoc.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.tabDienNuoc.Size = new System.Drawing.Size(1240, 787);
			this.tabDienNuoc.TabIndex = 2;
			this.tabDienNuoc.Text = "Chỉ số điện/nước";
			// 
			// label24
			// 
			this.label24.AutoSize = true;
			this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label24.Location = new System.Drawing.Point(461, 14);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(291, 39);
			this.label24.TabIndex = 7;
			this.label24.Text = "Quản lý điện nước";
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.btnLuu);
			this.groupBox5.Controls.Add(this.btnClear);
			this.groupBox5.Controls.Add(this.txtChiSoNuocMoi);
			this.groupBox5.Controls.Add(this.txtChiSoNuocCu);
			this.groupBox5.Controls.Add(this.label17);
			this.groupBox5.Controls.Add(this.label18);
			this.groupBox5.Controls.Add(this.txtChiSoDienMoi);
			this.groupBox5.Controls.Add(this.txtChiSoDienCu);
			this.groupBox5.Controls.Add(this.label16);
			this.groupBox5.Controls.Add(this.txtPhongUpd);
			this.groupBox5.Controls.Add(this.label13);
			this.groupBox5.Controls.Add(this.label15);
			this.groupBox5.Location = new System.Drawing.Point(587, 65);
			this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.groupBox5.Size = new System.Drawing.Size(647, 190);
			this.groupBox5.TabIndex = 6;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Chỉnh sửa";
			// 
			// btnLuu
			// 
			this.btnLuu.Location = new System.Drawing.Point(524, 121);
			this.btnLuu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.btnLuu.Name = "btnLuu";
			this.btnLuu.Size = new System.Drawing.Size(91, 26);
			this.btnLuu.TabIndex = 9;
			this.btnLuu.Text = "Lưu";
			this.btnLuu.UseVisualStyleBackColor = true;
			this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
			// 
			// btnClear
			// 
			this.btnClear.Location = new System.Drawing.Point(349, 121);
			this.btnClear.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.btnClear.Name = "btnClear";
			this.btnClear.Size = new System.Drawing.Size(91, 26);
			this.btnClear.TabIndex = 10;
			this.btnClear.Text = "Làm mới";
			this.btnClear.UseVisualStyleBackColor = true;
			this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
			// 
			// txtChiSoNuocMoi
			// 
			this.txtChiSoNuocMoi.Location = new System.Drawing.Point(465, 80);
			this.txtChiSoNuocMoi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.txtChiSoNuocMoi.Name = "txtChiSoNuocMoi";
			this.txtChiSoNuocMoi.Size = new System.Drawing.Size(149, 22);
			this.txtChiSoNuocMoi.TabIndex = 13;
			// 
			// txtChiSoNuocCu
			// 
			this.txtChiSoNuocCu.Location = new System.Drawing.Point(465, 36);
			this.txtChiSoNuocCu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.txtChiSoNuocCu.Name = "txtChiSoNuocCu";
			this.txtChiSoNuocCu.Size = new System.Drawing.Size(149, 22);
			this.txtChiSoNuocCu.TabIndex = 12;
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(346, 85);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(118, 17);
			this.label17.TabIndex = 11;
			this.label17.Text = "Chỉ số nước(mới):";
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(346, 39);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(113, 17);
			this.label18.TabIndex = 10;
			this.label18.Text = "Chỉ số nước(Cũ):";
			// 
			// txtChiSoDienMoi
			// 
			this.txtChiSoDienMoi.Location = new System.Drawing.Point(149, 128);
			this.txtChiSoDienMoi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.txtChiSoDienMoi.Name = "txtChiSoDienMoi";
			this.txtChiSoDienMoi.Size = new System.Drawing.Size(157, 22);
			this.txtChiSoDienMoi.TabIndex = 8;
			// 
			// txtChiSoDienCu
			// 
			this.txtChiSoDienCu.Location = new System.Drawing.Point(149, 87);
			this.txtChiSoDienCu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.txtChiSoDienCu.Name = "txtChiSoDienCu";
			this.txtChiSoDienCu.Size = new System.Drawing.Size(157, 22);
			this.txtChiSoDienCu.TabIndex = 7;
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(32, 133);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(118, 17);
			this.label16.TabIndex = 6;
			this.label16.Text = "Chỉ số điện (mới):";
			// 
			// txtPhongUpd
			// 
			this.txtPhongUpd.Location = new System.Drawing.Point(149, 39);
			this.txtPhongUpd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.txtPhongUpd.Name = "txtPhongUpd";
			this.txtPhongUpd.Size = new System.Drawing.Size(157, 22);
			this.txtPhongUpd.TabIndex = 4;
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(32, 87);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(111, 17);
			this.label13.TabIndex = 3;
			this.label13.Text = "Chỉ số điện (cũ):";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(32, 39);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(53, 17);
			this.label15.TabIndex = 2;
			this.label15.Text = "Phòng:";
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.panel1);
			this.groupBox4.Location = new System.Drawing.Point(5, 260);
			this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.groupBox4.Size = new System.Drawing.Size(1324, 527);
			this.groupBox4.TabIndex = 1;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Danh sách phòng";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.dataGridViewSoDienNuoc);
			this.panel1.Location = new System.Drawing.Point(7, 21);
			this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1311, 534);
			this.panel1.TabIndex = 0;
			// 
			// dataGridViewSoDienNuoc
			// 
			this.dataGridViewSoDienNuoc.AllowUserToAddRows = false;
			dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle40.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle40.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle40.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle40.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewSoDienNuoc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle40;
			this.dataGridViewSoDienNuoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewSoDienNuoc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_Phong1,
            this.SoTienThanhToan1,
            this.ID_SoDien,
            this.ID_LichSuThuePhong1,
            this.ID_SoNuoc,
            this.TrangThai,
            this.SoDienCu,
            this.SoDienMoi,
            this.SoNuocCu,
            this.SoNuocMoi,
            this.SoDienSuDung,
            this.SoNuocSuDung,
            this.DonGiaDien,
            this.DonGiaNuoc,
            this.ThanhTien});
			dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle41.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle41.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle41.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle41.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle41.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridViewSoDienNuoc.DefaultCellStyle = dataGridViewCellStyle41;
			this.dataGridViewSoDienNuoc.Location = new System.Drawing.Point(3, 2);
			this.dataGridViewSoDienNuoc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.dataGridViewSoDienNuoc.Name = "dataGridViewSoDienNuoc";
			dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle42.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle42.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle42.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle42.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle42.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewSoDienNuoc.RowHeadersDefaultCellStyle = dataGridViewCellStyle42;
			this.dataGridViewSoDienNuoc.RowHeadersVisible = false;
			this.dataGridViewSoDienNuoc.RowTemplate.Height = 24;
			this.dataGridViewSoDienNuoc.Size = new System.Drawing.Size(1219, 495);
			this.dataGridViewSoDienNuoc.TabIndex = 0;
			this.dataGridViewSoDienNuoc.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewSoDienNuoc_CellClick);
			// 
			// ID_Phong1
			// 
			this.ID_Phong1.DataPropertyName = "ID_Phong";
			this.ID_Phong1.HeaderText = "Mã số phòng";
			this.ID_Phong1.Name = "ID_Phong1";
			// 
			// SoTienThanhToan1
			// 
			this.SoTienThanhToan1.DataPropertyName = "SoTienThanhToan";
			this.SoTienThanhToan1.HeaderText = "SoTienThanhToan";
			this.SoTienThanhToan1.Name = "SoTienThanhToan1";
			this.SoTienThanhToan1.Visible = false;
			// 
			// ID_SoDien
			// 
			this.ID_SoDien.DataPropertyName = "ID_SoDien";
			this.ID_SoDien.HeaderText = "ID_SoDien";
			this.ID_SoDien.Name = "ID_SoDien";
			this.ID_SoDien.Visible = false;
			// 
			// ID_LichSuThuePhong1
			// 
			this.ID_LichSuThuePhong1.DataPropertyName = "ID_LichSuThuePhong";
			this.ID_LichSuThuePhong1.HeaderText = "ID_LichSuThuePhong";
			this.ID_LichSuThuePhong1.Name = "ID_LichSuThuePhong1";
			this.ID_LichSuThuePhong1.Visible = false;
			// 
			// ID_SoNuoc
			// 
			this.ID_SoNuoc.DataPropertyName = "ID_SoNuoc";
			this.ID_SoNuoc.HeaderText = "ID_SoNuoc";
			this.ID_SoNuoc.Name = "ID_SoNuoc";
			this.ID_SoNuoc.Visible = false;
			// 
			// TrangThai
			// 
			this.TrangThai.DataPropertyName = "TrangThai";
			this.TrangThai.HeaderText = "Trạng thái";
			this.TrangThai.Name = "TrangThai";
			this.TrangThai.Visible = false;
			// 
			// SoDienCu
			// 
			this.SoDienCu.DataPropertyName = "SoDienCu";
			this.SoDienCu.HeaderText = "Số điện cũ";
			this.SoDienCu.Name = "SoDienCu";
			this.SoDienCu.Width = 80;
			// 
			// SoDienMoi
			// 
			this.SoDienMoi.DataPropertyName = "SoDienMoi";
			this.SoDienMoi.HeaderText = "Số điện mới";
			this.SoDienMoi.Name = "SoDienMoi";
			this.SoDienMoi.Width = 80;
			// 
			// SoNuocCu
			// 
			this.SoNuocCu.DataPropertyName = "SoNuocCu";
			this.SoNuocCu.HeaderText = "Số nước cũ";
			this.SoNuocCu.Name = "SoNuocCu";
			this.SoNuocCu.Width = 80;
			// 
			// SoNuocMoi
			// 
			this.SoNuocMoi.DataPropertyName = "SoNuocMoi";
			this.SoNuocMoi.HeaderText = "Số nước mới";
			this.SoNuocMoi.Name = "SoNuocMoi";
			this.SoNuocMoi.Width = 80;
			// 
			// SoDienSuDung
			// 
			this.SoDienSuDung.DataPropertyName = "SoDienSuDung";
			this.SoDienSuDung.HeaderText = "Số điện sử dụng";
			this.SoDienSuDung.Name = "SoDienSuDung";
			this.SoDienSuDung.Width = 80;
			// 
			// SoNuocSuDung
			// 
			this.SoNuocSuDung.DataPropertyName = "SoNuocSuDung";
			this.SoNuocSuDung.HeaderText = "Số nước sử dụng";
			this.SoNuocSuDung.Name = "SoNuocSuDung";
			this.SoNuocSuDung.Width = 80;
			// 
			// DonGiaDien
			// 
			this.DonGiaDien.DataPropertyName = "DonGiaDien";
			this.DonGiaDien.HeaderText = "Đơn giá điện";
			this.DonGiaDien.Name = "DonGiaDien";
			this.DonGiaDien.Width = 80;
			// 
			// DonGiaNuoc
			// 
			this.DonGiaNuoc.DataPropertyName = "DonGiaNuoc";
			this.DonGiaNuoc.HeaderText = "Đơn giá nước";
			this.DonGiaNuoc.Name = "DonGiaNuoc";
			this.DonGiaNuoc.Width = 80;
			// 
			// ThanhTien
			// 
			this.ThanhTien.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.ThanhTien.DataPropertyName = "ThanhTien";
			this.ThanhTien.HeaderText = "Thành tiền";
			this.ThanhTien.Name = "ThanhTien";
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.btnTimKiemSoDienNuoc);
			this.groupBox3.Controls.Add(this.cbTrangThai);
			this.groupBox3.Controls.Add(this.txtPhongTimKiem);
			this.groupBox3.Controls.Add(this.labeltt);
			this.groupBox3.Controls.Add(this.label14);
			this.groupBox3.Location = new System.Drawing.Point(16, 65);
			this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.groupBox3.Size = new System.Drawing.Size(544, 190);
			this.groupBox3.TabIndex = 0;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Tìm kiếm";
			// 
			// btnTimKiemSoDienNuoc
			// 
			this.btnTimKiemSoDienNuoc.Location = new System.Drawing.Point(399, 30);
			this.btnTimKiemSoDienNuoc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.btnTimKiemSoDienNuoc.Name = "btnTimKiemSoDienNuoc";
			this.btnTimKiemSoDienNuoc.Size = new System.Drawing.Size(91, 26);
			this.btnTimKiemSoDienNuoc.TabIndex = 11;
			this.btnTimKiemSoDienNuoc.Text = "Tìm kiếm";
			this.btnTimKiemSoDienNuoc.UseVisualStyleBackColor = true;
			this.btnTimKiemSoDienNuoc.Click += new System.EventHandler(this.btnTimKiemSoDienNuoc_Click);
			// 
			// cbTrangThai
			// 
			this.cbTrangThai.FormattingEnabled = true;
			this.cbTrangThai.Location = new System.Drawing.Point(140, 78);
			this.cbTrangThai.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.cbTrangThai.Name = "cbTrangThai";
			this.cbTrangThai.Size = new System.Drawing.Size(200, 24);
			this.cbTrangThai.TabIndex = 5;
			// 
			// txtPhongTimKiem
			// 
			this.txtPhongTimKiem.Location = new System.Drawing.Point(140, 34);
			this.txtPhongTimKiem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.txtPhongTimKiem.Name = "txtPhongTimKiem";
			this.txtPhongTimKiem.Size = new System.Drawing.Size(200, 22);
			this.txtPhongTimKiem.TabIndex = 4;
			// 
			// labeltt
			// 
			this.labeltt.AutoSize = true;
			this.labeltt.Location = new System.Drawing.Point(24, 86);
			this.labeltt.Name = "labeltt";
			this.labeltt.Size = new System.Drawing.Size(77, 17);
			this.labeltt.TabIndex = 3;
			this.labeltt.Text = "Trạng thái:";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(24, 39);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(53, 17);
			this.label14.TabIndex = 2;
			this.label14.Text = "Phòng:";
			// 
			// tabTinhTien
			// 
			this.tabTinhTien.BackColor = System.Drawing.Color.White;
			this.tabTinhTien.Controls.Add(this.label1);
			this.tabTinhTien.Controls.Add(this.button2);
			this.tabTinhTien.Controls.Add(this.button1);
			this.tabTinhTien.Controls.Add(this.groupBox7);
			this.tabTinhTien.Controls.Add(this.groupBox6);
			this.tabTinhTien.Location = new System.Drawing.Point(4, 25);
			this.tabTinhTien.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.tabTinhTien.Name = "tabTinhTien";
			this.tabTinhTien.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.tabTinhTien.Size = new System.Drawing.Size(1240, 787);
			this.tabTinhTien.TabIndex = 3;
			this.tabTinhTien.Text = "Tính tiền/trả phòng";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(473, 25);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(262, 39);
			this.label1.TabIndex = 8;
			this.label1.Text = "Quản lý tính tiền";
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(612, 150);
			this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(111, 31);
			this.button2.TabIndex = 2;
			this.button2.Text = "Trả phòng";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(480, 150);
			this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(99, 31);
			this.button1.TabIndex = 0;
			this.button1.Text = "Tính tiền";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// groupBox7
			// 
			this.groupBox7.Controls.Add(this.panel2);
			this.groupBox7.Location = new System.Drawing.Point(6, 212);
			this.groupBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.groupBox7.Name = "groupBox7";
			this.groupBox7.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.groupBox7.Size = new System.Drawing.Size(1238, 551);
			this.groupBox7.TabIndex = 1;
			this.groupBox7.TabStop = false;
			this.groupBox7.Text = "Danh sách phòng";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.dataGridViewTinhTien);
			this.panel2.Location = new System.Drawing.Point(5, 21);
			this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(1283, 524);
			this.panel2.TabIndex = 0;
			// 
			// dataGridViewTinhTien
			// 
			this.dataGridViewTinhTien.AllowUserToAddRows = false;
			dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle43.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle43.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle43.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle43.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle43.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewTinhTien.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle43;
			this.dataGridViewTinhTien.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewTinhTien.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_Phong2,
            this.ID_CongNo2,
            this.TrangThai2,
            this.ID_SoDien2,
            this.ID_SoNuoc2,
            this.ID_KhachHang2,
            this.TenKhachHang2,
            this.ID_LichSuThuePhong2,
            this.TongSoTien,
            this.TongSoTienDaThanhToan,
            this.TongSoTienChuaThanhToan});
			dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle44.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle44.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle44.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle44.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle44.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridViewTinhTien.DefaultCellStyle = dataGridViewCellStyle44;
			this.dataGridViewTinhTien.Location = new System.Drawing.Point(3, 4);
			this.dataGridViewTinhTien.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.dataGridViewTinhTien.Name = "dataGridViewTinhTien";
			dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle45.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle45.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle45.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle45.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle45.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewTinhTien.RowHeadersDefaultCellStyle = dataGridViewCellStyle45;
			this.dataGridViewTinhTien.RowHeadersVisible = false;
			this.dataGridViewTinhTien.RowTemplate.Height = 24;
			this.dataGridViewTinhTien.Size = new System.Drawing.Size(1211, 517);
			this.dataGridViewTinhTien.TabIndex = 0;
			this.dataGridViewTinhTien.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTinhTien_CellClick);
			// 
			// ID_Phong2
			// 
			this.ID_Phong2.DataPropertyName = "ID_Phong";
			this.ID_Phong2.HeaderText = "Mã số phòng";
			this.ID_Phong2.Name = "ID_Phong2";
			// 
			// ID_CongNo2
			// 
			this.ID_CongNo2.DataPropertyName = "ID_CongNo";
			this.ID_CongNo2.HeaderText = "ID_CongNo";
			this.ID_CongNo2.Name = "ID_CongNo2";
			this.ID_CongNo2.Visible = false;
			// 
			// TrangThai2
			// 
			this.TrangThai2.DataPropertyName = "TrangThai";
			this.TrangThai2.HeaderText = "Trạng thái";
			this.TrangThai2.Name = "TrangThai2";
			this.TrangThai2.Visible = false;
			// 
			// ID_SoDien2
			// 
			this.ID_SoDien2.DataPropertyName = "ID_SoDien";
			this.ID_SoDien2.HeaderText = "ID_SoDien2";
			this.ID_SoDien2.Name = "ID_SoDien2";
			this.ID_SoDien2.Visible = false;
			// 
			// ID_SoNuoc2
			// 
			this.ID_SoNuoc2.DataPropertyName = "ID_SoNuoc";
			this.ID_SoNuoc2.HeaderText = "ID_SoNuoc2";
			this.ID_SoNuoc2.Name = "ID_SoNuoc2";
			this.ID_SoNuoc2.Visible = false;
			// 
			// ID_KhachHang2
			// 
			this.ID_KhachHang2.DataPropertyName = "ID_KhachHang";
			this.ID_KhachHang2.HeaderText = "ID_KhachHang";
			this.ID_KhachHang2.Name = "ID_KhachHang2";
			this.ID_KhachHang2.Visible = false;
			// 
			// TenKhachHang2
			// 
			this.TenKhachHang2.DataPropertyName = "TenKhachHang";
			this.TenKhachHang2.HeaderText = "Tên khách hàng";
			this.TenKhachHang2.Name = "TenKhachHang2";
			this.TenKhachHang2.Width = 200;
			// 
			// ID_LichSuThuePhong2
			// 
			this.ID_LichSuThuePhong2.DataPropertyName = "ID_LichSuThuePhong";
			this.ID_LichSuThuePhong2.HeaderText = "ID_LichSuThuePhong";
			this.ID_LichSuThuePhong2.Name = "ID_LichSuThuePhong2";
			this.ID_LichSuThuePhong2.Visible = false;
			// 
			// TongSoTien
			// 
			this.TongSoTien.DataPropertyName = "TongSoTien";
			this.TongSoTien.HeaderText = "Tông số tiền";
			this.TongSoTien.Name = "TongSoTien";
			// 
			// TongSoTienDaThanhToan
			// 
			this.TongSoTienDaThanhToan.DataPropertyName = "TongSoTienDaThanhToan";
			this.TongSoTienDaThanhToan.HeaderText = "Tổng số tiền đã thanh toán";
			this.TongSoTienDaThanhToan.Name = "TongSoTienDaThanhToan";
			// 
			// TongSoTienChuaThanhToan
			// 
			this.TongSoTienChuaThanhToan.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.TongSoTienChuaThanhToan.DataPropertyName = "TongSoTienChuaThanhToan";
			this.TongSoTienChuaThanhToan.HeaderText = "Tổng số tiền chưa thanh toán";
			this.TongSoTienChuaThanhToan.Name = "TongSoTienChuaThanhToan";
			// 
			// groupBox6
			// 
			this.groupBox6.Controls.Add(this.label21);
			this.groupBox6.Controls.Add(this.label20);
			this.groupBox6.Controls.Add(this.comboBox1);
			this.groupBox6.Controls.Add(this.btnTImKiemTinhTien);
			this.groupBox6.Controls.Add(this.txtTImKiemTinhTien);
			this.groupBox6.Controls.Add(this.label19);
			this.groupBox6.Location = new System.Drawing.Point(21, 20);
			this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.groupBox6.Size = new System.Drawing.Size(383, 161);
			this.groupBox6.TabIndex = 0;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "Tìm kiếm";
			// 
			// label21
			// 
			this.label21.AutoSize = true;
			this.label21.Location = new System.Drawing.Point(27, 89);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(160, 17);
			this.label21.TabIndex = 8;
			this.label21.Text = "Phòng/ Tên khách hàng";
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Location = new System.Drawing.Point(24, 22);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(73, 17);
			this.label20.TabIndex = 7;
			this.label20.Text = "Trạng thái";
			// 
			// comboBox1
			// 
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Location = new System.Drawing.Point(27, 42);
			this.comboBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(180, 24);
			this.comboBox1.TabIndex = 6;
			// 
			// btnTImKiemTinhTien
			// 
			this.btnTImKiemTinhTien.Location = new System.Drawing.Point(257, 34);
			this.btnTImKiemTinhTien.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.btnTImKiemTinhTien.Name = "btnTImKiemTinhTien";
			this.btnTImKiemTinhTien.Size = new System.Drawing.Size(101, 32);
			this.btnTImKiemTinhTien.TabIndex = 2;
			this.btnTImKiemTinhTien.Text = "Tìm kiếm";
			this.btnTImKiemTinhTien.UseVisualStyleBackColor = true;
			this.btnTImKiemTinhTien.Click += new System.EventHandler(this.btnTImKiemTinhTien_Click);
			// 
			// txtTImKiemTinhTien
			// 
			this.txtTImKiemTinhTien.Location = new System.Drawing.Point(27, 112);
			this.txtTImKiemTinhTien.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.txtTImKiemTinhTien.Name = "txtTImKiemTinhTien";
			this.txtTImKiemTinhTien.Size = new System.Drawing.Size(180, 22);
			this.txtTImKiemTinhTien.TabIndex = 1;
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(19, 22);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(0, 17);
			this.label19.TabIndex = 0;
			// 
			// tabgeBaoCao
			// 
			this.tabgeBaoCao.Controls.Add(this.label12);
			this.tabgeBaoCao.Controls.Add(this.panel3);
			this.tabgeBaoCao.Controls.Add(this.bc);
			this.tabgeBaoCao.Location = new System.Drawing.Point(4, 25);
			this.tabgeBaoCao.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.tabgeBaoCao.Name = "tabgeBaoCao";
			this.tabgeBaoCao.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.tabgeBaoCao.Size = new System.Drawing.Size(1240, 787);
			this.tabgeBaoCao.TabIndex = 4;
			this.tabgeBaoCao.Text = "Báo cáo";
			this.tabgeBaoCao.UseVisualStyleBackColor = true;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(12, 144);
			this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(111, 17);
			this.label12.TabIndex = 0;
			this.label12.Text = "Hiển thị báo cáo";
			// 
			// panel3
			// 
			this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel3.Controls.Add(this.dgvBaoCao);
			this.panel3.Location = new System.Drawing.Point(8, 153);
			this.panel3.Margin = new System.Windows.Forms.Padding(4);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(1232, 625);
			this.panel3.TabIndex = 2;
			// 
			// dgvBaoCao
			// 
			this.dgvBaoCao.AllowUserToAddRows = false;
			dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle46.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle46.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle46.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle46.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle46.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvBaoCao.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle46;
			this.dgvBaoCao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvBaoCao.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Bc_ID_Phong,
            this.Bc_TrangThai,
            this.Bc_ID_KhachHang,
            this.Bc_TenKhachHang,
            this.Bc_ID_SoDien,
            this.Bc_ID_SoNuoc,
            this.Bc_ID_LichSuThuePhong,
            this.Bc_ThoiGianBatDauThue,
            this.Bc_ThoiGianKetThucThue,
            this.Bc_SoThangDaThue,
            this.Bc_SoDienSuDung,
            this.Bc_SoNuocDaSuDung,
            this.Bc_TongSoTien,
            this.Bc_TongSoTienDaThanhToan,
            this.Bc_TongSoTienChuaThanhToan,
            this.Bc_DateFoodter});
			dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle47.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle47.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle47.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle47.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle47.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.dgvBaoCao.DefaultCellStyle = dataGridViewCellStyle47;
			this.dgvBaoCao.Location = new System.Drawing.Point(4, 10);
			this.dgvBaoCao.Margin = new System.Windows.Forms.Padding(4);
			this.dgvBaoCao.Name = "dgvBaoCao";
			dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle48.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle48.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle48.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle48.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle48.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvBaoCao.RowHeadersDefaultCellStyle = dataGridViewCellStyle48;
			this.dgvBaoCao.RowHeadersVisible = false;
			this.dgvBaoCao.Size = new System.Drawing.Size(1220, 609);
			this.dgvBaoCao.TabIndex = 0;
			// 
			// Bc_ID_Phong
			// 
			this.Bc_ID_Phong.DataPropertyName = "ID_Phong";
			this.Bc_ID_Phong.HeaderText = "Mã phòng";
			this.Bc_ID_Phong.Name = "Bc_ID_Phong";
			this.Bc_ID_Phong.Width = 60;
			// 
			// Bc_TrangThai
			// 
			this.Bc_TrangThai.DataPropertyName = "TrangThai";
			this.Bc_TrangThai.HeaderText = "Trạng thái";
			this.Bc_TrangThai.Name = "Bc_TrangThai";
			// 
			// Bc_ID_KhachHang
			// 
			this.Bc_ID_KhachHang.DataPropertyName = "ID_KhachHang";
			this.Bc_ID_KhachHang.HeaderText = "ID_KhachHang";
			this.Bc_ID_KhachHang.Name = "Bc_ID_KhachHang";
			this.Bc_ID_KhachHang.Visible = false;
			// 
			// Bc_TenKhachHang
			// 
			this.Bc_TenKhachHang.DataPropertyName = "TenKhachHang";
			this.Bc_TenKhachHang.HeaderText = "Tên khách hàng";
			this.Bc_TenKhachHang.Name = "Bc_TenKhachHang";
			// 
			// Bc_ID_SoDien
			// 
			this.Bc_ID_SoDien.DataPropertyName = "ID_SoDien";
			this.Bc_ID_SoDien.HeaderText = "ID_SoDien";
			this.Bc_ID_SoDien.Name = "Bc_ID_SoDien";
			this.Bc_ID_SoDien.Visible = false;
			// 
			// Bc_ID_SoNuoc
			// 
			this.Bc_ID_SoNuoc.DataPropertyName = "ID_SoNuoc";
			this.Bc_ID_SoNuoc.HeaderText = "ID_SoNuoc";
			this.Bc_ID_SoNuoc.Name = "Bc_ID_SoNuoc";
			this.Bc_ID_SoNuoc.Visible = false;
			// 
			// Bc_ID_LichSuThuePhong
			// 
			this.Bc_ID_LichSuThuePhong.DataPropertyName = "ID_LichSuThuePhong";
			this.Bc_ID_LichSuThuePhong.HeaderText = "ID_LichSuThuePhong";
			this.Bc_ID_LichSuThuePhong.Name = "Bc_ID_LichSuThuePhong";
			this.Bc_ID_LichSuThuePhong.Visible = false;
			// 
			// Bc_ThoiGianBatDauThue
			// 
			this.Bc_ThoiGianBatDauThue.DataPropertyName = "ThoiGianBatDauThue";
			this.Bc_ThoiGianBatDauThue.HeaderText = "Bắt đầu thuê";
			this.Bc_ThoiGianBatDauThue.Name = "Bc_ThoiGianBatDauThue";
			// 
			// Bc_ThoiGianKetThucThue
			// 
			this.Bc_ThoiGianKetThucThue.DataPropertyName = "ThoiGianKetThucThue";
			this.Bc_ThoiGianKetThucThue.HeaderText = "Kết thúc thuê";
			this.Bc_ThoiGianKetThucThue.Name = "Bc_ThoiGianKetThucThue";
			// 
			// Bc_SoThangDaThue
			// 
			this.Bc_SoThangDaThue.DataPropertyName = "SoThangDaThue";
			this.Bc_SoThangDaThue.HeaderText = "Số tháng đã thuê";
			this.Bc_SoThangDaThue.Name = "Bc_SoThangDaThue";
			this.Bc_SoThangDaThue.Width = 60;
			// 
			// Bc_SoDienSuDung
			// 
			this.Bc_SoDienSuDung.DataPropertyName = "SoDienSuDung";
			this.Bc_SoDienSuDung.HeaderText = "Số điện đã sử dụng";
			this.Bc_SoDienSuDung.Name = "Bc_SoDienSuDung";
			this.Bc_SoDienSuDung.Width = 60;
			// 
			// Bc_SoNuocDaSuDung
			// 
			this.Bc_SoNuocDaSuDung.DataPropertyName = "SoNuocDaSuDung";
			this.Bc_SoNuocDaSuDung.HeaderText = "Số nước đã sử dụng";
			this.Bc_SoNuocDaSuDung.Name = "Bc_SoNuocDaSuDung";
			this.Bc_SoNuocDaSuDung.Width = 60;
			// 
			// Bc_TongSoTien
			// 
			this.Bc_TongSoTien.DataPropertyName = "TongSoTien";
			this.Bc_TongSoTien.HeaderText = "Tổng số tiền";
			this.Bc_TongSoTien.Name = "Bc_TongSoTien";
			// 
			// Bc_TongSoTienDaThanhToan
			// 
			this.Bc_TongSoTienDaThanhToan.DataPropertyName = "TongSoTienDaThanhToan";
			this.Bc_TongSoTienDaThanhToan.HeaderText = "Số tiền đã thanh toán";
			this.Bc_TongSoTienDaThanhToan.Name = "Bc_TongSoTienDaThanhToan";
			// 
			// Bc_TongSoTienChuaThanhToan
			// 
			this.Bc_TongSoTienChuaThanhToan.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.Bc_TongSoTienChuaThanhToan.DataPropertyName = "TongSoTienChuaThanhToan";
			this.Bc_TongSoTienChuaThanhToan.HeaderText = "Số tiền chưa thanh toán";
			this.Bc_TongSoTienChuaThanhToan.Name = "Bc_TongSoTienChuaThanhToan";
			// 
			// Bc_DateFoodter
			// 
			this.Bc_DateFoodter.DataPropertyName = "DateFoodter";
			this.Bc_DateFoodter.HeaderText = "DateFoodter";
			this.Bc_DateFoodter.Name = "Bc_DateFoodter";
			this.Bc_DateFoodter.Visible = false;
			// 
			// bc
			// 
			this.bc.Controls.Add(this.dateBaoBao);
			this.bc.Controls.Add(this.label25);
			this.bc.Controls.Add(this.label23);
			this.bc.Controls.Add(this.comboBoxtt);
			this.bc.Controls.Add(this.textBoxTimKiem);
			this.bc.Controls.Add(this.labKhachHag);
			this.bc.Controls.Add(this.btnBoaCao);
			this.bc.Location = new System.Drawing.Point(5, 6);
			this.bc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.bc.Name = "bc";
			this.bc.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.bc.Size = new System.Drawing.Size(1228, 126);
			this.bc.TabIndex = 1;
			this.bc.TabStop = false;
			this.bc.Text = "Báo cáo";
			// 
			// dateBaoBao
			// 
			this.dateBaoBao.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateBaoBao.Location = new System.Drawing.Point(127, 28);
			this.dateBaoBao.Margin = new System.Windows.Forms.Padding(4);
			this.dateBaoBao.Name = "dateBaoBao";
			this.dateBaoBao.Size = new System.Drawing.Size(180, 22);
			this.dateBaoBao.TabIndex = 9;
			// 
			// label25
			// 
			this.label25.AutoSize = true;
			this.label25.Location = new System.Drawing.Point(364, 81);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(77, 17);
			this.label25.TabIndex = 8;
			this.label25.Text = "Trạng thái:";
			// 
			// label23
			// 
			this.label23.AutoSize = true;
			this.label23.Location = new System.Drawing.Point(24, 28);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(56, 17);
			this.label23.TabIndex = 6;
			this.label23.Text = "Từ gày:";
			// 
			// comboBoxtt
			// 
			this.comboBoxtt.FormattingEnabled = true;
			this.comboBoxtt.Location = new System.Drawing.Point(447, 78);
			this.comboBoxtt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.comboBoxtt.Name = "comboBoxtt";
			this.comboBoxtt.Size = new System.Drawing.Size(200, 24);
			this.comboBoxtt.TabIndex = 4;
			// 
			// textBoxTimKiem
			// 
			this.textBoxTimKiem.Location = new System.Drawing.Point(127, 78);
			this.textBoxTimKiem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.textBoxTimKiem.Name = "textBoxTimKiem";
			this.textBoxTimKiem.Size = new System.Drawing.Size(180, 22);
			this.textBoxTimKiem.TabIndex = 3;
			// 
			// labKhachHag
			// 
			this.labKhachHag.AutoSize = true;
			this.labKhachHag.Location = new System.Drawing.Point(24, 64);
			this.labKhachHag.MaximumSize = new System.Drawing.Size(91, 0);
			this.labKhachHag.Name = "labKhachHag";
			this.labKhachHag.Size = new System.Drawing.Size(84, 34);
			this.labKhachHag.TabIndex = 1;
			this.labKhachHag.Text = "Phòng Khách hàng";
			// 
			// btnBoaCao
			// 
			this.btnBoaCao.Location = new System.Drawing.Point(683, 80);
			this.btnBoaCao.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.btnBoaCao.Name = "btnBoaCao";
			this.btnBoaCao.Size = new System.Drawing.Size(105, 26);
			this.btnBoaCao.TabIndex = 0;
			this.btnBoaCao.Text = "Tạo báo cáo";
			this.btnBoaCao.UseVisualStyleBackColor = true;
			this.btnBoaCao.Click += new System.EventHandler(this.btnBaoCao_Click);
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.btnDangXuat);
			this.tabPage1.Controls.Add(this.groupBox9);
			this.tabPage1.Controls.Add(this.groupBox8);
			this.tabPage1.Controls.Add(this.dgvTaiKhoan);
			this.tabPage1.Location = new System.Drawing.Point(4, 25);
			this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
			this.tabPage1.Size = new System.Drawing.Size(1240, 787);
			this.tabPage1.TabIndex = 5;
			this.tabPage1.Text = "Tài khoản";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// btnDangXuat
			// 
			this.btnDangXuat.Location = new System.Drawing.Point(986, 180);
			this.btnDangXuat.Margin = new System.Windows.Forms.Padding(4);
			this.btnDangXuat.Name = "btnDangXuat";
			this.btnDangXuat.Size = new System.Drawing.Size(100, 28);
			this.btnDangXuat.TabIndex = 3;
			this.btnDangXuat.Text = "Đăng xuất";
			this.btnDangXuat.UseVisualStyleBackColor = true;
			this.btnDangXuat.Click += new System.EventHandler(this.btnDangXuat_Click);
			// 
			// groupBox9
			// 
			this.groupBox9.Controls.Add(this.label29);
			this.groupBox9.Controls.Add(this.btnTimKiemTK);
			this.groupBox9.Controls.Add(this.txtTimKiemTK);
			this.groupBox9.Location = new System.Drawing.Point(831, 20);
			this.groupBox9.Margin = new System.Windows.Forms.Padding(4);
			this.groupBox9.Name = "groupBox9";
			this.groupBox9.Padding = new System.Windows.Forms.Padding(4);
			this.groupBox9.Size = new System.Drawing.Size(396, 108);
			this.groupBox9.TabIndex = 2;
			this.groupBox9.TabStop = false;
			this.groupBox9.Text = "Tìm kiếm";
			// 
			// label29
			// 
			this.label29.AutoSize = true;
			this.label29.Location = new System.Drawing.Point(8, 47);
			this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label29.Name = "label29";
			this.label29.Size = new System.Drawing.Size(99, 17);
			this.label29.TabIndex = 9;
			this.label29.Text = "Tên tài khoản:";
			// 
			// btnTimKiemTK
			// 
			this.btnTimKiemTK.Location = new System.Drawing.Point(288, 40);
			this.btnTimKiemTK.Margin = new System.Windows.Forms.Padding(4);
			this.btnTimKiemTK.Name = "btnTimKiemTK";
			this.btnTimKiemTK.Size = new System.Drawing.Size(100, 28);
			this.btnTimKiemTK.TabIndex = 2;
			this.btnTimKiemTK.Text = "Tìm kiếm";
			this.btnTimKiemTK.UseVisualStyleBackColor = true;
			this.btnTimKiemTK.Click += new System.EventHandler(this.btnTimKiemTK_Click);
			// 
			// txtTimKiemTK
			// 
			this.txtTimKiemTK.Location = new System.Drawing.Point(118, 44);
			this.txtTimKiemTK.Margin = new System.Windows.Forms.Padding(4);
			this.txtTimKiemTK.Name = "txtTimKiemTK";
			this.txtTimKiemTK.Size = new System.Drawing.Size(162, 22);
			this.txtTimKiemTK.TabIndex = 0;
			// 
			// groupBox8
			// 
			this.groupBox8.Controls.Add(this.btnLamMoi);
			this.groupBox8.Controls.Add(this.btnSuaTK);
			this.groupBox8.Controls.Add(this.btnXoaTK);
			this.groupBox8.Controls.Add(this.btnThemTK);
			this.groupBox8.Controls.Add(this.cbQuyen);
			this.groupBox8.Controls.Add(this.labMatKhauMoi);
			this.groupBox8.Controls.Add(this.label28);
			this.groupBox8.Controls.Add(this.label27);
			this.groupBox8.Controls.Add(this.label26);
			this.groupBox8.Controls.Add(this.label22);
			this.groupBox8.Controls.Add(this.txtNhapLaiMK);
			this.groupBox8.Controls.Add(this.txtMatKhau);
			this.groupBox8.Controls.Add(this.txtTenTK);
			this.groupBox8.Controls.Add(this.txtMaTK);
			this.groupBox8.Location = new System.Drawing.Point(9, 20);
			this.groupBox8.Margin = new System.Windows.Forms.Padding(4);
			this.groupBox8.Name = "groupBox8";
			this.groupBox8.Padding = new System.Windows.Forms.Padding(4);
			this.groupBox8.Size = new System.Drawing.Size(814, 206);
			this.groupBox8.TabIndex = 1;
			this.groupBox8.TabStop = false;
			this.groupBox8.Text = "Thông tin tài khoản";
			// 
			// btnLamMoi
			// 
			this.btnLamMoi.Location = new System.Drawing.Point(322, 154);
			this.btnLamMoi.Margin = new System.Windows.Forms.Padding(4);
			this.btnLamMoi.Name = "btnLamMoi";
			this.btnLamMoi.Size = new System.Drawing.Size(100, 28);
			this.btnLamMoi.TabIndex = 11;
			this.btnLamMoi.Text = "Làm mới";
			this.btnLamMoi.UseVisualStyleBackColor = true;
			this.btnLamMoi.Click += new System.EventHandler(this.btnLamMoi_Click);
			// 
			// btnSuaTK
			// 
			this.btnSuaTK.Location = new System.Drawing.Point(557, 154);
			this.btnSuaTK.Margin = new System.Windows.Forms.Padding(4);
			this.btnSuaTK.Name = "btnSuaTK";
			this.btnSuaTK.Size = new System.Drawing.Size(100, 28);
			this.btnSuaTK.TabIndex = 1;
			this.btnSuaTK.Text = "Sửa";
			this.btnSuaTK.UseVisualStyleBackColor = true;
			this.btnSuaTK.Click += new System.EventHandler(this.btnSuaTK_Click);
			// 
			// btnXoaTK
			// 
			this.btnXoaTK.Location = new System.Drawing.Point(677, 154);
			this.btnXoaTK.Margin = new System.Windows.Forms.Padding(4);
			this.btnXoaTK.Name = "btnXoaTK";
			this.btnXoaTK.Size = new System.Drawing.Size(100, 28);
			this.btnXoaTK.TabIndex = 2;
			this.btnXoaTK.Text = "Xóa";
			this.btnXoaTK.UseVisualStyleBackColor = true;
			this.btnXoaTK.Click += new System.EventHandler(this.btnXoaTK_Click);
			// 
			// btnThemTK
			// 
			this.btnThemTK.Location = new System.Drawing.Point(443, 154);
			this.btnThemTK.Margin = new System.Windows.Forms.Padding(4);
			this.btnThemTK.Name = "btnThemTK";
			this.btnThemTK.Size = new System.Drawing.Size(100, 28);
			this.btnThemTK.TabIndex = 0;
			this.btnThemTK.Text = "Thêm";
			this.btnThemTK.UseVisualStyleBackColor = true;
			this.btnThemTK.Click += new System.EventHandler(this.btnThemTK_Click);
			// 
			// cbQuyen
			// 
			this.cbQuyen.FormattingEnabled = true;
			this.cbQuyen.Location = new System.Drawing.Point(135, 154);
			this.cbQuyen.Margin = new System.Windows.Forms.Padding(4);
			this.cbQuyen.Name = "cbQuyen";
			this.cbQuyen.Size = new System.Drawing.Size(160, 24);
			this.cbQuyen.TabIndex = 10;
			// 
			// labMatKhauMoi
			// 
			this.labMatKhauMoi.AutoSize = true;
			this.labMatKhauMoi.Location = new System.Drawing.Point(405, 103);
			this.labMatKhauMoi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.labMatKhauMoi.Name = "labMatKhauMoi";
			this.labMatKhauMoi.Size = new System.Drawing.Size(96, 17);
			this.labMatKhauMoi.TabIndex = 9;
			this.labMatKhauMoi.Text = "Mật khẩu mới:";
			// 
			// label28
			// 
			this.label28.AutoSize = true;
			this.label28.Location = new System.Drawing.Point(405, 46);
			this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size(70, 17);
			this.label28.TabIndex = 8;
			this.label28.Text = "Mật khẩu:";
			// 
			// label27
			// 
			this.label27.AutoSize = true;
			this.label27.Location = new System.Drawing.Point(23, 160);
			this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(54, 17);
			this.label27.TabIndex = 7;
			this.label27.Text = "Quyền:";
			// 
			// label26
			// 
			this.label26.AutoSize = true;
			this.label26.Location = new System.Drawing.Point(23, 107);
			this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(99, 17);
			this.label26.TabIndex = 6;
			this.label26.Text = "Tên tài khoản:";
			// 
			// label22
			// 
			this.label22.AutoSize = true;
			this.label22.Location = new System.Drawing.Point(23, 46);
			this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(93, 17);
			this.label22.TabIndex = 5;
			this.label22.Text = "Mã tài khoản:";
			// 
			// txtNhapLaiMK
			// 
			this.txtNhapLaiMK.Location = new System.Drawing.Point(515, 95);
			this.txtNhapLaiMK.Margin = new System.Windows.Forms.Padding(4);
			this.txtNhapLaiMK.Name = "txtNhapLaiMK";
			this.txtNhapLaiMK.Size = new System.Drawing.Size(164, 22);
			this.txtNhapLaiMK.TabIndex = 3;
			// 
			// txtMatKhau
			// 
			this.txtMatKhau.Location = new System.Drawing.Point(515, 42);
			this.txtMatKhau.Margin = new System.Windows.Forms.Padding(4);
			this.txtMatKhau.Name = "txtMatKhau";
			this.txtMatKhau.Size = new System.Drawing.Size(164, 22);
			this.txtMatKhau.TabIndex = 2;
			// 
			// txtTenTK
			// 
			this.txtTenTK.Location = new System.Drawing.Point(135, 98);
			this.txtTenTK.Margin = new System.Windows.Forms.Padding(4);
			this.txtTenTK.Name = "txtTenTK";
			this.txtTenTK.Size = new System.Drawing.Size(164, 22);
			this.txtTenTK.TabIndex = 1;
			// 
			// txtMaTK
			// 
			this.txtMaTK.Location = new System.Drawing.Point(135, 46);
			this.txtMaTK.Margin = new System.Windows.Forms.Padding(4);
			this.txtMaTK.Name = "txtMaTK";
			this.txtMaTK.Size = new System.Drawing.Size(164, 22);
			this.txtMaTK.TabIndex = 0;
			// 
			// dgvTaiKhoan
			// 
			this.dgvTaiKhoan.AllowUserToAddRows = false;
			this.dgvTaiKhoan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvTaiKhoan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_TaiKhoan,
            this.DelFlg,
            this.TenTaiKhoan,
            this.Password,
            this.Quyen_TK});
			this.dgvTaiKhoan.Location = new System.Drawing.Point(8, 249);
			this.dgvTaiKhoan.Margin = new System.Windows.Forms.Padding(4);
			this.dgvTaiKhoan.Name = "dgvTaiKhoan";
			this.dgvTaiKhoan.RowHeadersVisible = false;
			this.dgvTaiKhoan.Size = new System.Drawing.Size(1219, 503);
			this.dgvTaiKhoan.TabIndex = 0;
			this.dgvTaiKhoan.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTaiKhoan_CellClick);
			// 
			// ID_TaiKhoan
			// 
			this.ID_TaiKhoan.DataPropertyName = "ID_TaiKhoan";
			this.ID_TaiKhoan.HeaderText = "Mã tài khoản";
			this.ID_TaiKhoan.Name = "ID_TaiKhoan";
			this.ID_TaiKhoan.Width = 200;
			// 
			// DelFlg
			// 
			this.DelFlg.DataPropertyName = "DelFlg";
			this.DelFlg.HeaderText = "DelFlg";
			this.DelFlg.Name = "DelFlg";
			this.DelFlg.Visible = false;
			// 
			// TenTaiKhoan
			// 
			this.TenTaiKhoan.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.TenTaiKhoan.DataPropertyName = "TenTaiKhoan";
			this.TenTaiKhoan.HeaderText = "Tên tài khoản";
			this.TenTaiKhoan.Name = "TenTaiKhoan";
			// 
			// Password
			// 
			this.Password.DataPropertyName = "Password";
			this.Password.HeaderText = "Mật khẩu";
			this.Password.Name = "Password";
			// 
			// Quyen_TK
			// 
			this.Quyen_TK.DataPropertyName = "Quyen";
			this.Quyen_TK.HeaderText = "Quyền";
			this.Quyen_TK.Name = "Quyen_TK";
			this.Quyen_TK.Width = 200;
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
			// 
			// groupBoxTimKiem
			// 
			this.groupBoxTimKiem.BackColor = System.Drawing.Color.White;
			this.groupBoxTimKiem.Controls.Add(this.btnTimKiem);
			this.groupBoxTimKiem.Controls.Add(this.txtSoPhong);
			this.groupBoxTimKiem.Controls.Add(this.cbTrangThaiPhi);
			this.groupBoxTimKiem.Controls.Add(this.cbTrangThaiPhong);
			this.groupBoxTimKiem.Location = new System.Drawing.Point(5, 14);
			this.groupBoxTimKiem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.groupBoxTimKiem.Name = "groupBoxTimKiem";
			this.groupBoxTimKiem.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.groupBoxTimKiem.Size = new System.Drawing.Size(828, 70);
			this.groupBoxTimKiem.TabIndex = 0;
			this.groupBoxTimKiem.TabStop = false;
			this.groupBoxTimKiem.Text = "Tìm kiếm";
			// 
			// cbTrangThaiPhong
			// 
			this.cbTrangThaiPhong.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbTrangThaiPhong.FormattingEnabled = true;
			this.cbTrangThaiPhong.IntegralHeight = false;
			this.cbTrangThaiPhong.Items.AddRange(new object[] {
            "-- Trạng thái phòng ---",
            "-- Đã thuê --",
            "-- Chưa thuê --"});
			this.cbTrangThaiPhong.Location = new System.Drawing.Point(17, 27);
			this.cbTrangThaiPhong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.cbTrangThaiPhong.Name = "cbTrangThaiPhong";
			this.cbTrangThaiPhong.Size = new System.Drawing.Size(205, 28);
			this.cbTrangThaiPhong.TabIndex = 0;
			// 
			// cbTrangThaiPhi
			// 
			this.cbTrangThaiPhi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbTrangThaiPhi.FormattingEnabled = true;
			this.cbTrangThaiPhi.Location = new System.Drawing.Point(249, 27);
			this.cbTrangThaiPhi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.cbTrangThaiPhi.Name = "cbTrangThaiPhi";
			this.cbTrangThaiPhi.Size = new System.Drawing.Size(207, 28);
			this.cbTrangThaiPhi.TabIndex = 1;
			// 
			// txtSoPhong
			// 
			this.txtSoPhong.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtSoPhong.Location = new System.Drawing.Point(491, 27);
			this.txtSoPhong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.txtSoPhong.Name = "txtSoPhong";
			this.txtSoPhong.Size = new System.Drawing.Size(205, 26);
			this.txtSoPhong.TabIndex = 2;
			// 
			// btnTimKiem
			// 
			this.btnTimKiem.Location = new System.Drawing.Point(713, 25);
			this.btnTimKiem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.btnTimKiem.Name = "btnTimKiem";
			this.btnTimKiem.Size = new System.Drawing.Size(93, 30);
			this.btnTimKiem.TabIndex = 3;
			this.btnTimKiem.Text = "Tìm kiếm";
			this.btnTimKiem.UseVisualStyleBackColor = true;
			this.btnTimKiem.Click += new System.EventHandler(this.btnTimKiem_Click);
			// 
			// groupBoxTrangChu
			// 
			this.groupBoxTrangChu.BackColor = System.Drawing.Color.White;
			this.groupBoxTrangChu.Controls.Add(this.panelPhong);
			this.groupBoxTrangChu.Location = new System.Drawing.Point(5, 133);
			this.groupBoxTrangChu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.groupBoxTrangChu.Name = "groupBoxTrangChu";
			this.groupBoxTrangChu.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.groupBoxTrangChu.Size = new System.Drawing.Size(1239, 642);
			this.groupBoxTrangChu.TabIndex = 1;
			this.groupBoxTrangChu.TabStop = false;
			this.groupBoxTrangChu.Text = "Danh sách phòng";
			// 
			// panelPhong
			// 
			this.panelPhong.AutoScroll = true;
			this.panelPhong.BackColor = System.Drawing.Color.White;
			this.panelPhong.Location = new System.Drawing.Point(6, 19);
			this.panelPhong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.panelPhong.Name = "panelPhong";
			this.panelPhong.Size = new System.Drawing.Size(1223, 607);
			this.panelPhong.TabIndex = 0;
			// 
			// labelTrong
			// 
			this.labelTrong.AutoSize = true;
			this.labelTrong.Location = new System.Drawing.Point(13, 103);
			this.labelTrong.Name = "labelTrong";
			this.labelTrong.Size = new System.Drawing.Size(74, 17);
			this.labelTrong.TabIndex = 2;
			this.labelTrong.Text = "Còn trống:";
			// 
			// labelSoPhongTrong
			// 
			this.labelSoPhongTrong.AutoSize = true;
			this.labelSoPhongTrong.Location = new System.Drawing.Point(97, 103);
			this.labelSoPhongTrong.Name = "labelSoPhongTrong";
			this.labelSoPhongTrong.Size = new System.Drawing.Size(16, 17);
			this.labelSoPhongTrong.TabIndex = 3;
			this.labelSoPhongTrong.Text = "0";
			// 
			// labelDaChoThue
			// 
			this.labelDaChoThue.AutoSize = true;
			this.labelDaChoThue.Location = new System.Drawing.Point(139, 103);
			this.labelDaChoThue.Name = "labelDaChoThue";
			this.labelDaChoThue.Size = new System.Drawing.Size(89, 17);
			this.labelDaChoThue.TabIndex = 4;
			this.labelDaChoThue.Text = "Đã cho thuê:";
			// 
			// labelSoPhongChoThue
			// 
			this.labelSoPhongChoThue.AutoSize = true;
			this.labelSoPhongChoThue.Location = new System.Drawing.Point(239, 103);
			this.labelSoPhongChoThue.Name = "labelSoPhongChoThue";
			this.labelSoPhongChoThue.Size = new System.Drawing.Size(16, 17);
			this.labelSoPhongChoThue.TabIndex = 5;
			this.labelSoPhongChoThue.Text = "0";
			// 
			// labelChuaThuPhi
			// 
			this.labelChuaThuPhi.AutoSize = true;
			this.labelChuaThuPhi.Location = new System.Drawing.Point(280, 103);
			this.labelChuaThuPhi.Name = "labelChuaThuPhi";
			this.labelChuaThuPhi.Size = new System.Drawing.Size(92, 17);
			this.labelChuaThuPhi.TabIndex = 6;
			this.labelChuaThuPhi.Text = "Chưa thu phí:";
			// 
			// labelSoPhongChuaThuPhi
			// 
			this.labelSoPhongChuaThuPhi.AutoSize = true;
			this.labelSoPhongChuaThuPhi.Location = new System.Drawing.Point(381, 103);
			this.labelSoPhongChuaThuPhi.Name = "labelSoPhongChuaThuPhi";
			this.labelSoPhongChuaThuPhi.Size = new System.Drawing.Size(16, 17);
			this.labelSoPhongChuaThuPhi.TabIndex = 7;
			this.labelSoPhongChuaThuPhi.Text = "0";
			// 
			// btnThemPhongTro
			// 
			this.btnThemPhongTro.Location = new System.Drawing.Point(497, 96);
			this.btnThemPhongTro.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.btnThemPhongTro.Name = "btnThemPhongTro";
			this.btnThemPhongTro.Size = new System.Drawing.Size(205, 30);
			this.btnThemPhongTro.TabIndex = 9;
			this.btnThemPhongTro.Text = "Thêm phòng trọ";
			this.btnThemPhongTro.UseVisualStyleBackColor = true;
			this.btnThemPhongTro.Click += new System.EventHandler(this.btnThemPhongTro_Click);
			// 
			// tabPhong
			// 
			this.tabPhong.BackColor = System.Drawing.Color.White;
			this.tabPhong.Controls.Add(this.btnThemPhongTro);
			this.tabPhong.Controls.Add(this.labelSoPhongChuaThuPhi);
			this.tabPhong.Controls.Add(this.labelChuaThuPhi);
			this.tabPhong.Controls.Add(this.labelSoPhongChoThue);
			this.tabPhong.Controls.Add(this.labelDaChoThue);
			this.tabPhong.Controls.Add(this.labelSoPhongTrong);
			this.tabPhong.Controls.Add(this.labelTrong);
			this.tabPhong.Controls.Add(this.groupBoxTrangChu);
			this.tabPhong.Controls.Add(this.groupBoxTimKiem);
			this.tabPhong.Location = new System.Drawing.Point(4, 25);
			this.tabPhong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.tabPhong.Name = "tabPhong";
			this.tabPhong.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.tabPhong.Size = new System.Drawing.Size(1240, 787);
			this.tabPhong.TabIndex = 0;
			this.tabPhong.Text = "Phòng";
			// 
			// Main
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.BackColor = System.Drawing.Color.Azure;
			this.ClientSize = new System.Drawing.Size(1290, 827);
			this.Controls.Add(this.QLPT);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
			this.MaximizeBox = false;
			this.Name = "Main";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Quản lý phòng trọ";
			this.Load += new System.EventHandler(this.Main_Load);
			this.QLPT.ResumeLayout(false);
			this.tabkhachHang.ResumeLayout(false);
			this.tabkhachHang.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewDsKhachHang)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.tabDienNuoc.ResumeLayout(false);
			this.tabDienNuoc.PerformLayout();
			this.groupBox5.ResumeLayout(false);
			this.groupBox5.PerformLayout();
			this.groupBox4.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewSoDienNuoc)).EndInit();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.tabTinhTien.ResumeLayout(false);
			this.tabTinhTien.PerformLayout();
			this.groupBox7.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewTinhTien)).EndInit();
			this.groupBox6.ResumeLayout(false);
			this.groupBox6.PerformLayout();
			this.tabgeBaoCao.ResumeLayout(false);
			this.tabgeBaoCao.PerformLayout();
			this.panel3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvBaoCao)).EndInit();
			this.bc.ResumeLayout(false);
			this.bc.PerformLayout();
			this.tabPage1.ResumeLayout(false);
			this.groupBox9.ResumeLayout(false);
			this.groupBox9.PerformLayout();
			this.groupBox8.ResumeLayout(false);
			this.groupBox8.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvTaiKhoan)).EndInit();
			this.groupBoxTimKiem.ResumeLayout(false);
			this.groupBoxTimKiem.PerformLayout();
			this.groupBoxTrangChu.ResumeLayout(false);
			this.tabPhong.ResumeLayout(false);
			this.tabPhong.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabControl QLPT;
		private System.Windows.Forms.TabPage tabkhachHang;
		private System.Windows.Forms.TabPage tabDienNuoc;
		private System.Windows.Forms.TabPage tabTinhTien;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button btnResert;
		private System.Windows.Forms.Button btnXoa;
		private System.Windows.Forms.Button btnThem;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DataGridView dataGridViewDsKhachHang;
		private System.Windows.Forms.DateTimePicker dateTimeKetThuc;
		private System.Windows.Forms.TextBox txtGia;
		private System.Windows.Forms.ComboBox cbLoaiPhong;
		private System.Windows.Forms.ComboBox cbPhong;
		private System.Windows.Forms.CheckBox checkBoxThuTien;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox txtSdt;
		private System.Windows.Forms.TextBox txtCMNN;
		private System.Windows.Forms.RichTextBox txtDiaChi;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtTenKhachHang;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label labelCanhBao;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID_KhachHang;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID_LichSuThuePhong;
		private System.Windows.Forms.DataGridViewTextBoxColumn TenKhachHang;
		private System.Windows.Forms.DataGridViewTextBoxColumn CMND;
		private System.Windows.Forms.DataGridViewTextBoxColumn SoDienThoai;
		private System.Windows.Forms.DataGridViewTextBoxColumn DiaChi;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID_Phong;
		private System.Windows.Forms.DataGridViewTextBoxColumn TenLoaiPhong;
		private System.Windows.Forms.DataGridViewTextBoxColumn Gia;
		private System.Windows.Forms.DataGridViewTextBoxColumn ThoiGianBatDauThue;
		private System.Windows.Forms.DataGridViewTextBoxColumn ThoiGianKetThucThue;
		private System.Windows.Forms.TabPage tabgeBaoCao;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.TextBox txtPhongUpd;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.ComboBox cbTrangThai;
		private System.Windows.Forms.TextBox txtPhongTimKiem;
		private System.Windows.Forms.Label labeltt;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Button btnLuu;
		private System.Windows.Forms.TextBox txtChiSoDienMoi;
		private System.Windows.Forms.TextBox txtChiSoDienCu;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.DataGridView dataGridViewSoDienNuoc;
		private System.Windows.Forms.TextBox txtChiSoNuocMoi;
		private System.Windows.Forms.TextBox txtChiSoNuocCu;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Button btnClear;
		private System.Windows.Forms.Button btnTimKiemSoDienNuoc;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID_Phong1;
		private System.Windows.Forms.DataGridViewTextBoxColumn SoTienThanhToan1;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID_SoDien;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID_LichSuThuePhong1;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID_SoNuoc;
		private System.Windows.Forms.DataGridViewTextBoxColumn TrangThai;
		private System.Windows.Forms.DataGridViewTextBoxColumn SoDienCu;
		private System.Windows.Forms.DataGridViewTextBoxColumn SoDienMoi;
		private System.Windows.Forms.DataGridViewTextBoxColumn SoNuocCu;
		private System.Windows.Forms.DataGridViewTextBoxColumn SoNuocMoi;
		private System.Windows.Forms.DataGridViewTextBoxColumn SoDienSuDung;
		private System.Windows.Forms.DataGridViewTextBoxColumn SoNuocSuDung;
		private System.Windows.Forms.DataGridViewTextBoxColumn DonGiaDien;
		private System.Windows.Forms.DataGridViewTextBoxColumn DonGiaNuoc;
		private System.Windows.Forms.DataGridViewTextBoxColumn ThanhTien;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.DataGridView dataGridViewTinhTien;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.Button btnTImKiemTinhTien;
		private System.Windows.Forms.TextBox txtTImKiemTinhTien;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID_Phong2;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID_CongNo2;
		private System.Windows.Forms.DataGridViewTextBoxColumn TrangThai2;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID_SoDien2;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID_SoNuoc2;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID_KhachHang2;
		private System.Windows.Forms.DataGridViewTextBoxColumn TenKhachHang2;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID_LichSuThuePhong2;
		private System.Windows.Forms.DataGridViewTextBoxColumn TongSoTien;
		private System.Windows.Forms.DataGridViewTextBoxColumn TongSoTienDaThanhToan;
		private System.Windows.Forms.DataGridViewTextBoxColumn TongSoTienChuaThanhToan;
		private System.Windows.Forms.GroupBox bc;
		private System.Windows.Forms.Button btnBoaCao;
		private System.Windows.Forms.ComboBox comboBoxtt;
		private System.Windows.Forms.TextBox textBoxTimKiem;
		private System.Windows.Forms.Label labKhachHag;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.DateTimePicker dateTimeBatDau;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dgvBaoCao;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bc_ID_Phong;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bc_TrangThai;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bc_ID_KhachHang;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bc_TenKhachHang;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bc_ID_SoDien;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bc_ID_SoNuoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bc_ID_LichSuThuePhong;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bc_ThoiGianBatDauThue;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bc_ThoiGianKetThucThue;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bc_SoThangDaThue;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bc_SoDienSuDung;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bc_SoNuocDaSuDung;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bc_TongSoTien;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bc_TongSoTienDaThanhToan;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bc_TongSoTienChuaThanhToan;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bc_DateFoodter;
        private System.Windows.Forms.Button btnTimKiemKH;
        private System.Windows.Forms.TextBox txtTimKiemKH;
        private System.Windows.Forms.DateTimePicker dateBaoBao;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label labMatKhauMoi;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtNhapLaiMK;
        private System.Windows.Forms.TextBox txtMatKhau;
        private System.Windows.Forms.TextBox txtTenTK;
        private System.Windows.Forms.TextBox txtMaTK;
        private System.Windows.Forms.DataGridView dgvTaiKhoan;
        private System.Windows.Forms.ComboBox cbQuyen;
        private System.Windows.Forms.Button btnTimKiemTK;
        private System.Windows.Forms.TextBox txtTimKiemTK;
        private System.Windows.Forms.Button btnSuaTK;
        private System.Windows.Forms.Button btnXoaTK;
        private System.Windows.Forms.Button btnThemTK;
		private System.Windows.Forms.Button btnDangXuat;
		private System.Windows.Forms.Button btnLamMoi;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID_TaiKhoan;
		private System.Windows.Forms.DataGridViewTextBoxColumn DelFlg;
		private System.Windows.Forms.DataGridViewTextBoxColumn TenTaiKhoan;
		private System.Windows.Forms.DataGridViewTextBoxColumn Password;
		private System.Windows.Forms.DataGridViewTextBoxColumn Quyen_TK;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.TabPage tabPhong;
		private System.Windows.Forms.Button btnThemPhongTro;
		private System.Windows.Forms.Label labelSoPhongChuaThuPhi;
		private System.Windows.Forms.Label labelChuaThuPhi;
		private System.Windows.Forms.Label labelSoPhongChoThue;
		private System.Windows.Forms.Label labelDaChoThue;
		private System.Windows.Forms.Label labelSoPhongTrong;
		private System.Windows.Forms.Label labelTrong;
		private System.Windows.Forms.GroupBox groupBoxTrangChu;
		private System.Windows.Forms.Panel panelPhong;
		private System.Windows.Forms.GroupBox groupBoxTimKiem;
		private System.Windows.Forms.Button btnTimKiem;
		private System.Windows.Forms.TextBox txtSoPhong;
		private System.Windows.Forms.ComboBox cbTrangThaiPhi;
		private System.Windows.Forms.ComboBox cbTrangThaiPhong;
    }
}

