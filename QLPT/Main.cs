﻿using BUS;
using DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;

namespace QLPT
{
	public partial class Main : Form
	{
		#region Biến toàn cục

		string quyen = string.Empty;
		string idTk = string.Empty;
		string idKhacHang = string.Empty;
		string idLsThuePhong = string.Empty;
		string btnChiTietName = string.Empty;
		string btnXoaName = string.Empty;
		string btnSuaName = string.Empty;
		string idPhong = string.Empty;
		string idSoNuoc = string.Empty;
		string idSoDien = string.Empty;
		string idLsThuePhong_DienNuoc = string.Empty;
		string soTienThanhToan = string.Empty;
		string thanhTien = string.Empty;
		string event_Gia = string.Empty;
		string event_CbloaiPhong = string.Empty;
		bool flagInsertDN = false;

		#endregion

		public Main()
		{
			InitializeComponent();
		}

		#region custom group box

		public class CustomGroupBox : GroupBox
		{
			public CustomGroupBox()
			{
				this.DoubleBuffered = true;
				this.TitleBackColor = Color.SteelBlue;
				this.TitleForeColor = Color.White;
				this.TitleFont = new Font(this.Font.FontFamily, Font.Size + 8, FontStyle.Bold);
				this.BackColor = Color.Transparent;
				this.Radious = 25;
				this.TitleHatchStyle = HatchStyle.Percent60;
			}

			protected override void OnPaint(PaintEventArgs e)
			{
				base.OnPaint(e);
				GroupBoxRenderer.DrawParentBackground(e.Graphics, this.ClientRectangle, this);
				var rect = ClientRectangle;
				using (var path = GetRoundRectagle(this.ClientRectangle, Radious))
				{
					e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
					rect = new Rectangle(0, 0, rect.Width, TitleFont.Height + Padding.Bottom + Padding.Top);
					if (this.BackColor != Color.Transparent)
					{
						using (var brush = new SolidBrush(BackColor))
						{
							e.Graphics.FillPath(brush, path);
						}
					}

					var clip = e.Graphics.ClipBounds;
					e.Graphics.SetClip(rect);

					using (var brush = new HatchBrush(TitleHatchStyle, TitleBackColor, ControlPaint.Light(TitleBackColor)))
					{ 
						e.Graphics.FillPath(brush, path);
					}

					using (var pen = new Pen(TitleBackColor, 1))
					{
						e.Graphics.DrawPath(pen, path);
					}

					TextRenderer.DrawText(e.Graphics, Text, TitleFont, rect, TitleForeColor);
					e.Graphics.SetClip(clip);

					using (var pen = new Pen(TitleBackColor, 1))
					{
						e.Graphics.DrawPath(pen, path);
					}
				}
			}

			public Color TitleBackColor { get; set; }
			public HatchStyle TitleHatchStyle { get; set; }
			public Font TitleFont { get; set; }
			public Color TitleForeColor { get; set; }
			public int Radious { get; set; }

			private GraphicsPath GetRoundRectagle(Rectangle b, int r)
			{
				GraphicsPath path = new GraphicsPath();
				path.AddArc(b.X, b.Y, r, r, 180, 90);
				path.AddArc(b.X + b.Width - r - 1, b.Y, r, r, 270, 90);
				path.AddArc(b.X + b.Width - r - 1, b.Y + b.Height - r - 1, r, r, 0, 90);
				path.AddArc(b.X, b.Y + b.Height - r - 1, r, r, 90, 90);
				path.CloseAllFigures();
				return path;
			}

		}

		#endregion

		#region QLPT : Nhận giá trị UserName từ Flogin

		/// <summary>
		/// Nhận giá trị UserName từ Flogin
		/// </summary>
		/// <param name="giatrinhan"></param>
		public Main(string quyenLoad, string idLoad)
			: this()
		{
			this.quyen = quyenLoad;
			this.idTk = idLoad;
		}

		#endregion

		#region khởi tạo màn hình

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Main_Load(object sender, EventArgs e)
		{
			this.txtGia.Enabled = false;
			this.txtNhapLaiMK.Visible = false;
			this.labMatKhauMoi.Visible = false;
			this.txtMaTK.Enabled = false;
			if (this.quyen != "9999")
			{
				this.btnThemPhongTro.Visible = false;
				this.btnLuu.Visible = false;
				this.button1.Visible = false;
				this.button2.Visible = false;
				this.btnThem.Visible = false;
				this.btnXoa.Visible = false;
			}

			#region Tab phong

			// khởi tạo group box
			this.InitGroupBoxTrangChu();

			// khởi tạo combobox trạng thái phòng
			this.InitComboboxTrangThaiPhong();

			// khởi tạo combobox trạng thái phí
			this.InitComboboxTrangThaiPhi();

			this.InitLabelCnt();

			#endregion

			#region Tab khách hàng

			// Khởi tạo danh sách khách hàng
			this.InitDataGridViewDsKhachHang();

			this.InitComboboxPhong();

			this.InitComboboxLoaiPhong();

			#endregion

			#region Tab điện nước

			this.txtPhongUpd.Enabled = false;

			// Khởi tạo danh sách khách hàng
			this.InitDataGridViewDsDienNuoc();

			this.InitComboboxTrangThaiPhongTimKiem();

			#endregion

			#region tab tính tiền

			// Khởi tạo danh sách khách hàng
			this.InitDataGridViewTinhTien();

			this.InitComboboxTrangThaiTinhTien();

			#endregion

			#region tab tài khoản

			this.btnXoaTK.Enabled = false;
			this.btnSuaTK.Enabled = false;
			if (this.quyen == "9999")
			{
				this.cbQuyen.Enabled = true;
				this.btnThemTK.Enabled = true;
			}
			else
			{
				this.cbQuyen.Enabled = false;
				this.btnThemTK.Enabled = false;
			}

			this.LoadCbTK();

			this.loadTaiKhoan();

			#endregion
		}

        #endregion

        #region tab phòng

        #region khởi tạo GroupBoxTrangChu

        private void InitGroupBoxTrangChu()
		{
			// khai báo biến
			int y = 15;
			int x = 15;
			int rowCnt = 0;
			string searchStr = string.Empty;
			string trangThaiPhong = string.Empty;
			string trangThaiPhi = string.Empty;
			this.panelPhong.Controls.Clear();

			BUS_DataGet dataGet = new BUS_DataGet();
			if (this.cbTrangThaiPhong.SelectedValue != null)
			{
				trangThaiPhong = this.cbTrangThaiPhong.SelectedValue.ToString();
			}

			if (this.cbTrangThaiPhi.SelectedValue != null)
			{
				trangThaiPhi = this.cbTrangThaiPhi.SelectedValue.ToString();
			}

			if (!string.IsNullOrEmpty(this.txtSoPhong.Text.ToString()))
			{
				searchStr = this.txtSoPhong.Text;
			}

			DataTable target = dataGet.GetDataTagetUseTrangChu(searchStr, trangThaiPhong, trangThaiPhi);

			if (target!=null && target.Rows.Count > 0)
			{
				// lặp qua sanh sách phòng
				foreach (DataRow row in target.Rows)
				{
					int ii = 0;
					// Group box phòng
					CustomGroupBox gb = new CustomGroupBox();
					gb.Name = "groupBoxPhong" + row["ID_Phong"].ToString();
					gb.Text = "Phòng " + row["ID_Phong"].ToString();
					gb.Size = new Size(230, 140);

					if (row["TrangThai"].ToString() == "1")
					{
						gb.TitleBackColor = Color.Tan;
					}
					// chiều ngang
					x = 15 + (15 * rowCnt) + (230 * rowCnt);

					// chiều dọc
					// xuống dòng khi đủ 3 phòng trên 1 row
					if (x > 750)
					{
						x = 15;
						y = y + 175;
						rowCnt = 0;
					}

					// set vị trí cho group box
					gb.Location = new Point(x, y);

					// label tên khách hàng
					// Value tên khách hàng
					Label LabName = new Label();
					Label valueName = new Label();
					if (string.IsNullOrEmpty(row["ID_KhachHang"].ToString()))
					{
						LabName.Name = "LabelTenKhachHang" + ii;
						valueName.Name = "TenKhachHang" + ii;
						valueName.Text = "Chưa thuê";
					}
					else
					{
						LabName.Name = "LabelTenKhachHang" + row["ID_KhachHang"].ToString();
						valueName.Name = "TenKhachHang" + row["ID_KhachHang"].ToString();
						valueName.Text = row["TenKhachHang"].ToString();
					}

					LabName.Text = "Khách hàng:";
					LabName.Location = new Point(15, 35);
					gb.Controls.Add(LabName);

					valueName.Location = new Point(113, 35);
					gb.Controls.Add(valueName);

					// label giá
					Label LabGia = new Label();
					LabGia.Name = "LabelGia" + row["ID_Phong"].ToString();
					LabGia.Text = "Giá:";
					LabGia.Location = new Point(15, 58);
					gb.Controls.Add(LabGia);

					// Value giá
					Label valueGia = new Label();
					valueGia.Name = "txtGia" + row["ID_Phong"].ToString();
					valueGia.Text = convertIntToMoney(row["Gia"].ToString());
					valueGia.Location = new Point(113, 58);
					valueGia.BorderStyle = BorderStyle.None;
					gb.Controls.Add(valueGia);

					// label loại phòng
					Label labLoaiPhong = new Label();
					labLoaiPhong.Name = "LabLoaiPhong" + row["ID_Phong"].ToString();
					labLoaiPhong.Text = "Loại phòng:";
					labLoaiPhong.Location = new Point(15, 81);
					labLoaiPhong.Size = new Size(80, 20);
					gb.Controls.Add(labLoaiPhong);

					// Value giá
					Label valueLoaiPhong = new Label();
					valueLoaiPhong.Name = "cbLoaiPhong" + row["ID_Phong"].ToString();
					valueLoaiPhong.Text = row["TenLoaiPhong"].ToString();
					valueLoaiPhong.Location = new Point(113, 81);
					valueLoaiPhong.Size = new Size(60, 20);
					gb.Controls.Add(valueLoaiPhong);

					if (this.quyen == "9999")
					{
						// button ChiTiet
						Button btnChiTiet = new Button();
						btnChiTiet.Name = "btnChiTiet" + row["ID_Phong"].ToString();
						btnChiTiet.Text = "Xem";
						btnChiTiet.Size = new Size(56, 25);
						btnChiTiet.Location = new Point(35, 105);
						btnChiTiet.Click += btnChiTiet_Click;
						gb.Controls.Add(btnChiTiet);

						// button xóa
						Button btnXoa = new Button();
						btnXoa.Name = "btnXoa" + row["ID_Phong"].ToString();
						btnXoa.Text = "Xóa";
						btnXoa.Size = new Size(56, 25);
						btnXoa.Location = new Point(141, 105);
						btnXoa.Click += ButtonXoa_Click;
						gb.Controls.Add(btnXoa);
					}

					// thêm group box vào danh sách phòng
					panelPhong.Controls.Add(gb);
					rowCnt++;
					ii++;
				}
			}
		}

		#endregion

		#region khởi tạo combo box trạng thái phòng

		private void InitComboboxTrangThaiPhong()
		{
			var trangThaiPhong = new BindingList<KeyValuePair<string, string>>();
			trangThaiPhong.Add(new KeyValuePair<string, string>("", " --- Trạng thái phòng --- "));
			trangThaiPhong.Add(new KeyValuePair<string, string>("0", " --- Chưa thuê --- "));
			trangThaiPhong.Add(new KeyValuePair<string, string>("1", " ---  Đã thuê  --- "));

			this.cbTrangThaiPhong.DataSource = trangThaiPhong;
			this.cbTrangThaiPhong.ValueMember = "Key";
			this.cbTrangThaiPhong.DisplayMember = "Value";
			this.cbTrangThaiPhong.SelectedIndex = 0;
		}

		#endregion

		#region khởi tạo combo box trạng thái phi

		private void InitComboboxTrangThaiPhi()
		{
			var trangThaiPhi = new BindingList<KeyValuePair<string, string>>();
			trangThaiPhi.Add(new KeyValuePair<string, string>("", " --- Trạng thái phí --- "));
			trangThaiPhi.Add(new KeyValuePair<string, string>("1", " --- Chưa thu phí  --- "));
			trangThaiPhi.Add(new KeyValuePair<string, string>("0", " --- Đã thu phí --- "));

			this.cbTrangThaiPhi.DataSource = trangThaiPhi;
			this.cbTrangThaiPhi.ValueMember = "Key";
			this.cbTrangThaiPhi.DisplayMember = "Value";
			this.cbTrangThaiPhi.SelectedIndex = 0;
		}

		#endregion

		#region khởi tạo label số phòng trống, chưa thu phí, ...

		private void InitLabelCnt()
		{
			BUS_Phong phong = new BUS_Phong();
			DataTable phongList = phong.GetPhong();

			BUS_CongNo congNo = new BUS_CongNo();
			DataTable congNoList = congNo.GetCongNo();

			if (phongList!= null && phongList.Rows.Count > 0)
			{
				int soPhongDaThue = phongList.AsEnumerable().Where(x => x["TrangThai"].ToString() == "1" && x["DelFlag"].ToString() == "0").ToList().Count;
				this.labelSoPhongChoThue.Text = soPhongDaThue.ToString();

				int soPhongTrong = phongList.AsEnumerable().Where(x => x["TrangThai"].ToString() == "0" && x["DelFlag"].ToString() == "0").ToList().Count;
				this.labelSoPhongTrong.Text = soPhongTrong.ToString();
			}

			if (congNoList!= null && congNoList.Rows.Count >0)
			{
				int chuaThuPhi = congNoList.AsEnumerable().Where(x => x["TrangThaiNo"].ToString() == "1" && x["DelFlg"].ToString() == "0").ToList().Count;
				this.labelSoPhongChuaThuPhi.Text = chuaThuPhi.ToString();
			}
		}

		#endregion

		#region Sự kiện tìm kiếm

		private void btnTimKiem_Click(object sender, EventArgs e)
		{
			string searchStr = this.txtSoPhong.Text;
			string trangThaiPhong = string.Empty;
			string trangThaiPhi = string.Empty;

			if (this.cbTrangThaiPhong.SelectedValue != null)
			{
				trangThaiPhong = this.cbTrangThaiPhong.SelectedValue.ToString();
			}

			if (this.cbTrangThaiPhi.SelectedValue != null)
			{
				trangThaiPhi = this.cbTrangThaiPhi.SelectedValue.ToString();
			}

			this.InitGroupBoxTrangChu();
		}

		#endregion

		#region sự kiện click button chi tiết

		private void btnChiTiet_Click(object sender, EventArgs e)
		{
			Button button = sender as Button;
			string id = button.Name.Substring(button.Name.Length - 3, 3); ;

			ChiTiet f = new ChiTiet(id);
			this.Hide();
			f.ShowDialog();
			this.Show();
			this.InitGroupBoxTrangChu();
		}

		private void ButtonXoa_Click(object sender, EventArgs e)
		{
			Button button = sender as Button;
			string id = button.Name.Substring(button.Name.Length - 3, 3);

			BUS_LichSuThuePhong bUS_LichSuThuePhong = new BUS_LichSuThuePhong();
			DataTable dt = bUS_LichSuThuePhong.GetLichSuThuePhongByIdPhong(id);

			if (dt!= null && dt.Rows.Count> 0)
			{
				MessageBox.Show("Phòng đang được thuê, không thể xóa");
				return;
			}

			BUS_Phong bUS_Phong = new BUS_Phong();
			bUS_Phong.Delete(id);

			this.InitGroupBoxTrangChu();
		}

		#endregion

		#endregion

		#region tab khách hàng

		#region khởi tạo data grid view

		private void InitDataGridViewDsKhachHang()
		{
			BUS_DataGet dataGet = new BUS_DataGet();
			DataTable thongTinKhachHang = dataGet.GetDataKhachHangInf(txtTimKiemKH.Text.ToString());

			if (dataGridViewDsKhachHang.Rows.Count> 0)
			{
				dataGridViewDsKhachHang.Rows.Clear();
			}
			
			for (int i = 0; i < thongTinKhachHang.Rows.Count; i++)
			{
				dataGridViewDsKhachHang.RowCount = thongTinKhachHang.Rows.Count;
				dataGridViewDsKhachHang.Rows[i].Cells["ID_KhachHang"].Value = thongTinKhachHang.Rows[i]["ID_KhachHang"].ToString();
				dataGridViewDsKhachHang.Rows[i].Cells["TenKhachHang"].Value = thongTinKhachHang.Rows[i]["TenKhachHang"].ToString();
				dataGridViewDsKhachHang.Rows[i].Cells["CMND"].Value = thongTinKhachHang.Rows[i]["CCCD"].ToString();
				dataGridViewDsKhachHang.Rows[i].Cells["SoDienThoai"].Value = thongTinKhachHang.Rows[i]["SoDienThoai"].ToString();
				dataGridViewDsKhachHang.Rows[i].Cells["DiaChi"].Value = thongTinKhachHang.Rows[i]["DiaChi"].ToString();
				dataGridViewDsKhachHang.Rows[i].Cells["ID_Phong"].Value = thongTinKhachHang.Rows[i]["ID_Phong"].ToString();
				if (!string.IsNullOrEmpty(thongTinKhachHang.Rows[i]["Gia"].ToString()))
				{
					dataGridViewDsKhachHang.Rows[i].Cells["Gia"].Value = convertIntToMoney( thongTinKhachHang.Rows[i]["Gia"].ToString());
					
				}
				else
				{
					dataGridViewDsKhachHang.Rows[i].Cells["Gia"].Value = "0 đ";
				}
				dataGridViewDsKhachHang.Rows[i].Cells["TenLoaiPhong"].Value = thongTinKhachHang.Rows[i]["TenLoaiPhong"].ToString();
				dataGridViewDsKhachHang.Rows[i].Cells["ID_LichSuThuePhong"].Value = thongTinKhachHang.Rows[i]["ID_LichSuThuePhong"].ToString();
				dataGridViewDsKhachHang.Rows[i].Cells["ThoiGianBatDauThue"].Value = thongTinKhachHang.Rows[i]["ThoiGianBatDauThue"].ToString();
				dataGridViewDsKhachHang.Rows[i].Cells["ThoiGianKetThucThue"].Value = thongTinKhachHang.Rows[i]["ThoiGianKetThucThue"].ToString();
			}
		}

		#endregion

		#region khởi tạo combo box phòng

		private void InitComboboxPhong()
		{
			BUS_Phong bUS_hong = new BUS_Phong();
			DataTable dsPhong = bUS_hong.GetPhong();

			DataTable dt = new DataTable();
			dt.Columns.Add("Key", typeof(string));
			dt.Columns.Add("Value", typeof(string));

			DataRow row = dt.NewRow();
			row["Key"] = "000";
			row["Value"] = "--- Chọn phòng ---";
			dt.Rows.Add(row);

			foreach (DataRow dsRow in dsPhong.Rows)
			{
				//if (dsRow["TrangThai"].ToString() == "0")
				//{
				row = dt.NewRow();
				row["Key"] = dsRow["ID_Phong"].ToString();
				row["Value"] = dsRow["ID_Phong"].ToString();
				dt.Rows.Add(row);
				//}
			}

			this.cbPhong.ValueMember = "Key";
			this.cbPhong.DisplayMember = "Value";
			this.cbPhong.DataSource = dt;
			this.cbPhong.SelectedIndex = 0;
		}

		#endregion

		#region khởi tạo combo box loại phòng

		private void InitComboboxLoaiPhong()
		{
			BUS_LoaiPhong bUS_LoaiPhong = new BUS_LoaiPhong();
			DataTable dsPhong = bUS_LoaiPhong.GetLoaiPhong();

			DataTable dt = new DataTable();
			dt.Columns.Add("Key", typeof(string));
			dt.Columns.Add("Value", typeof(string));

			DataRow row = dt.NewRow();
			row["Key"] = "000";
			row["Value"] = "--- Chọn loại phòng ---";
			dt.Rows.Add(row);

			foreach (DataRow dsRow in dsPhong.Rows)
			{
				row = dt.NewRow();
				row["Key"] = dsRow["ID_LoaiPhong"].ToString();
				row["Value"] = dsRow["TenLoaiPhong"].ToString();
				dt.Rows.Add(row);
			}

			this.cbLoaiPhong.ValueMember = "Key";
			this.cbLoaiPhong.DisplayMember = "Value";
			this.cbLoaiPhong.DataSource = dt;
			this.cbLoaiPhong.SelectedIndex = 0;
			this.cbLoaiPhong.Enabled = false;
		}

		#endregion

		private void dataGridViewDsKhachHang_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			dataGridViewDsKhachHang.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			if (dataGridViewDsKhachHang.SelectedCells.Count > 0 && e.RowIndex >= 0)
			{
				DataGridViewRow selectedRow = dataGridViewDsKhachHang.Rows[e.RowIndex];
				this.idKhacHang = selectedRow.Cells["ID_KhachHang"].Value.ToString();
				this.idLsThuePhong = selectedRow.Cells["ID_LichSuThuePhong"].Value.ToString();
				this.txtTenKhachHang.Text = selectedRow.Cells["TenKhachHang"].Value.ToString();
				this.txtCMNN.Text = selectedRow.Cells["CMND"].Value.ToString();
				this.txtSdt.Text = selectedRow.Cells["SoDienThoai"].Value.ToString();
				this.txtDiaChi.Text = selectedRow.Cells["DiaChi"].Value.ToString();
				this.txtGia.Text = selectedRow.Cells["Gia"].Value.ToString();

				if (DateTime.Parse( selectedRow.Cells["ThoiGianBatDauThue"].Value.ToString() ).CompareTo(new DateTime(1900,01,1)) != 0)
				{
					this.dateTimeBatDau.Value = DateTime.Parse(selectedRow.Cells["ThoiGianBatDauThue"].Value.ToString());
				}

				if (DateTime.Parse(selectedRow.Cells["ThoiGianKetThucThue"].Value.ToString()).CompareTo(new DateTime(1900, 01, 1)) != 0)
				{
					this.dateTimeKetThuc.Value = DateTime.Parse(selectedRow.Cells["ThoiGianKetThucThue"].Value.ToString());
				}

				this.cbPhong.SelectedValue = selectedRow.Cells["ID_Phong"].Value.ToString();
				if (this.cbPhong.SelectedValue!= null)
				{
					this.SelectLoaiPhong(this.cbPhong.SelectedValue.ToString());
				}
				else
				{
					this.labelCanhBao.Visible = false;
					this.SelectLoaiPhong("000");
				}

				BUS_CongNo bUS_CongNo = new BUS_CongNo();
				DataTable congNo = bUS_CongNo.GetCongNoByID(this.idLsThuePhong, this.idKhacHang, selectedRow.Cells["ID_Phong"].Value.ToString());

				if (congNo != null && congNo.Rows.Count > 0)
				{
					if (congNo.Rows[0]["TrangThaiNo"].ToString() == "1")
					{
						this.checkBoxThuTien.Checked = false;
					}
					else
					{
						this.checkBoxThuTien.Checked = true;
					}
				}
				else if ( string.IsNullOrEmpty (this.idLsThuePhong) )
				{
					this.checkBoxThuTien.Checked = false;
				}
				else
				{
					this.checkBoxThuTien.Checked = true;
				}
			}
		}

		private void cbPhong_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.cbPhong.SelectedValue!= null)
			{
				string idPhong = this.cbPhong.SelectedValue.ToString();
				this.SelectLoaiPhong(idPhong);

				BUS_Phong bUS_Phong = new BUS_Phong();
				DataTable dtp = bUS_Phong.GetPhongByID(this.cbPhong.SelectedValue.ToString());

				if (dtp != null && dtp.Rows.Count > 0)
				{
					this.txtGia.Text = convertIntToMoney(dtp.Rows[0]["Gia"].ToString());

					if (dtp.Rows[0]["TrangThai"].ToString() == "1")
					{
						this.labelCanhBao.Visible = true;
					}
					else
					{
						this.labelCanhBao.Visible = false;
					}
				}
			}
		}

		private void SelectLoaiPhong(string idPhong)
		{
			if (idPhong != "000")
			{
				BUS_Phong bUS_Phong = new BUS_Phong();
				DataTable dt = bUS_Phong.GetPhongByID(idPhong);

				if (dt.Rows.Count > 0)
				{
					this.cbLoaiPhong.SelectedValue = dt.Rows[0]["ID_LoaiPhong"].ToString();
				}
			}
			else
			{
				this.cbLoaiPhong.SelectedValue = "000";
			}
		}

		private void btnResert_Click(object sender, EventArgs e)
		{
			this.clear();
		}

		private void btnThem_Click(object sender, EventArgs e)
		{
			BUS_KhachHang bUS_KhachHang = new BUS_KhachHang();
			BUS_LichSuThuePhong bUS_LichSuThuePhong = new BUS_LichSuThuePhong();
			DTO_KhachHang dTO_KhachHang = new DTO_KhachHang();
			DTO_LichSuThuePhong dTO_LichSuThuePhong = new DTO_LichSuThuePhong();
			BUS_Phong bUS_Phong = new BUS_Phong();
			DTO_CongNo dTO_CongNo = new DTO_CongNo();
			BUS_SoDien bUS_SoDien = new BUS_SoDien();
			BUS_SoNuoc bUS_SoNuoc = new BUS_SoNuoc();
			BUS_LichSuThanhToan bUS_LichSuThanhToan = new BUS_LichSuThanhToan();
			DTO_LichSuThanhToan dTO_LichSuThanhToan = new DTO_LichSuThanhToan();
			try
			{
				// check valid trước khi update/insert
				if (!this.checkValid())
				{
					return;
				}

				string idsodien = "0";
				if (!string.IsNullOrEmpty(bUS_SoDien.GetMaxIdSoDien()))
				{
					idsodien = bUS_SoDien.GetMaxIdSoDien();
				}
				this.idSoDien = (int.Parse(idsodien) + 1).ToString().PadLeft(5, '0');
				DTO_SoDien dTO_SoDien = new DTO_SoDien();
				dTO_SoDien.ID_Phong = cbPhong.SelectedValue.ToString();
				dTO_SoDien.ID_SoDien = this.idSoDien;
				dTO_SoDien.SoCu = "0";
				dTO_SoDien.SoMoi = "0";
				dTO_SoDien.NgayBatDauSuDungDien = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
				dTO_SoDien.NgayKetThucSuDungDien = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
				dTO_SoDien.ID_LoaiDonGia = "00";
				dTO_SoDien.FlagInsert = true;

				string idsonuoc = "0";
				if (!string.IsNullOrEmpty(bUS_SoNuoc.GetMaxIdSoNuoc()))
				{
					idsonuoc = bUS_SoNuoc.GetMaxIdSoNuoc();
				}
				this.idSoNuoc = (int.Parse(idsonuoc) + 1).ToString().PadLeft(5, '0');

				DTO_SoNuoc dTO_SoNuoc = new DTO_SoNuoc();
				dTO_SoNuoc.ID_Phong = cbPhong.SelectedValue.ToString();
				dTO_SoNuoc.ID_SoNuoc = this.idSoNuoc;
				dTO_SoNuoc.SoCu = "0";
				dTO_SoNuoc.SoMoi = "0";
				dTO_SoNuoc.NgayBatDauSuDungNuoc = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
				dTO_SoNuoc.NgayKetThucSuDungNuoc = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
				dTO_SoNuoc.ID_LoaiDonGia = "01";
				dTO_SoNuoc.FlagInsert = true;

				// update thông tin khách hàng
				if (!string.IsNullOrEmpty( this.idKhacHang ))
				{
					dTO_KhachHang.FlagInsert = false;
					dTO_KhachHang.ID_KhachHang = this.idKhacHang;
				}
				// insert thông tin khách hàng
				else
				{
					dTO_KhachHang.FlagInsert = true;
					string id = "0";
					if (!string.IsNullOrEmpty( bUS_KhachHang.GetMaxIdKhachHang()))
					{
						id = bUS_KhachHang.GetMaxIdKhachHang();
					}
					dTO_KhachHang.ID_KhachHang = (int.Parse(id) + 1).ToString().PadLeft(5, '0'); 
				}

				dTO_KhachHang.TenKhachHang = this.txtTenKhachHang.Text;
				dTO_KhachHang.CMND = this.txtCMNN.Text;
				dTO_KhachHang.SoDienThoai = this.txtSdt.Text;
				dTO_KhachHang.DiaChi = this.txtDiaChi.Text;
				dTO_KhachHang.DelFlg = 0;

				// update thông tin lịch sử thuê phòng
				if (!string.IsNullOrEmpty(this.idLsThuePhong))
				{
					dTO_LichSuThuePhong.FlagInsert = false;
					dTO_LichSuThuePhong.ID_LichSuThuePhong = this.idLsThuePhong;
					this.flagInsertDN = false;
				}
				// insert thông tin lịch sử thuê phòng
				else
				{
					dTO_LichSuThuePhong.FlagInsert = true;
					this.flagInsertDN = true;
					string id = "0";
					if (!string.IsNullOrEmpty(bUS_LichSuThuePhong.GetMaxIdLsThuePhong()))
					{
						id = bUS_LichSuThuePhong.GetMaxIdLsThuePhong();
					}
					dTO_LichSuThuePhong.ID_LichSuThuePhong = (int.Parse(id) + 1).ToString().PadLeft(5, '0');
				}

				dTO_LichSuThuePhong.ID_KhachHang = dTO_KhachHang.ID_KhachHang;
				dTO_LichSuThuePhong.ID_Phong = this.cbPhong.SelectedValue.ToString();
				dTO_LichSuThuePhong.SoTienThanhToan = this.txtGia.Text.Replace("đ", "").Replace(".", "");

				if (this.checkBoxThuTien.Checked == true)
				{
					dTO_LichSuThuePhong.NgayThanhToan = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
				}
				else
				{
					dTO_LichSuThuePhong.NgayThanhToan = new DateTime(1900,1,1,0,0,0).ToString("MM/dd/yyyy HH:mm:ss");
				}

				dTO_LichSuThuePhong.ThoiGianBatDauThue = this.dateTimeBatDau.Value.ToString("MM/dd/yyyy HH:mm:ss");
				dTO_LichSuThuePhong.ThoiGianKetThucThue = this.dateTimeKetThuc.Value.ToString("MM/dd/yyyy HH:mm:ss");
				dTO_LichSuThuePhong.DelFlg = 0;

				BUS_CongNo bUS_CongNo = new BUS_CongNo();
				DataTable congNo = bUS_CongNo.GetCongNoByID(dTO_LichSuThuePhong.ID_LichSuThuePhong
															, dTO_KhachHang.ID_KhachHang
															, dTO_LichSuThuePhong.ID_Phong);
				
				if (this.checkBoxThuTien.Checked == true)
				{
					if (congNo != null && congNo.Rows.Count > 0)
					{
						bUS_CongNo.UpdateTrangThaiNo(congNo.Rows[0]["ID_CongNo"].ToString(), "0");
					}
					else
					{
						
						string id = "0";
						if ( !string.IsNullOrEmpty( bUS_LichSuThanhToan.GetMaxIdLsThanhToan()))
						{
							id = bUS_LichSuThanhToan.GetMaxIdLsThanhToan();
						}
						
						dTO_LichSuThanhToan.ID_ThanhToan = (int.Parse(id) + 1).ToString().PadLeft(5, '0');
						dTO_LichSuThanhToan.ID_LichSuThuePhong = dTO_LichSuThuePhong.ID_LichSuThuePhong;
						dTO_LichSuThanhToan.ID_SoDien = this.idSoDien;
						dTO_LichSuThanhToan.ID_SoNuoc = this.idSoNuoc;
						dTO_LichSuThanhToan.KyThanhToan = string.Empty;
						dTO_LichSuThanhToan.SoTienThanhToan = dTO_LichSuThuePhong.SoTienThanhToan;
						dTO_LichSuThanhToan.FlagInsert = true;
					}
				}
				else
				{
					if (congNo != null && congNo.Rows.Count > 0)
					{
						bUS_CongNo.UpdateTrangThaiNo(congNo.Rows[0]["ID_CongNo"].ToString(), "1");
					}
					else
					{
						
						string id = "0";
						if (!string.IsNullOrEmpty(bUS_CongNo.GetMaxIdCongNo()))
						{
							id = bUS_CongNo.GetMaxIdCongNo();
						}
						dTO_CongNo.ID_CongNo = (int.Parse(id) + 1).ToString().PadLeft(5, '0');
						dTO_CongNo.ID_KhachHang = dTO_KhachHang.ID_KhachHang;
						dTO_CongNo.ID_Phong = dTO_LichSuThuePhong.ID_Phong;
						dTO_CongNo.ThoiGianBatDau = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
						dTO_CongNo.ThoiGianKetThuc = this.dateTimeKetThuc.Value.ToString("MM/dd/yyyy HH:mm:ss");
						dTO_CongNo.TrangThaiNo = "1";
						dTO_CongNo.DelFlg = 0;
						dTO_CongNo.ID_LichSuThuePhong = dTO_LichSuThuePhong.ID_LichSuThuePhong;
						dTO_CongNo.SoTienNo = dTO_LichSuThuePhong.SoTienThanhToan.Replace("đ","").Replace(".","");
						dTO_CongNo.ID_SoDien = dTO_SoDien.ID_SoDien;
						dTO_CongNo.ID_SoNuoc = dTO_SoNuoc.ID_SoNuoc;
						dTO_CongNo.FlagInsert = true;
					}
				}
				//trình tự insert
				//1 insert khách hàng
				bUS_KhachHang.Update(dTO_KhachHang);

                // 2. insert sổ điện, sổ nước
                if (this.flagInsertDN)
                {
					bUS_SoDien.Insert(dTO_SoDien);
					bUS_SoNuoc.Insert(dTO_SoNuoc);
				}

				//3. insert lịch sử thuê phòng
				bUS_LichSuThuePhong.Update(dTO_LichSuThuePhong);

                //4. insert lịch sử thanh toán
                if (checkBoxThuTien.Checked==true)
                {
					bUS_LichSuThanhToan.Update(dTO_LichSuThanhToan);

				}
				//5.insert công nợ
				bUS_CongNo.Update(dTO_CongNo);

				//6. update trạng thái thuê phòng
				bUS_Phong.UpdateTrangThai(this.cbPhong.SelectedValue.ToString(), "1");
			}
			catch (Exception ex)
			{
				MessageBox.Show("Đăng ký/Update thất bại");
				return;
			}
			MessageBox.Show("Update thành công");
			// Khởi tạo danh sách khách hàng
			this.InitDataGridViewDsKhachHang();
			this.clear();
		}

		private bool checkValid()
		{
			if ( string.IsNullOrEmpty(this.txtTenKhachHang.Text)
			  || string.IsNullOrEmpty(this.txtCMNN.Text)
			  || string.IsNullOrEmpty(this.txtSdt.Text)
			  || string.IsNullOrEmpty(this.txtDiaChi.Text)
			  || string.IsNullOrEmpty(this.txtGia.Text)
			  || string.IsNullOrEmpty(this.txtGia.Text)
			  || string.IsNullOrEmpty(this.txtGia.Text)
			  || this.cbPhong.SelectedValue == "000"
			  || this.dateTimeBatDau.Value.CompareTo( new DateTime(1900,1,1,0,0,0) ) == 0
			  || this.dateTimeKetThuc.Value.CompareTo(new DateTime(1900, 1, 1, 0, 0, 0)) == 0 )
			{
				MessageBox.Show("Vui lòng nhập đầy đủ thông tin");
				return false;
			}

			return true;
		}

		private void clear()
		{
			this.txtTenKhachHang.Text = string.Empty;
			this.txtCMNN.Text = string.Empty;
			this.txtSdt.Text = string.Empty;
			this.txtDiaChi.Text = string.Empty;
			this.txtGia.Text = string.Empty;
			this.cbPhong.SelectedValue = "000";
			this.cbLoaiPhong.SelectedValue = "000";
			this.dateTimeBatDau.Value = DateTime.Now;
			this.dateTimeKetThuc.Value = DateTime.Now;
			this.idKhacHang = string.Empty;
			this.idLsThuePhong = string.Empty;
			this.checkBoxThuTien.Checked = false;
			this.labelCanhBao.Visible = false;
		}

		private void QLPT_Selecting(object sender, TabControlCancelEventArgs e)
		{
			this.InitDataGridViewDsKhachHang();

			// khởi tạo group box
			this.InitGroupBoxTrangChu();

			this.InitDataGridViewDsDienNuoc();

			this.InitDataGridViewTinhTien();

			this.InitComboboxPhong();

			this.dataGridViewDsKhachHang.Refresh();

			this.LoadCbTK();

			this.loadTaiKhoan();
		}

		private void btnXoa_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(this.idLsThuePhong) && string.IsNullOrEmpty(this.idKhacHang))
			{
				MessageBox.Show("Bạn chưa chọn khách hàng");
				return;
			}

			if (!string.IsNullOrEmpty(this.idKhacHang))
			{
				BUS_LichSuThuePhong bUS_LichSuThuePhong = new BUS_LichSuThuePhong();
				DataTable dt = bUS_LichSuThuePhong.GetLichSuThuePhongByID(this.idLsThuePhong);

				if (dt!=null && dt.Rows.Count>0)
				{
					DateTime date = DateTime.Parse(dt.Rows[0]["ThoiGianKetThucThue"].ToString());

					if (date.CompareTo(DateTime.Now) > 0)
					{
						MessageBox.Show("Khách hàng đang trong thời gian thuê phòng");
						return;
					}
					else
					{
						BUS_KhachHang bUS_KhachHang = new BUS_KhachHang();
						bUS_KhachHang.Delete(this.idKhacHang);

						BUS_Phong bUS_Phong = new BUS_Phong();
						bUS_Phong.UpdateTrangThai(dt.Rows[0]["ID_Phong"].ToString(), "0");

						bUS_LichSuThuePhong.Delete(dt.Rows[0]["ID_LichSuThuePhong"].ToString());

						BUS_SoDien bUS_SoDien = new BUS_SoDien();
						bUS_SoDien.DeleteByIdPhong(dt.Rows[0]["ID_Phong"].ToString());

						BUS_SoNuoc bUS_SoNuoc = new BUS_SoNuoc();
						bUS_SoNuoc.DeleteByIdPhong(dt.Rows[0]["ID_Phong"].ToString());

						MessageBox.Show("Xóa khách hàng thành công");

						this.InitDataGridViewDsKhachHang();
					}
				}
                else
                {
					BUS_KhachHang bUS_KhachHang = new BUS_KhachHang();
					bUS_KhachHang.Delete(this.idKhacHang);
				}
			}
		}

		#endregion

		#region tab điện nước

		#region Chỉ sổ điện nước

		private void InitDataGridViewDsDienNuoc()
		{
			string tt = string.Empty;
			string phongtimkiem = this.txtPhongTimKiem.Text.ToString();
			if (this.cbTrangThai.SelectedValue!= null)
			{
				tt = this.cbTrangThai.SelectedValue.ToString();
			}

			BUS_DataGet bUS_DataGet = new BUS_DataGet();
			DataTable dt = bUS_DataGet.GetDataDienNuocInf(phongtimkiem, tt);

			for (int i = 0; i < dt.Rows.Count; i++)
			{
				dataGridViewSoDienNuoc.RowCount = dt.Rows.Count;
				dataGridViewSoDienNuoc.Rows[i].Cells["ID_Phong1"].Value = dt.Rows[i]["ID_Phong"].ToString();
				dataGridViewSoDienNuoc.Rows[i].Cells["TrangThai"].Value = dt.Rows[i]["TrangThai"].ToString();
				dataGridViewSoDienNuoc.Rows[i].Cells["ID_LichSuThuePhong1"].Value = dt.Rows[i]["ID_LichSuThuePhong"].ToString();
				dataGridViewSoDienNuoc.Rows[i].Cells["SoTienThanhToan1"].Value = dt.Rows[i]["SoTienThanhToan"].ToString();
				dataGridViewSoDienNuoc.Rows[i].Cells["ID_SoDien"].Value = dt.Rows[i]["ID_SoDien"].ToString();
				dataGridViewSoDienNuoc.Rows[i].Cells["ID_SoNuoc"].Value = dt.Rows[i]["ID_SoNuoc"].ToString();
				dataGridViewSoDienNuoc.Rows[i].Cells["SoDienCu"].Value = dt.Rows[i]["SoDienCu"].ToString();
				dataGridViewSoDienNuoc.Rows[i].Cells["SoDienMoi"].Value = dt.Rows[i]["SoDienMoi"].ToString();
				dataGridViewSoDienNuoc.Rows[i].Cells["SoNuocCu"].Value = dt.Rows[i]["SoNuocCu"].ToString();
				dataGridViewSoDienNuoc.Rows[i].Cells["SoNuocMoi"].Value = dt.Rows[i]["SoNuocMoi"].ToString();
				dataGridViewSoDienNuoc.Rows[i].Cells["SoDienSuDung"].Value = dt.Rows[i]["SoDienSuDung"].ToString();
				dataGridViewSoDienNuoc.Rows[i].Cells["SoNuocSuDung"].Value = dt.Rows[i]["SoNuocSuDung"].ToString();
				if (!string.IsNullOrEmpty(dt.Rows[i]["DonGiaDien"].ToString()))
				{
					dataGridViewSoDienNuoc.Rows[i].Cells["DonGiaDien"].Value = convertIntToMoney(dt.Rows[i]["DonGiaDien"].ToString());
				}
				else
				{
					dataGridViewSoDienNuoc.Rows[i].Cells["DonGiaDien"].Value = "0 đ";
				}

				if (!string.IsNullOrEmpty(dt.Rows[i]["DonGiaNuoc"].ToString()))
				{
					dataGridViewSoDienNuoc.Rows[i].Cells["DonGiaNuoc"].Value = convertIntToMoney(dt.Rows[i]["DonGiaNuoc"].ToString());
				}
				else
				{
					dataGridViewSoDienNuoc.Rows[i].Cells["DonGiaNuoc"].Value = "0 đ";
				}

				if (!string.IsNullOrEmpty(dt.Rows[i]["ThanhTien"].ToString()))
				{
					dataGridViewSoDienNuoc.Rows[i].Cells["ThanhTien"].Value = convertIntToMoney(dt.Rows[i]["ThanhTien"].ToString());
				}
				else
				{
					dataGridViewSoDienNuoc.Rows[i].Cells["ThanhTien"].Value = "0 đ";
				}
			}
			//this.dataGridViewSoDienNuoc.DataSource = dt;
		}

		#endregion

		private void dataGridViewSoDienNuoc_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			dataGridViewSoDienNuoc.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			if (dataGridViewSoDienNuoc.SelectedCells.Count > 0 && e.RowIndex >= 0)
			{
				DataGridViewRow selectedRow = dataGridViewSoDienNuoc.Rows[e.RowIndex];
				this.idPhong = selectedRow.Cells["ID_Phong1"].Value.ToString();
				this.idSoDien = selectedRow.Cells["ID_SoDien"].Value.ToString();
				this.idSoNuoc = selectedRow.Cells["ID_SoNuoc"].Value.ToString();
				this.idLsThuePhong_DienNuoc = selectedRow.Cells["ID_LichSuThuePhong1"].Value.ToString();
				this.soTienThanhToan = selectedRow.Cells["SoTienThanhToan1"].Value.ToString();
				this.thanhTien = selectedRow.Cells["ThanhTien"].Value.ToString();
				this.txtPhongUpd.Text = selectedRow.Cells["ID_Phong1"].Value.ToString();
				this.txtChiSoDienCu.Text = selectedRow.Cells["SoDienCu"].Value.ToString();
				this.txtChiSoDienMoi.Text = selectedRow.Cells["SoDienMoi"].Value.ToString();
				this.txtChiSoNuocCu.Text = selectedRow.Cells["SoNuocCu"].Value.ToString();
				this.txtChiSoNuocMoi.Text = selectedRow.Cells["SoNuocMoi"].Value.ToString();
			}
		}

		private void btnClear_Click(object sender, EventArgs e)
		{
			this.txtPhongUpd.Text = string.Empty;
			this.txtChiSoDienCu.Text = string.Empty;
			this.txtChiSoDienMoi.Text = string.Empty;
			this.txtChiSoNuocCu.Text = string.Empty;
			this.txtChiSoNuocMoi.Text = string.Empty;
			this.idPhong = string.Empty;
			this.idSoDien = string.Empty;
			this.idSoNuoc = string.Empty;
			this.idLsThuePhong_DienNuoc = string.Empty;
			this.soTienThanhToan = string.Empty;
			this.thanhTien = string.Empty;
		}

		private void btnLuu_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(this.txtPhongUpd.Text.ToString()))
			{
				MessageBox.Show("Vui lòng chọn phòng");
				return;
			}

			if ( string.IsNullOrEmpty(this.idLsThuePhong_DienNuoc))
			{
				MessageBox.Show("Phòng chưa có khách thuê");
				return;
			}

			if ( string.IsNullOrEmpty(this.txtChiSoDienCu.Text.ToString())
				|| string.IsNullOrEmpty(this.txtChiSoDienMoi.Text.ToString())
				|| string.IsNullOrEmpty(this.txtChiSoNuocCu.Text.ToString())
				|| string.IsNullOrEmpty(this.txtChiSoNuocMoi.Text.ToString())
				)
			{
				MessageBox.Show("Vui lòng nhập chỉ số điện nước");
				return;
			}


			if (!string.IsNullOrEmpty( this.idPhong ) )
			{
				try
				{
					// Tạo mới công nợ
					DTO_CongNo dTO_CongNo = new DTO_CongNo();
					BUS_CongNo bUS_CongNo = new BUS_CongNo();
					BUS_LichSuThuePhong bUS_LichSuThuePhong = new BUS_LichSuThuePhong();
					BUS_SoDien bUS_SoDien = new BUS_SoDien();
					BUS_SoNuoc bUS_SoNuoc = new BUS_SoNuoc();

					DataTable check = bUS_CongNo.CheckCongNoByIdDienNuoc(this.idLsThuePhong_DienNuoc, this.idSoDien,this.idSoNuoc);
					if (check.Rows.Count == 0)
					{
						dTO_CongNo.FlagInsert = true;
					}
					else
					{
						dTO_CongNo.FlagInsert = false;
					}

					if (string.IsNullOrEmpty(this.idSoDien))
					{
						string idsodien = "0";
						if (!string.IsNullOrEmpty(bUS_SoDien.GetMaxIdSoDien()))
						{
							idsodien = bUS_SoDien.GetMaxIdSoDien();
						}
						this.idSoDien = (int.Parse(idsodien) + 1).ToString().PadLeft(5, '0');
						DTO_SoDien dTO_SoDien = new DTO_SoDien();
						dTO_SoDien.ID_Phong = this.idPhong;
						dTO_SoDien.ID_SoDien = this.idSoDien;
						dTO_SoDien.SoCu = this.txtChiSoDienCu.Text;
						dTO_SoDien.SoMoi = this.txtChiSoDienMoi.Text;
						dTO_SoDien.NgayBatDauSuDungDien = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
						dTO_SoDien.NgayKetThucSuDungDien = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
						dTO_SoDien.ID_LoaiDonGia = "00";
						dTO_SoDien.FlagInsert = true;
						bUS_SoDien.Insert(dTO_SoDien);
					}
					else
					{
						bUS_SoDien.Update(this.idPhong,
									   this.idSoDien,
									   this.txtChiSoDienCu.Text.ToString(),
									   this.txtChiSoDienMoi.Text.ToString()
						);

					}

					if (string.IsNullOrEmpty(this.idSoNuoc))
					{

						string idsonuoc = "0";
						if (!string.IsNullOrEmpty(bUS_SoNuoc.GetMaxIdSoNuoc()))
						{
							idsonuoc = bUS_SoNuoc.GetMaxIdSoNuoc();
						}
						this.idSoNuoc = (int.Parse(idsonuoc) + 1).ToString().PadLeft(5, '0');

						DTO_SoNuoc dTO_SoNuoc= new DTO_SoNuoc();
						dTO_SoNuoc.ID_Phong = this.idPhong;
						dTO_SoNuoc.ID_SoNuoc = this.idSoNuoc;
						dTO_SoNuoc.SoCu = this.txtChiSoNuocCu.Text;
						dTO_SoNuoc.SoMoi = this.txtChiSoNuocMoi.Text;
						dTO_SoNuoc.NgayBatDauSuDungNuoc = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
						dTO_SoNuoc.NgayKetThucSuDungNuoc = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
						dTO_SoNuoc.ID_LoaiDonGia = "01";
						dTO_SoNuoc.FlagInsert = true;
						bUS_SoNuoc.Insert(dTO_SoNuoc);
					}
					else
					{
						bUS_SoNuoc.Update(this.idPhong,
									   this.idSoNuoc,
									   this.txtChiSoNuocCu.Text.ToString(),
									   this.txtChiSoNuocMoi.Text.ToString()
						);
					}

					DataTable dt = bUS_LichSuThuePhong.GetLichSuThuePhongByID(this.idLsThuePhong_DienNuoc);
					string idKhachHang = string.Empty;

					if (dt != null)
					{
						idKhacHang = dt.Rows[0]["ID_KhachHang"].ToString();
					}

					string id = "0";
					DataTable dtCognNo = bUS_CongNo.GetCongNoByID(this.idLsThuePhong_DienNuoc, idKhacHang, this.idPhong);
					if (dtCognNo != null && dtCognNo.Rows.Count > 0)
					{
						if (!string.IsNullOrEmpty(dtCognNo.Rows[0]["ID_SoDien"].ToString()))
						{
							dTO_CongNo.ID_CongNo = dtCognNo.Rows[0]["ID_CongNo"].ToString();
						}
						else
						{
							if (!string.IsNullOrEmpty(bUS_CongNo.GetMaxIdCongNo()))
							{
								id = bUS_CongNo.GetMaxIdCongNo();
							}

							dTO_CongNo.ID_CongNo = (int.Parse(id) + 1).ToString().PadLeft(5, '0');
							dTO_CongNo.FlagInsert = true;
						}
					}
					else
					{
						if (!string.IsNullOrEmpty(bUS_CongNo.GetMaxIdCongNo()))
						{
							id = bUS_CongNo.GetMaxIdCongNo();
							dTO_CongNo.ID_CongNo = (int.Parse(id) + 1).ToString().PadLeft(5, '0');
						}
					}

					dTO_CongNo.ID_KhachHang = idKhacHang;
					dTO_CongNo.ID_Phong = this.idPhong;
					dTO_CongNo.ThoiGianBatDau = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
					dTO_CongNo.ThoiGianKetThuc = this.dateTimeKetThuc.Value.ToString("MM/dd/yyyy HH:mm:ss");
					dTO_CongNo.TrangThaiNo = "1";
					dTO_CongNo.DelFlg = 0;
					dTO_CongNo.ID_LichSuThuePhong = this.idLsThuePhong_DienNuoc;
					dTO_CongNo.ID_SoDien = this.idSoDien;
					dTO_CongNo.ID_SoNuoc = this.idSoNuoc;

					//
					int soDienTieuThu = int.Parse(this.txtChiSoDienMoi.Text.ToString()) - int.Parse(this.txtChiSoDienCu.Text.ToString());
					int soNuocTieuThu = int.Parse(this.txtChiSoNuocMoi.Text.ToString()) - int.Parse(this.txtChiSoNuocCu.Text.ToString());

                    if (dtCognNo.Rows.Count > 0)
                    {
						dTO_CongNo.SoTienNo = (Int32.Parse(dtCognNo.Rows[0]["SoTienNo"].ToString())+ (soDienTieuThu * 1500 + soNuocTieuThu * 10000)).ToString();
					}
                    else
                    {
						dTO_CongNo.SoTienNo = (soDienTieuThu * 1500 + soNuocTieuThu * 10000).ToString();
					}

					bUS_CongNo.Update(dTO_CongNo);

					MessageBox.Show("Update thành công");
					this.InitDataGridViewDsDienNuoc();
				}
				catch (Exception ex)
				{
					MessageBox.Show("Lỗi: " + ex.ToString());
					return;
				}
			}
		}

		private void InitComboboxTrangThaiPhongTimKiem()
		{
			var trangThaiPhong = new BindingList<KeyValuePair<string, string>>();
			trangThaiPhong.Add(new KeyValuePair<string, string>("", " --- Trạng thái phòng --- "));
			trangThaiPhong.Add(new KeyValuePair<string, string>("0", " --- Chưa thuê --- "));
			trangThaiPhong.Add(new KeyValuePair<string, string>("1", " ---  Đã thuê  --- "));

			this.cbTrangThai.DataSource = trangThaiPhong;
			this.cbTrangThai.ValueMember = "Key";
			this.cbTrangThai.DisplayMember = "Value";
			this.cbTrangThai.SelectedIndex = 0;

			this.comboBoxtt.DataSource = trangThaiPhong;
			this.comboBoxtt.ValueMember = "Key";
			this.comboBoxtt.DisplayMember = "Value";
			this.comboBoxtt.SelectedIndex = 0;
		}

		private void btnTimKiemSoDienNuoc_Click(object sender, EventArgs e)
		{
			this.InitDataGridViewDsDienNuoc();
		}

		#endregion

		#region tab tính tiền

		private void InitDataGridViewTinhTien()
		{
			BUS_DataGet bUS_DataGet = new BUS_DataGet();
			string tt = string.Empty;
			if (this.comboBox1.SelectedValue != null)
			{
				tt = this.comboBox1.SelectedValue.ToString();
			}

			DataTable dtTinhTien = bUS_DataGet.GetDataTinhTienInf( this.txtTImKiemTinhTien.Text.ToString(), tt);

			for (int i = 0; i < dtTinhTien.Rows.Count; i++)
			{
				dataGridViewTinhTien.RowCount = dtTinhTien.Rows.Count;
				dataGridViewTinhTien.Rows[i].Cells["ID_Phong2"].Value = dtTinhTien.Rows[i]["ID_Phong"].ToString();
				dataGridViewTinhTien.Rows[i].Cells["TrangThai2"].Value = dtTinhTien.Rows[i]["TrangThai"].ToString();
				dataGridViewTinhTien.Rows[i].Cells["ID_CongNo2"].Value = dtTinhTien.Rows[i]["ID_CongNo"].ToString();
				dataGridViewTinhTien.Rows[i].Cells["ID_SoDien2"].Value = dtTinhTien.Rows[i]["ID_SoDien"].ToString();
				dataGridViewTinhTien.Rows[i].Cells["ID_SoNuoc2"].Value = dtTinhTien.Rows[i]["ID_SoNuoc"].ToString();
				dataGridViewTinhTien.Rows[i].Cells["ID_KhachHang2"].Value = dtTinhTien.Rows[i]["ID_KhachHang"].ToString();
				dataGridViewTinhTien.Rows[i].Cells["TenKhachHang2"].Value = dtTinhTien.Rows[i]["TenKhachHang"].ToString();
				dataGridViewTinhTien.Rows[i].Cells["ID_LichSuThuePhong2"].Value = dtTinhTien.Rows[i]["ID_LichSuThuePhong"].ToString();
				dataGridViewTinhTien.Rows[i].Cells["TongSoTien"].Value = convertIntToMoney(dtTinhTien.Rows[i]["TongSoTien"].ToString());
				dataGridViewTinhTien.Rows[i].Cells["TongSoTienDaThanhToan"].Value = convertIntToMoney(dtTinhTien.Rows[i]["TongSoTienDaThanhToan"].ToString());
				dataGridViewTinhTien.Rows[i].Cells["TongSoTienChuaThanhToan"].Value = convertIntToMoney(dtTinhTien.Rows[i]["TongSoTienChuaThanhToan"].ToString());
			}

			//this.dataGridViewTinhTien.DataSource = dtTinhTien;
		}

		private void InitComboboxTrangThaiTinhTien()
		{
			var trangThai = new BindingList<KeyValuePair<string, string>>();
			trangThai.Add(new KeyValuePair<string, string>("", " --- Trạng thái --- "));
			trangThai.Add(new KeyValuePair<string, string>("1", " --- Chưa tính tiền --- "));
			trangThai.Add(new KeyValuePair<string, string>("0", " --- Đã tính tiền --- "));

			this.comboBox1.DataSource = trangThai;
			this.comboBox1.ValueMember = "Key";
			this.comboBox1.DisplayMember = "Value";
			this.comboBox1.SelectedIndex = 0;
		}

		private void btnTImKiemTinhTien_Click(object sender, EventArgs e)
		{
			this.InitDataGridViewTinhTien();
		}

		private void dataGridViewTinhTien_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			dataGridViewTinhTien.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (dataGridViewTinhTien.SelectedCells.Count > 0 )
			{
				DataGridViewRow row = this.dataGridViewTinhTien.SelectedRows[0];
				BUS_LichSuThanhToan bUS_LichSuThanhToan = new BUS_LichSuThanhToan();

				string id = "0";
				if (!string.IsNullOrEmpty(bUS_LichSuThanhToan.GetMaxIdLsThanhToan()))
				{
					id = bUS_LichSuThanhToan.GetMaxIdLsThanhToan();
				}

				DTO_LichSuThanhToan dTO_LichSuThanhToan = new DTO_LichSuThanhToan();
				dTO_LichSuThanhToan.ID_ThanhToan = (int.Parse(id) + 1).ToString().PadLeft(5, '0');
				dTO_LichSuThanhToan.ID_LichSuThuePhong = row.Cells["ID_LichSuThuePhong2"].Value.ToString();
				dTO_LichSuThanhToan.ID_SoDien = row.Cells["ID_SoDien2"].Value.ToString();
				dTO_LichSuThanhToan.ID_SoNuoc = row.Cells["ID_SoNuoc2"].Value.ToString();
				dTO_LichSuThanhToan.KyThanhToan = string.Empty;
				dTO_LichSuThanhToan.SoTienThanhToan = row.Cells["TongSoTienChuaThanhToan"].Value.ToString().Replace("đ","").Replace(".","");
				dTO_LichSuThanhToan.FlagInsert = true;
				bUS_LichSuThanhToan.Update(dTO_LichSuThanhToan);

				BUS_CongNo bUS_CongNo = new BUS_CongNo();
				bUS_CongNo.UpdateTrangThaiNo(row.Cells["ID_CongNo2"].Value.ToString(), "0");

				MessageBox.Show("Tính tiền thành công");

				this.InitDataGridViewTinhTien();
			}
			else
			{
				MessageBox.Show("vui lòng chọn khách hàng cần tính tiền");
				return;
			}

		}

		private void button2_Click(object sender, EventArgs e)
		{
			if (dataGridViewTinhTien.SelectedRows.Count != 0)
			{
				DataGridViewRow row = this.dataGridViewTinhTien.SelectedRows[0];
				if (row.Cells["TongSoTienChuaThanhToan"].Value.ToString() == "0 đ")
				{
					BUS_KhachHang bUS_KhachHang = new BUS_KhachHang();
					bUS_KhachHang.Delete(row.Cells["ID_KhachHang2"].Value.ToString());

					BUS_Phong bUS_Phong = new BUS_Phong();
					bUS_Phong.UpdateTrangThai(row.Cells["ID_Phong2"].Value.ToString(), "0");

					BUS_LichSuThuePhong bUS_LichSuThuePhong = new BUS_LichSuThuePhong();
					bUS_LichSuThuePhong.Delete(row.Cells["ID_LichSuThuePhong2"].Value.ToString());

					BUS_SoDien bUS_SoDien = new BUS_SoDien();
					bUS_SoDien.Delete(row.Cells["ID_SoDien2"].Value.ToString());

					BUS_SoNuoc bUS_SoNuoc = new BUS_SoNuoc();
					bUS_SoNuoc.Delete(row.Cells["ID_SoNuoc2"].Value.ToString());

					MessageBox.Show("Xóa khách hàng thành công");

					this.InitDataGridViewTinhTien();
				}
				else
				{
					MessageBox.Show("Khách hàng chưa thanh toán tiền phòng");
					return;
				}

			}
			else
			{
				MessageBox.Show("vui lòng chọn khách hàng cần trả phòng");
				return;
			}
		}

		private void btnThemPhongTro_Click(object sender, EventArgs e)
		{
			BUS_Phong bUS_Phong = new BUS_Phong();

			string id = "0";
			if (!string.IsNullOrEmpty(bUS_Phong.GetMaxIDPhong()))
			{
				id = bUS_Phong.GetMaxIDPhong();
			}
			bUS_Phong.Insert((int.Parse(id) + 1).ToString().PadLeft(3, '0'));
			this.InitGroupBoxTrangChu();
		}

		private void btnBaoCao_Click(object sender, EventArgs e)
		{
			string search = this.textBoxTimKiem.Text;
			string tt = string.Empty;
			if (this.comboBoxtt.SelectedValue != null)
			{
				tt = this.comboBoxtt.SelectedValue.ToString();
			}

			string ngay = "0";
			if(dateBaoBao.Value!=null)
            {
				ngay = dateBaoBao.Value.ToString("yyyy-MM-dd");
			}				

			BUS_DataGet bUS_DataGet = new BUS_DataGet();
			DataTable dt = bUS_DataGet.GetDataBaoCao(search, tt, ngay);
			this.dgvBaoCao.DataSource = dt;
		}

		private string convertIntToMoney(string tongTien)
		{
			string temp = tongTien;
			for (int i = 3; i < tongTien.Length; i = i + 3)
			{
				temp = temp.Insert(tongTien.Length - i, ".");
			}

			return temp + " đ";
		}

		private void btnTimKiemKH_Click(object sender, EventArgs e)
		{
			this.InitDataGridViewDsKhachHang();
		}

        #endregion

        #region tab tài khoản

        private void loadTaiKhoan()
        {
			BUS_TaiKhoan BUS_TaiKhoan = new BUS_TaiKhoan();

			if (this.txtTimKiemTK.Text.Trim() != string.Empty)
			{
				DataTable tk = BUS_TaiKhoan.GetTaiKhoanbyName(this.txtTimKiemTK.Text.Trim());
				this.dgvTaiKhoan.DataSource = tk;
			}
			else
			{
				DataTable tk = BUS_TaiKhoan.GetTaiKhoan();
				this.dgvTaiKhoan.DataSource = tk;
			}
		}

		private void dgvTaiKhoan_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			if (this.dgvTaiKhoan.CurrentRow!= null)
			{
				this.txtNhapLaiMK.Visible = true;
				this.labMatKhauMoi.Visible = true;
				this.txtMaTK.Enabled = false;
				this.txtMaTK.Text = this.dgvTaiKhoan.CurrentRow.Cells["ID_TaiKhoan"].Value.ToString();
				this.txtTenTK.Text = this.dgvTaiKhoan.CurrentRow.Cells["TenTaiKhoan"].Value.ToString();
				this.cbQuyen.SelectedValue = this.dgvTaiKhoan.CurrentRow.Cells["Quyen_TK"].Value.ToString();
				this.txtMatKhau.Text = this.dgvTaiKhoan.CurrentRow.Cells["Password"].Value.ToString();
				this.btnThemTK.Enabled = false;
				this.txtNhapLaiMK.Text = "";
				this.btnSuaTK.Enabled = true;
				this.btnXoaTK.Enabled = true;

				if (this.quyen == "9999")
				{
					this.cbQuyen.Enabled = true;
					this.btnXoaTK.Enabled = true;
				}
				else
				{
					this.cbQuyen.Enabled = false;
					this.btnXoaTK.Enabled = false;
				}

				BUS_TaiKhoan bUS_TaiKhoan = new BUS_TaiKhoan();
				DataTable checkTk = bUS_TaiKhoan.GetTaiKhoanByID(this.idTk);

				if (checkTk.Rows.Count> 0)
				{
					if (checkTk.Rows[0]["Quyen"].ToString() == "5555" && checkTk.Rows[0]["ID_TaiKhoan"].ToString() == this.txtMaTK.Text.Trim())
					{
						this.btnSuaTK.Enabled = true;
					}
					else if (checkTk.Rows[0]["Quyen"].ToString() == "9999" )
					{
						this.btnSuaTK.Enabled = true;
					}
					else
					{
						this.btnSuaTK.Enabled = false;
					}
				}
			}
		}

		private void LoadCbTK()
        {
			var cbQuyen = new BindingList<KeyValuePair<string, string>>();
			cbQuyen.Add(new KeyValuePair<string, string>("", " --- Chọn quyền --- "));
			cbQuyen.Add(new KeyValuePair<string, string>("9999", " --- Admin --- "));
			cbQuyen.Add(new KeyValuePair<string, string>("5555", " ---  Nhân viên  --- "));

			this.cbQuyen.DataSource = cbQuyen;
			this.cbQuyen.ValueMember = "Key";
			this.cbQuyen.DisplayMember = "Value";
			this.cbQuyen.SelectedIndex = 0;

		}

		private bool Valid()
		{
			if (this.txtTenTK.Text.Trim() == "")
			{
				MessageBox.Show("Tên tài khoản không được để trống");
				return false;
			}

			if (this.cbQuyen.SelectedValue.ToString().Trim() == "")
			{
				MessageBox.Show("Vùi lòng chọn quyền cho người dùng");
				return false;
			}

			if (this.txtMatKhau.Text.Trim() == "")
			{
				MessageBox.Show("Mật khẩu không được để trống");
				return false;
			}

			return true;
		}

		private void btnThemTK_Click(object sender, EventArgs e)
		{
			if (Valid())
			{
				BUS_TaiKhoan bUS_TaiKhoan = new BUS_TaiKhoan();
				string id = string.Empty;
				if (!string.IsNullOrEmpty(bUS_TaiKhoan.GetMaxId()))
				{
					id = (int.Parse( bUS_TaiKhoan.GetMaxId() ) + 1).ToString();
				}

				DTO_TaiKhoan dTO_TaiKhoan = new DTO_TaiKhoan();
				dTO_TaiKhoan.ID_TaiKhoan = id.PadLeft(5, '0');
				dTO_TaiKhoan.TenTaiKhoan = txtTenTK.Text;
				dTO_TaiKhoan.Quyen = cbQuyen.SelectedValue.ToString();
				dTO_TaiKhoan.Password = txtMatKhau.Text;
				dTO_TaiKhoan.DelFlg = 0;
				dTO_TaiKhoan.FlagInsert = true;

				BUS_TaiKhoan bBUS_TaiKhoan = new BUS_TaiKhoan();
				if(bBUS_TaiKhoan.Update(dTO_TaiKhoan))
				{
					MessageBox.Show("Thêm thành công!");
					loadTaiKhoan();
				}
				else
				{
					MessageBox.Show("Thất bại!");
				}
			}
		}

		private void btnXoaTK_Click(object sender, EventArgs e)
		{
			BUS_TaiKhoan bBUS_TaiKhoan = new BUS_TaiKhoan();
			if (bBUS_TaiKhoan.Delete(txtMaTK.Text))
			{
				MessageBox.Show("Update thành công!");
				loadTaiKhoan();
			}
			else
			{
				MessageBox.Show("Thất bại!");
			}
		}

		private void btnSuaTK_Click(object sender, EventArgs e)
		{
			if (Valid())
			{
				DTO_TaiKhoan dTO_TaiKhoan = new DTO_TaiKhoan();
				dTO_TaiKhoan.ID_TaiKhoan = txtMaTK.Text;
				dTO_TaiKhoan.TenTaiKhoan = txtTenTK.Text;
				dTO_TaiKhoan.Quyen = cbQuyen.SelectedValue.ToString();
				if (txtNhapLaiMK.Text.Trim() == "")
				{
					dTO_TaiKhoan.Password = txtMatKhau.Text;
				}
				else
				{
					dTO_TaiKhoan.Password = txtNhapLaiMK.Text;
				}
				
				dTO_TaiKhoan.DelFlg = 0;
				dTO_TaiKhoan.FlagInsert = false;

				BUS_TaiKhoan bBUS_TaiKhoan = new BUS_TaiKhoan();
				if(bBUS_TaiKhoan.Update(dTO_TaiKhoan))
				{
					MessageBox.Show("Update thành công!");
					loadTaiKhoan();
				}
				else
				{
					MessageBox.Show("Thất bại!");
				}
			}
		}

		private void btnLamMoi_Click(object sender, EventArgs e)
		{
			this.txtMaTK.Enabled = false;
			this.btnSuaTK.Enabled = false;
			this.btnXoaTK.Enabled = false;
			this.txtMaTK.Text = string.Empty;
			this.txtTenTK.Text = string.Empty;
			this.txtMatKhau.Text = string.Empty;
			this.txtNhapLaiMK.Text = string.Empty;
			this.labMatKhauMoi.Visible = false;
			this.txtNhapLaiMK.Visible = false;
			this.cbQuyen.SelectedIndex = 0;

			if (this.quyen == "9999")
			{
				this.cbQuyen.Enabled = true;
				this.btnThemTK.Enabled = true;
			}
			else
			{
				this.cbQuyen.Enabled = false;
				this.btnThemTK.Enabled = false;
			}
		}

		private void btnTimKiemTK_Click(object sender, EventArgs e)
		{
			loadTaiKhoan();
		}

		private void btnDangXuat_Click(object sender, EventArgs e)
		{
			if (MessageBox.Show("Bạn có muốn đăng xuất không?","Thông báo",MessageBoxButtons.OKCancel,MessageBoxIcon.Question)==DialogResult.OK)
			{
				this.Hide();
				Login f = new Login();
				f.Show();
			}
		}
		#endregion
	}
}