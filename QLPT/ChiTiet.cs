﻿using BUS;
using DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLPT
{
	public partial class ChiTiet : Form
	{
		string Id = string.Empty;
		string idkhachHang = string.Empty;

		public ChiTiet()
		{
			InitializeComponent();
		}

		public ChiTiet(string idPhong)
			: this()
		{
			this.Id = idPhong;
		}

		private void ChiTiet_Load(object sender, EventArgs e)
		{
			this.Init();
		}

		private void Init()
		{
			BUS_LoaiPhong bUS_LoaiPhong = new BUS_LoaiPhong();
			DataTable dsPhong = bUS_LoaiPhong.GetLoaiPhong();

			DataTable dt = new DataTable();
			dt.Columns.Add("Key", typeof(string));
			dt.Columns.Add("Value", typeof(string));

			DataRow row = dt.NewRow();
			row["Key"] = "000";
			row["Value"] = "--- Chọn loại phòng ---";
			dt.Rows.Add(row);

			foreach (DataRow dsRow in dsPhong.Rows)
			{
				row = dt.NewRow();
				row["Key"] = dsRow["ID_LoaiPhong"].ToString();
				row["Value"] = dsRow["TenLoaiPhong"].ToString();
				dt.Rows.Add(row);
			}

			this.comboBoxLoaiPhong.ValueMember = "Key";
			this.comboBoxLoaiPhong.DisplayMember = "Value";
			this.comboBoxLoaiPhong.DataSource = dt;
			this.comboBoxLoaiPhong.SelectedIndex = 0;

			BUS_Phong bUS_Phong = new BUS_Phong();
			DataTable phong = bUS_Phong.GetPhongByID(this.Id);

			this.textBoxIdPhong.Enabled = false;
			this.textBoxIdPhong.Text = this.Id;
			this.comboBoxLoaiPhong.SelectedValue = phong.Rows[0]["ID_LoaiPhong"].ToString();
			this.textBoxGia.Text = convertIntToMoney(phong.Rows[0]["Gia"].ToString());

			BUS_LichSuThuePhong bUS_LichSuThuePhong = new BUS_LichSuThuePhong();
			DataTable lstp = bUS_LichSuThuePhong.GetLichSuThuePhongByIdPhong(this.Id);

			if (lstp != null && lstp.Rows.Count > 0)
			{
				this.dateTimePickerBatDau.Value = Convert.ToDateTime( lstp.Rows[0]["ThoiGianBatDauThue"] );
				this.dateTimePickerKetThuc.Value = Convert.ToDateTime(lstp.Rows[0]["ThoiGianKetThucThue"]);

				BUS_KhachHang bUS_KhachHang = new BUS_KhachHang();
				DataTable khachHang = bUS_KhachHang.GetKhachHangByID(lstp.Rows[0]["ID_KhachHang"].ToString());
				this.idkhachHang = lstp.Rows[0]["ID_KhachHang"].ToString();
				if (khachHang != null && khachHang.Rows.Count > 0)
				{
					this.textBoxTenKhachHang.Text = khachHang.Rows[0]["TenKhachHang"].ToString();
					this.textBoxCMND.Text = khachHang.Rows[0]["CCCD"].ToString();
					this.textBoxSoDienThoai.Text = khachHang.Rows[0]["SoDienThoai"].ToString();
					this.richTextBoxDiaChi.Text = khachHang.Rows[0]["DiaChi"].ToString();
				}
			}

			BUS_DataGet bUS_DataGet = new BUS_DataGet();
			DataTable dtSoDien = bUS_DataGet.GetDataSoDien(this.Id);
			string tongdien = "0";
			string tongnuoc = "0";
			if (dtSoDien!= null && dtSoDien.Rows.Count>0)
			{
				this.lbSoDienCu.Text = dtSoDien.Rows[0]["SDSoCu"].ToString();
				this.lbSoDienMoi.Text = dtSoDien.Rows[0]["SDSoMoi"].ToString();
				this.lbDaSuDungDien.Text = Int32.Parse(dtSoDien.Rows[0]["SoDienSuDung"].ToString()).ToString();
				this.lbTongTienDien.Text = convertIntToMoney(dtSoDien.Rows[0]["ThanhTien"].ToString());
				tongdien = dtSoDien.Rows[0]["ThanhTien"].ToString();
			}
			else
			{
				this.lbSoDienCu.Text =  "0";
				this.lbSoDienMoi.Text =  "0";
				this.lbDaSuDungDien.Text =  "0";
				this.lbTongTienDien.Text =  "0" + " đ";
			}

			DataTable soNuoc = bUS_DataGet.GetDataSoNuoc(this.Id);
			if (soNuoc!= null && soNuoc.Rows.Count>0)
			{
				this.lbSoNuocCu.Text = soNuoc.Rows[0]["SNSoCu"].ToString();
				this.lbSoNuocMoi.Text = soNuoc.Rows[0]["SNSoMoi"].ToString();
				this.lbDaSuDungNuoc.Text = Int32.Parse(soNuoc.Rows[0]["SoNuocSuDung"].ToString()).ToString();
				this.lbTongTienNuoc.Text = convertIntToMoney(soNuoc.Rows[0]["ThanhTien"].ToString());
				tongnuoc = soNuoc.Rows[0]["ThanhTien"].ToString();
				
			}
			else
			{
				this.lbSoNuocCu.Text = "0";
				this.lbSoNuocMoi.Text = "0";
				this.lbDaSuDungNuoc.Text = "0";
				this.lbTongTienNuoc.Text = "0" + " đ";
			}

			if (!string.IsNullOrEmpty(tongnuoc) && !string.IsNullOrEmpty(tongdien))
			{
				this.labTong.Text = convertIntToMoney((Int32.Parse(tongdien) + Int32.Parse(tongnuoc)).ToString());
			}
			this.textBoxTenKhachHang.Enabled = false;
			this.textBoxSoDienThoai.Enabled = false;
			this.textBoxCMND.Enabled = false;
			this.richTextBoxDiaChi.Enabled = false;
		}

		private string convertIntToMoney(string tongTien)
		{
			string temp = tongTien;
			for (int i = 3; i < tongTien.Length; i = i +3)
			{
				temp = temp.Insert(tongTien.Length - i, ".");
			}

			return temp.Trim() + " đ";
		}

		private void button1_Click(object sender, EventArgs e)
		{
			BUS_KhachHang bUS_KhachHang = new BUS_KhachHang();
			DTO_KhachHang dTO_KhachHang = new DTO_KhachHang();

			// update thông tin khách hàng
			if (!string.IsNullOrEmpty(this.idkhachHang))
			{
				dTO_KhachHang.ID_KhachHang = this.idkhachHang;
				dTO_KhachHang.TenKhachHang = this.textBoxTenKhachHang.Text;
				dTO_KhachHang.CMND = this.textBoxCMND.Text;
				dTO_KhachHang.SoDienThoai = this.textBoxSoDienThoai.Text;
				dTO_KhachHang.DiaChi = this.richTextBoxDiaChi.Text;
				dTO_KhachHang.DelFlg = 0;
				dTO_KhachHang.FlagInsert = false;
				bUS_KhachHang.Update(dTO_KhachHang);
			}

			BUS_Phong bUS_Phong = new BUS_Phong();
			if (this.comboBoxLoaiPhong.SelectedValue != null)
			{
				bUS_Phong.Update(this.Id, this.textBoxGia.Text.ToString().Replace(".","").Replace("đ","").Trim(), this.comboBoxLoaiPhong.SelectedValue.ToString());
				MessageBox.Show("Update thành công");
				Init();
			}
		}

		private void textBox2_TextChanged(object sender, EventArgs e)
		{

		}
	}
}
