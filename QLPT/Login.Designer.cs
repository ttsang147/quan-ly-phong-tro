﻿
namespace QLPT
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.btnLogin = new System.Windows.Forms.Button();
			this.btnHuy = new System.Windows.Forms.Button();
			this.labDangNhap = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btHuy = new System.Windows.Forms.Button();
			this.btDangNhap = new System.Windows.Forms.Button();
			this.tbPassWord = new System.Windows.Forms.TextBox();
			this.tbUserName = new System.Windows.Forms.TextBox();
			this.labMatKhau = new System.Windows.Forms.Label();
			this.labUsername = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnLogin
			// 
			this.btnLogin.Location = new System.Drawing.Point(189, 384);
			this.btnLogin.Margin = new System.Windows.Forms.Padding(4);
			this.btnLogin.Name = "btnLogin";
			this.btnLogin.Size = new System.Drawing.Size(100, 31);
			this.btnLogin.TabIndex = 0;
			this.btnLogin.Text = "Đăng nhập";
			this.btnLogin.UseVisualStyleBackColor = true;
			// 
			// btnHuy
			// 
			this.btnHuy.Location = new System.Drawing.Point(513, 384);
			this.btnHuy.Margin = new System.Windows.Forms.Padding(4);
			this.btnHuy.Name = "btnHuy";
			this.btnHuy.Size = new System.Drawing.Size(100, 31);
			this.btnHuy.TabIndex = 1;
			this.btnHuy.Text = "Hủy";
			this.btnHuy.UseVisualStyleBackColor = true;
			// 
			// labDangNhap
			// 
			this.labDangNhap.AutoSize = true;
			this.labDangNhap.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labDangNhap.Location = new System.Drawing.Point(181, 12);
			this.labDangNhap.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.labDangNhap.Name = "labDangNhap";
			this.labDangNhap.Size = new System.Drawing.Size(154, 32);
			this.labDangNhap.TabIndex = 2;
			this.labDangNhap.Text = "Đăng nhập";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btHuy);
			this.panel1.Controls.Add(this.btDangNhap);
			this.panel1.Controls.Add(this.tbPassWord);
			this.panel1.Controls.Add(this.tbUserName);
			this.panel1.Controls.Add(this.labMatKhau);
			this.panel1.Controls.Add(this.labUsername);
			this.panel1.Location = new System.Drawing.Point(16, 48);
			this.panel1.Margin = new System.Windows.Forms.Padding(4);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(480, 219);
			this.panel1.TabIndex = 3;
			// 
			// btHuy
			// 
			this.btHuy.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btHuy.Location = new System.Drawing.Point(259, 159);
			this.btHuy.Margin = new System.Windows.Forms.Padding(4);
			this.btHuy.Name = "btHuy";
			this.btHuy.Size = new System.Drawing.Size(123, 31);
			this.btHuy.TabIndex = 5;
			this.btHuy.Text = "Hủy";
			this.btHuy.UseVisualStyleBackColor = true;
			this.btHuy.Click += new System.EventHandler(this.btHuy_Click);
			// 
			// btDangNhap
			// 
			this.btDangNhap.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btDangNhap.Location = new System.Drawing.Point(67, 159);
			this.btDangNhap.Margin = new System.Windows.Forms.Padding(4);
			this.btDangNhap.Name = "btDangNhap";
			this.btDangNhap.Size = new System.Drawing.Size(125, 31);
			this.btDangNhap.TabIndex = 4;
			this.btDangNhap.Text = "Đăng nhập";
			this.btDangNhap.UseVisualStyleBackColor = true;
			this.btDangNhap.Click += new System.EventHandler(this.btDangNhap_Click);
			// 
			// tbPassWord
			// 
			this.tbPassWord.Location = new System.Drawing.Point(171, 80);
			this.tbPassWord.Margin = new System.Windows.Forms.Padding(4);
			this.tbPassWord.Name = "tbPassWord";
			this.tbPassWord.Size = new System.Drawing.Size(241, 22);
			this.tbPassWord.TabIndex = 3;
			this.tbPassWord.UseSystemPasswordChar = true;
			// 
			// tbUserName
			// 
			this.tbUserName.Location = new System.Drawing.Point(171, 37);
			this.tbUserName.Margin = new System.Windows.Forms.Padding(4);
			this.tbUserName.Name = "tbUserName";
			this.tbUserName.Size = new System.Drawing.Size(241, 22);
			this.tbUserName.TabIndex = 2;
			// 
			// labMatKhau
			// 
			this.labMatKhau.AutoSize = true;
			this.labMatKhau.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labMatKhau.Location = new System.Drawing.Point(29, 85);
			this.labMatKhau.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.labMatKhau.Name = "labMatKhau";
			this.labMatKhau.Size = new System.Drawing.Size(71, 19);
			this.labMatKhau.TabIndex = 1;
			this.labMatKhau.Text = "Mật khẩu";
			// 
			// labUsername
			// 
			this.labUsername.AutoSize = true;
			this.labUsername.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labUsername.Location = new System.Drawing.Point(29, 37);
			this.labUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.labUsername.Name = "labUsername";
			this.labUsername.Size = new System.Drawing.Size(105, 19);
			this.labUsername.TabIndex = 0;
			this.labUsername.Text = "Tên đăng nhập";
			// 
			// Login
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(512, 283);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.labDangNhap);
			this.Controls.Add(this.btnHuy);
			this.Controls.Add(this.btnLogin);
			this.Margin = new System.Windows.Forms.Padding(4);
			this.Name = "Login";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Đăng nhập";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnHuy;
        private System.Windows.Forms.Label labDangNhap;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labMatKhau;
        private System.Windows.Forms.Label labUsername;
        private System.Windows.Forms.TextBox tbPassWord;
        private System.Windows.Forms.TextBox tbUserName;
        private System.Windows.Forms.Button btHuy;
        private System.Windows.Forms.Button btDangNhap;
    }
}